DROP TRIGGER OPS$MESACAM.T_OPERACIONES_TIBCO;

CREATE OR REPLACE TRIGGER OPS$MESACAM.T_OPERACIONES_TIBCO
   BEFORE INSERT
   ON OPS$MESACAM.OPERACIONES_TIBCO    REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
WHEN (
(new.estado_tibco = 'R'
           AND new.tipo_operacion IN
                    ('CD',
                     'VD',
                     'TCD',
                     'TVD',
                     'AC',
                     'AV',
                     'AFC',
                     'AFV',
                     'CF',
                     'VF',
                     'TAC',
                     'TAV',
                     'XXX'))
         OR new.estado_tibco = 'DELET'
      )
DECLARE
   -- variables para el debug
   idtrigger                 VARCHAR2 (40) := 'T_OPERACIONES_TIBCO';
   msj                       VARCHAR2 (255);
   smj                       NUMBER := 0;
   --
   auxdummy                  NUMBER;
   folio                     NUMBER;
   salida exception;
   x_empresa                 VARCHAR2 (15);
   x_origin                  VARCHAR2 (15);
   x_codigo_area             NUMBER;
   x_codigo_area_ing         NUMBER;
   x_tasa_mon_nac            NUMBER;
   x_tasa_mon_ext            NUMBER;
   x_precio_financiero       NUMBER;
   x_banco_acreditar         VARCHAR2 (10);
   x_bco_pagador             VARCHAR2 (10);
   x_banco_debitar           VARCHAR2 (10);
   x_tipo_operacion          VARCHAR2 (5);
   x_documento_pago_mop      VARCHAR2 (2);
   x_documento_pago_mtx      VARCHAR2 (2);
   x_tasa_futuro             NUMBER;
   retorno_rutinas           NUMBER;
   x_FECHA_COMPENSACION      DATE;
   x_fecha_vencimiento       DATE;
   x_fecha_inicio            DATE;
   existe_op                 VARCHAR2 (1);
   x_clase_cliente           VARCHAR2 (3);
   x_ID_CLIENTE              VARCHAR2 (10);
   x_ID_PRODUCTO             VARCHAR2 (10);
   x_PORTAFOLIO              VARCHAR2 (40);
   x_ID_TRADER               VARCHAR2 (15);
   x_MESA                    VARCHAR2 (3);
   x_MONEDA_OPERACION        VARCHAR2 (3);
   x_MONEDA_TRANSACCION      VARCHAR2 (3);
   x_TIPO_DOLAR              VARCHAR2 (3);
   x_PARIDAD_OPERACION       NUMBER;
   x_PARIDAD_TRANSACCION     NUMBER;
   x_PRECIO_SPOT             NUMBER;
   x_TIPO_CAMBIO_DOLAR       NUMBER;
   x_MONTO_OPERACION         NUMBER;
   x_MONTO_PESOS             NUMBER;
   x_tasa_tranf_futuro       NUMBER;
   x_TIPO_ESTRATEGIA         VARCHAR2 (4);
   x_UNIDAD_ORGANIZACIONAL   VARCHAR2 (2);
   x_MARCA_CONTABLE          VARCHAR2 (1);
   x_estado_operacion        VARCHAR2 (1);
   x_fwd_usd_uf_comp_2       VARCHAR2 (30) := 'FWD USD/CLF com';   -- obsoleta
   x_fwd_usd_uf_comp         VARCHAR2 (30) := 'CH_FWD_USD_CLF_CLP';
   x_fwd_arb_com             VARCHAR2 (30) := 'CH_FWD_ARB_COM';
   x_fwd_usd_uf_efis         VARCHAR2 (30) := 'CH_FWD_USD/CLF';
   x_fxSwapS                 VARCHAR2 (30) := 'CH_FX_SWAP'; -- obsoleta ESTO PARA IGUALAR DESARROLLO Y PRODUCCION
   x_fxSwap                  VARCHAR2 (30) := 'CH_FX_SWAP'; -- ESTO PARA IGUALAR DESARROLLO Y PRODUCCION
   x_USD_CLP                 VARCHAR2 (30) := 'FWD USD/CLP';       -- obsoleta
   x_CLP_CLF                 VARCHAR2 (30) := 'FWD CLP/CLF';       -- obsoleta
   x_fwd_usd_uf_comp_1       VARCHAR2 (30) := 'FWD USD/CLF com 1'; -- obsoleta
   x_fwd_usd_uf_efis_1       VARCHAR2 (30) := 'FWD USD/CLF 1';     -- obsoleta
   x_fwd_acotado             VARCHAR2 (30) := 'CH_FWD_ACOTADO';
   x_base_tasfut             VARCHAR2 (1);
   x_bloqueo_oper            VARCHAR2 (1);
   x_fecha_proceso_base      DATE;
   x_concepto_comercio       VARCHAR2 (3);
   x_vamos_vienen_pago       VARCHAR2 (1);
   x_vamos_vienen_reem       VARCHAR2 (1);
   x_valor                   NUMBER;
   x_area_contable           VARCHAR2 (2);
   x_paridad_dolar_DM        NUMBER;
   x_precio_dolar_tasa       NUMBER;
   x_DESC_AREA_CONTABLE      VARCHAR2 (36);
   x_IND_ACT_LIN_FUT         VARCHAR2 (1);
   x_IND_CURSE               VARCHAR2 (1);
   x_IND_EXEDE_MARGEN        VARCHAR2 (1);
   x_riesgo_equivalente      NUMBER;
   x_paridad_remanente       NUMBER;
   x_FPAGO_COMPENSACION      DATE;
   x_valor_inicial           NUMBER;
   x_valor_final             NUMBER;
   x_valor_uf                NUMBER;
   x_dias_efectivos          NUMBER;
   x_tib                     NUMBER;
   x_ton                     NUMBER;
   x_dd_vta_mx               NUMBER;
   x_dd_vta_mn               NUMBER;
   x_invertido               VARCHAR2 (1);
   fp                        NUMBER;
   fd                        NUMBER;
   x_concepto_tran           NUMBER;
   x_PRECIO_MN_TASA          NUMBER;
   x_PRECIO_MN_TASA_ESP      NUMBER;
--   x_PRECIO_MX_TASA          NUMBER;
   x_PRECIO_MX_TASA_ESP      NUMBER;
   x_paridad_cliente         NUMBER;
   x_paridad_moneda_tx       NUMBER;
   x_precio_contable         NUMBER;
   x_moneda_pago_pag         VARCHAR2 (3);
   x_moneda_pago_rec         VARCHAR2 (3);
   x_utilidad_devengo        NUMBER;
   x_utilidad_resultado      NUMBER;
   x_ind_ficticio            NUMBER;
   x_factor_transferencia    NUMBER;
   x_valuta_mop              DATE;
   x_valuta_mtx              DATE;
   x_forma_tasa_pag          VARCHAR2 (1);
   x_tipo_cliente            VARCHAR2 (1);
   x_c_mt300                 VARCHAR2 (1);
   x_fecha_aux               DATE;
   x_cliente                 VARCHAR2 (10);
   x_dias_mop                NUMBER;
   x_dias_mtx                NUMBER;
   banco                     VARCHAR2 (10) := '97036000-K';
   corredora                 VARCHAR2 (10) := '96683200-2';
   agencia                   VARCHAR2 (10) := '96623460-1';
   investment                VARCHAR2 (10) := '96556210-9';
   x_messageID               VARCHAR2 (255);
   x_pagador_acreditar       VARCHAR2 (35);
   x_gestor                  NUMBER;

   x_PRECIO_MX_TASA          NUMBER:=  :new.PRECIO_MX_TASA;  -- 12/11/2013 ACM : Se asigna valor inicial FXRate de la operación para tasa_transferencia. 


   
BEGIN


   -- Aqui va la logica de la grabacion de la operacion
   -- para mensajes distintos debe modificarse este trigger o crear otro
   -- Despues se puede reemplazar por la logica de los triggers
   -- pide numero de operacion para los nuevos registros.
   -- controla bloqueo de operaciones para no publicar por efectos del devengo
   BEGIN
      SELECT   bloqueo_operaciones, fecha
        INTO   x_bloqueo_oper, x_fecha_proceso_base
        FROM   ops$mesacam.control_procesos
       WHERE   empresa = :NEW.empresa AND mesa = 'MDX';

      IF x_bloqueo_oper = 'B'
      THEN
         msj := 'Sistema sun bloqueado';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      END IF;

      IF :NEW.fecha_operacion <> x_fecha_proceso_base
         AND:NEW.estado_tibco <> 'DELET'
      THEN
         msj := 'Fecha operacion distinta a fecha de proceso';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      END IF;

      IF TRUNC (SYSDATE) <> x_fecha_proceso_base
      THEN
         msj := 'Fecha de proceso distinta a fecha calendario';
      --************************************************
      --********  Sacar al llevar a produccion
      --************************************************
                ops$mesacam.m_p(:NEW.empresa,:NEW.id_operacion,msj,idtrigger,smj);
                raise salida;
      --************************************************
      --************************************************
      --************************************************
      END IF;

      -- filtro para Fwd Acotado (Solo CLP/USD).
      IF NVL (:new.desc_area_contable, 'X') IN (x_fwd_acotado)
         AND (:new.moneda_operacion NOT IN ('CLP', 'USD')
              OR:new.moneda_transaccion NOT IN ('CLP', 'USD'))
         AND:NEW.estado_tibco <> 'DELET'
      THEN
         msj :=
            'Operacion Fwd Acotado no acepta monedas distintas de USD/CLP ('
            || :new.desc_area_contable
            || ')';
         ops$mesacam.m_d (msj, idtrigger, smj);
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      END IF;

      -- filtro UF/USD como op.normal.
      IF NVL (:new.desc_area_contable, 'X') NOT IN
               (x_fwd_usd_uf_comp,
                x_fwd_usd_uf_efis,
                x_fwd_usd_uf_comp_1,
                x_fwd_usd_uf_efis_1,
                x_fwd_usd_uf_comp_2)
         AND (:new.moneda_operacion IN ('UF', 'CLF')
              OR:new.moneda_transaccion IN ('UF', 'CLF'))
      THEN
         msj :=
               'Operacion en UF no viene como estructura ('
            || :new.desc_area_contable
            || ')';
         ops$mesacam.m_d (msj, idtrigger, smj);
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      END IF;

      -- estructura de UF / $
      IF NVL (:new.moneda_operacion, 'CLP') IN ('CLP', 'CLF', 'UF')
         AND NVL (:new.moneda_transaccion, 'CLP') IN ('CLP', 'CLF', 'UF')
      THEN
         msj :=
               'Operacion UF/Pesos no ingresada como Non Delivery ('
            || NVL (:new.moneda_transaccion, 'CLP')
            || '/'
            || NVL (:new.moneda_operacion, 'CLP')
            || ')';
         ops$mesacam.m_d (msj, idtrigger, smj);
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      END IF;

      -- control de messageID
      BEGIN
         SELECT   ROWID
           INTO   x_messageID
           FROM   operaciones_tibco
          WHERE       fecha_operacion = x_fecha_proceso_base
                  AND messageID = :NEW.messageId
                  AND ROWID <> :NEW.ROWID;

         msj :=
            'Operacion no creada. Mensaje correspondiente ya fue procesado. ('
            || :new.messageID
            || ')';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      EXCEPTION
         WHEN TOO_MANY_ROWS
         THEN
            msj :=
               'Operacion no creada. Mensaje correspondiente procesado mas de una vez. ('
               || :new.messageID
               || ')';
            ops$mesacam.m_p (:NEW.empresa,
                             :NEW.id_operacion,
                             msj,
                             idtrigger,
                             smj);
            RAISE salida;
         WHEN NO_DATA_FOUND
         THEN
            NULL;
         WHEN OTHERS
         THEN
            msj := 'Control MessageID. ' || SQLERRM;
            ops$mesacam.m_d (msj, idtrigger, smj);
            RAISE salida;
      END;
   EXCEPTION
      WHEN salida
      THEN
         --
         msj :=
               'Control bloqueo = ('
            || x_bloqueo_oper
            || ') ('
            || x_fecha_proceso_base
            || ') '
            || :NEW.id_operacion
            || ' '
            || :new.empresa;
         ops$mesacam.m_d (msj, idtrigger, smj);
         --
         RAISE salida;
      WHEN OTHERS
      THEN
         --
         msj := 'others en Control bloqueo: ' || SQLERRM;
         ops$mesacam.m_d (msj, idtrigger, smj);
         msj :=
               'others en Control bloqueo: ('
            || x_bloqueo_oper
            || ') ('
            || x_fecha_proceso_base
            || ')'
            || :NEW.id_operacion
            || ' '
            || :new.empresa;
         ops$mesacam.m_d (msj, idtrigger, smj);
         msj := '**** Error de sistema en el Control de Bloqueo. ****';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
   --
   END;

   -- graba el numero de operacion del sistema de la mesa
   msj := 'Inicio trigger';
   ops$mesacam.m_d (msj, idtrigger, smj);
   --
   -- definimos la empresa (SUN ==> entrega el rut)
   x_empresa := ops$mesacam.t_empresa (:NEW.empresa_origen, 'SUN');

   -- Define portafolio para sun.
   x_portafolio := :NEW.direccion;
   x_id_trader := :NEW.ID_TRADER;
   x_TIPO_ESTRATEGIA := NVL (:NEW.TIPO_ESTRATEGIA, 'LNP');
   x_UNIDAD_ORGANIZACIONAL := NVL (:NEW.UNIDAD_ORGANIZACIONAL, 'TG');
   x_MARCA_CONTABLE := 'T';                                    -- por definir.
   ops$mesacam.t_codigo_area (x_empresa,
                              'SUN',
                              x_codigo_area_ing,
                              x_portafolio,
                              x_ID_TRADER,
                              'MDX',
                              x_TIPO_ESTRATEGIA,
                              x_UNIDAD_ORGANIZACIONAL,
                              x_MARCA_CONTABLE);

   IF x_TIPO_ESTRATEGIA = 'X' AND :NEW.estado_tibco <> 'DELET'
   THEN
      msj :=
            'Error de portafolio ('
         || x_portafolio
         || '). Revisar tabla (y T_CODIGO_AREA). ('
         || :new.id_operacion
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);
      msj := 'Error de portafolio. Posiblemente no definido en tablas de sun';
      ops$mesacam.m_p (:NEW.empresa,
                       :NEW.id_operacion,
                       msj,
                       idtrigger,
                       smj);
      RAISE salida;
   END IF;

   BEGIN
      SELECT   codigo_area
        INTO   x_codigo_area
        FROM   ops$mesacam.nivel_trader
       WHERE   trader = x_id_trader;
   EXCEPTION
      WHEN OTHERS
      THEN
         msj :=
               'No existe trader ('
            || x_id_trader
            || ')  codigo area en 7 por default';
         ops$mesacam.m_d (msj, idtrigger, smj);
         x_codigo_area := 7;
   END;

   folio := :NEW.id_operacion;

   --Obtengo el id_cliente :
   BEGIN
      SELECT   id_cliente, clase_cliente, tipo_cliente
        INTO   x_id_cliente, x_clase_cliente, x_tipo_cliente
        FROM   ops$mesacam.clientes
       WHERE   id_cliente_rut = :NEW.id_cliente_rut;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- graba el numero de operacion del sistema de la mesa
         msj :=
               'Error rut ('
            || :NEW.ID_CLIENTE_RUT
            || ') op=>'
            || folio
            || ';'
            || SUBSTR (SQLERRM, 1, 150);
         ops$mesacam.m_d (msj, idtrigger, smj);

         --
         IF :NEW.estado_tibco <> 'DELET'
         THEN
            -- graba el numero de operacion del sistema de la mesa
            msj :=
                  'Error rut ('
               || :NEW.ID_CLIENTE_RUT
               || ') op=>'
               || folio
               || ';'
               || SUBSTR (SQLERRM, 1, 150);
            ops$mesacam.m_d (msj, idtrigger, smj);
            --
            msj := 'Error al tratar de obtener datos de cliente';
            ops$mesacam.m_p (:NEW.empresa,
                             :NEW.id_operacion,
                             msj,
                             idtrigger,
                             smj);
            RAISE salida;
         ELSE
            -- graba el numero de operacion del sistema de la mesa
            msj :=
                  'Delete, Por lo tanto sigue a pesar del error. Error rut ('
               || :NEW.ID_CLIENTE_RUT
               || ') op=>'
               || folio
               || ';'
               || SUBSTR (SQLERRM, 1, 150);
            ops$mesacam.m_d (msj, idtrigger, smj);
         --
         END IF;
   END;

   -- determina cuando es TCD,TVD,TAC o TAV y cuando es para TF.
   x_tipo_operacion := :NEW.tipo_operacion;
   x_area_contable := NULL;

   IF     :NEW.tipo_operacion IN ('CD', 'VD', 'AC', 'AV')
      AND x_empresa = '97036000-K'
      AND:NEW.cod_destino <> 174
   THEN
      x_tipo_operacion := 'T' || :NEW.tipo_operacion;
      -- aqui para TF.
      x_origin := 'MUREX';
      x_gestor := 0;

      IF FALSE
      THEN
         x_area_contable := 'S';
      END IF;
   ELSIF :NEW.tipo_operacion IN ('CD', 'VD', 'AC', 'AV') AND x_empresa = '97036000-K' AND NVL (x_tipo_cliente, 'X') NOT IN ('I', 'B', 'K')
   THEN
      x_tipo_operacion := 'T' || :NEW.tipo_operacion; -- caso especial corredora
   ELSIF :NEW.tipo_operacion IN ('CF', 'VF')
   THEN
      x_area_contable := 'DO';
   END IF;

   --  Caso especial Agencia, Sucursal <> 181
   IF :NEW.tipo_operacion IN
            ('CD', 'VD', 'AC', 'AV', 'TCD', 'TVD', 'TAC', 'TAV')
      AND x_empresa = '96623460-1'
      AND:NEW.cod_destino NOT IN (174)
   THEN
      x_origin := 'MUREX';
   END IF;

   IF NVL (folio, 0) = 0
   THEN
      -- si NEW.id_operacion es cero, no puedo insertar en la tabla.
      --       msj := '**** Error de sistema. Folio operacion entregado en cero por adaptadores. ****';
      --       ops$mesacam.m_p(:NEW.empresa,:NEW.id_operacion,msj,idtrigger,smj);
      RAISE salida;
   ELSE
      -- valido que la operacion no exista.
      -- si existe,  debo ver que se trate de una anulacion
      -- si no es anulacion, se trata de un mensaje relacionado que requiere otro
      -- tratamiento
      existe_op := 'N';

      BEGIN
         SELECT   id_operacion
           INTO   auxdummy
           FROM   ops$mesacam.operaciones
          WHERE   id_operacion = folio AND empresa = x_empresa;

         --
         msj := 'Existe op=>' || folio || '; emp ' || x_empresa || ';';
         ops$mesacam.m_d (msj, idtrigger, smj);
         --
         existe_op := 'S';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            --
            IF :NEW.estado_tibco <> 'DELET'
            THEN
               --
               msj :=
                     'no Existe op=>'
                  || folio
                  || ';  emp '
                  || x_empresa
                  || '; sigo';
               ops$mesacam.m_d (msj, idtrigger, smj);
            --
            ELSE
               -- graba el numero de operacion del sistema de la mesa
               msj :=
                  'Error. Se trata de borra una operacion inexistente en sun. op=>'
                  || folio
                  || '; Empresa '
                  || :new.empresa;
               ops$mesacam.m_d (msj, idtrigger, smj);
               --
               ops$mesacam.m_p (:NEW.empresa,
                                :NEW.id_operacion,
                                msj,
                                idtrigger,
                                smj);
               RAISE salida;
            END IF;
         WHEN OTHERS
         THEN
            -- graba el numero de operacion del sistema de la mesa
            msj := 'Error op=>' || folio || ';';
            msj := msj || SQLERRM;
            ops$mesacam.m_d (msj, idtrigger, smj);
            --
            msj := '**** Error de sistema al tratar de borrar operacion. ****';
            ops$mesacam.m_p (:NEW.empresa,
                             :NEW.id_operacion,
                             msj,
                             idtrigger,
                             smj);
            RAISE salida;
      END;
   END IF;

   -- verifico si existe operacion o no.
   IF existe_op = 'S'
   THEN
      -- verifico si es cancelacion o no.
      IF :NEW.estado_tibco = 'DELET' AND :NEW.estado_cdm = 'CAN'
      THEN
         BEGIN
            --
            msj :=
                  'Es anulacion de op=>'
               || folio
               || ';  emp '
               || x_empresa
               || '; sigo';
            ops$mesacam.m_d (msj, idtrigger, smj);

            SELECT   fecha_operacion
              INTO   x_fecha_aux
              FROM   ops$mesacam.operaciones
             WHERE   id_operacion = folio AND empresa = x_empresa;

            IF x_fecha_aux <> x_fecha_proceso_base
            THEN
               msj :=
                  'Error, fecha de operacion a eliminar distinta a fecha de proceso. Oper. ('
                  || folio
                  || ') fecha de operacion ('
                  || x_fecha_aux
                  || ')  fecha de proceso ('
                  || x_fecha_proceso_base
                  || ')';
               ops$mesacam.m_d (msj, idtrigger, smj);
               msj :=
                  'No se puede eliminar operaciones con fecha de operacion distinta a fecha de proceso';
               ops$mesacam.m_p (:NEW.empresa,
                                :NEW.id_operacion,
                                msj,
                                idtrigger,
                                smj);
            ELSE
               UPDATE   ops$mesacam.operaciones
                  SET   estado_operacion = 'X',
                        id_usuario_act = x_id_trader,
                        act_tibco = 'S',
                        fecha_act = SYSDATE
                WHERE   id_operacion = folio AND empresa = x_empresa;

               IF SQL%ROWCOUNT = 0                              --sql%NOTFOUND
               THEN
                  -- graba el numero de operacion del sistema de la mesa
                  msj :=
                        'Error, operacion a eliminar =>('
                     || folio
                     || ') no existe;';
                  ops$mesacam.m_d (msj, idtrigger, smj);
                  msj :=
                     '**** Error de sistema, operacion a eliminar no existe ****';
                  ops$mesacam.m_p (:NEW.empresa,
                                   :NEW.id_operacion,
                                   msj,
                                   idtrigger,
                                   smj);
               --
               END IF;

               -- Motor de Pagos Eliminacion
               BEGIN
                  msj :=
                        'Actualiza motor de pagos eliminacion ('
                     || folio
                     || ')  empresa ('
                     || x_empresa
                     || ')';
                  ops$mesacam.m_d (msj, idtrigger, smj);
                  ops$mesacam.Pr_motor_pagos_mx (folio,
                                                 x_empresa,
                                                 'E',
                                                 'M',
                                                 retorno_rutinas);

                  IF (retorno_rutinas <> 0)
                  THEN
                     msj :=
                        'Error actualiza motor de pagos eliminacion. Error ('
                        || retorno_rutinas
                        || ')  op. ('
                        || folio
                        || ')  empresa ('
                        || x_empresa
                        || ')';
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     msj := 'Error al actualizar motor de pagos. Eliminacion';
                     ops$mesacam.m_d (msj, idtrigger, smj);
                     msj := SQLERRM;
                     ops$mesacam.m_d (msj, idtrigger, smj);
               END;
            --
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               -- graba el numero de operacion del sistema de la mesa
               msj := 'Error cancelacion de op=>' || folio || ';';
               msj := msj || SQLERRM;
               ops$mesacam.m_d (msj, idtrigger, smj);
               --
               msj :=
                  '**** Error de sistema al tratar de cancelar operacion. ****';
               ops$mesacam.m_p (:NEW.empresa,
                                :NEW.id_operacion,
                                msj,
                                idtrigger,
                                smj);
               RAISE salida;
         END;
      ELSE
         --
         msj :=
               'Existe op=>'
            || folio
            || '; emp '
            || x_empresa
            || '; y no es cancelacion, no hago nada';
         ops$mesacam.m_d (msj, idtrigger, smj);
      --
      END IF;
   ELSE                                          -- operacion no existe en sun
      -- Calcula campos de la base

      x_moneda_operacion := :new.moneda_operacion;
      x_moneda_transaccion := :new.moneda_transaccion;

      -- precio_financiero
      IF x_tipo_operacion NOT IN ('CD', 'VD', 'TCD', 'TVD')
      THEN
         x_precio_financiero := :NEW.PRECIO_CONTABLE;
      ELSE
         x_precio_financiero := 0;
      END IF;

      --
      -- concepto_comercio (Moneda Liquidacion para CF, VF)
      IF :NEW.concepto_comercio IS NULL
      THEN
         IF x_tipo_operacion IN ('CF', 'VF')
         THEN
            IF ops$mesacam.f_grupo_invest (x_empresa) = 'S'
               AND x_id_cliente IN ('11-1', 'SAN-MADRID')
            THEN
               x_concepto_comercio := 'USD';
            ELSE
               x_concepto_comercio := 'CLP';
            END IF;
         ELSE
            x_concepto_comercio := NULL;
         END IF;
      ELSE
         x_concepto_comercio := :NEW.concepto_comercio;
      END IF;

      -- graba el numero de operacion del sistema de la mesa
      msj :=
            'Antes del producto estructura('
         || :new.desc_area_contable
         || ') tasa_futuro('
         || :new.paridad_cliente
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);

      --
      -- tib y ton
      BEGIN
         SELECT   valor
           INTO   x_tib
           FROM   ops$mesacam.parametros
          WHERE   id_parametro = 'TIB' AND fecha = x_fecha_proceso_base;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_tib := 0;
      END;

      BEGIN
         SELECT   valor
           INTO   x_ton
           FROM   ops$mesacam.parametros
          WHERE   id_parametro = 'TON' AND fecha = x_fecha_proceso_base;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_ton := 0;
      END;

      -- si el producto es compensacion esto vale 0
      IF x_tipo_operacion IN ('CF', 'VF')
      THEN
         x_dd_vta_mx := 0;
         x_dd_vta_mn := 0;
      ELSE
         x_dd_vta_mx :=
            NVL (:new.Valuta_mtx, x_fecha_proceso_base)
            - x_fecha_proceso_base;
         x_dd_vta_mn :=
            NVL (:new.Valuta_mop, x_fecha_proceso_base)
            - x_fecha_proceso_base;
      END IF;

      fp := x_tib / 3000;
      fd := x_ton / 36000;
      -- Ajuste al tipo de operacion por ser spot del banco. Esto debe ir antes de este insert
      x_paridad_operacion := :new.precio_contable;

      IF x_tipo_operacion IN ('CD', 'VD', 'TCD', 'TVD')
      THEN
         x_monto_operacion := ROUND (:new.monto_operacion, 0);
      ELSE
         x_monto_operacion := :new.monto_operacion;
      END IF;

      x_precio_spot := :new.precio_spot;
      x_paridad_transaccion := :NEW.paridad_transaccion;
      x_precio_dolar_tasa := :new.precio_dolar_tasa;
      x_tasa_futuro := :new.tasa_futuro;

      IF x_tipo_operacion IN ('CD', 'VD', 'TCD', 'TVD')
      THEN
         x_tipo_cambio_dolar := x_paridad_transaccion;
      ELSE
         ops$mesacam.t_leer_val_param ('DOB',
                                       x_fecha_proceso_base,
                                       x_tipo_cambio_dolar,
                                       'BC',
                                       2);
      END IF;

      --
      IF x_tipo_operacion IN ('TCD', 'TAC', 'CD', 'AC', 'AFC', 'CF')
      THEN
         x_ind_ficticio := 0;
      ELSE
         x_ind_ficticio := 1;
      END IF;

      x_tasa_tranf_futuro := 0;

      IF x_tipo_operacion IN ('TCD', 'TVD')
      THEN
         x_paridad_operacion := x_paridad_transaccion;
         x_precio_contable := x_tipo_cambio_dolar;

         IF NVL (:new.c_mt300, 'X') = 'W'
         THEN
            x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mx_tasa;
         ELSE
            x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mn_tasa;
         END IF;
      ELSIF x_tipo_operacion IN ('CD', 'VD')
      THEN
         x_paridad_operacion := x_paridad_transaccion;
         x_precio_contable := x_tipo_cambio_dolar;

         IF x_empresa = '96623460-1'
         THEN                                                       -- agencia
            IF NVL (:new.c_mt300, 'X') = 'W'
            THEN
               x_precio_dolar_tasa :=
                  x_paridad_transaccion - :new.precio_mx_tasa;
            ELSE
               x_precio_dolar_tasa :=
                  x_paridad_transaccion - :new.precio_mn_tasa;
            END IF;
         --                x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mn_tasa;
         ELSE
            x_precio_dolar_tasa := NULL;
         END IF;
      ELSIF x_tipo_operacion IN ('TAC', 'TAV')
      THEN
         x_paridad_operacion := 1;
         ops$mesacam.t_leer_val_param (x_moneda_transaccion,
                                       x_fecha_proceso_base,
                                       x_paridad_moneda_tx,
                                       'MO',
                                       x_ind_ficticio);
         x_precio_contable := x_tipo_cambio_dolar / x_paridad_moneda_tx;

         IF NVL (:new.c_mt300, 'X') = 'W'
         THEN
            x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mx_tasa;
         ELSE
            x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mn_tasa;
         END IF;
      --             x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mn_tasa;
      ELSIF x_tipo_operacion IN ('AC', 'AV')
      THEN
         x_paridad_operacion := 1;
         ops$mesacam.t_leer_val_param (x_moneda_transaccion,
                                       x_fecha_proceso_base,
                                       x_paridad_moneda_tx,
                                       'MO',
                                       x_ind_ficticio);
         x_precio_contable := x_tipo_cambio_dolar / x_paridad_moneda_tx;

         IF NVL (:new.c_mt300, 'X') = 'W'
         THEN
            x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mx_tasa;
         ELSE
            x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mn_tasa;
         END IF;
      --             x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mn_tasa;
      ELSIF x_tipo_operacion IN ('AFC', 'AFV')
      THEN
         x_paridad_operacion := 1;
         ops$mesacam.t_leer_val_param (x_moneda_transaccion,
                                       x_fecha_proceso_base,
                                       x_paridad_moneda_tx,
                                       'MO',
                                       x_ind_ficticio);
         x_precio_contable := x_tipo_cambio_dolar / x_paridad_moneda_tx;

         IF NVL (:new.c_mt300, 'X') = 'W'
         THEN
            x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mx_tasa;
         ELSE
            x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mn_tasa;
         END IF;
      --             x_precio_dolar_tasa := x_paridad_transaccion - :new.precio_mn_tasa;
      ELSIF x_tipo_operacion IN ('CF', 'VF')
      THEN                                                   --- Siempre es DO
         --             x_tasa_tranf_futuro   := x_paridad_transaccion - :new.precio_mn_tasa;
         IF NVL (:new.c_mt300, 'X') = 'W'
         THEN
            x_tasa_tranf_futuro := x_paridad_transaccion - :new.precio_mx_tasa;
         ELSE
            x_tasa_tranf_futuro := x_paridad_transaccion - :new.precio_mn_tasa;
         END IF;

         x_paridad_operacion := x_tipo_cambio_dolar;
         x_paridad_transaccion := x_tasa_futuro;
         x_precio_dolar_tasa := 0;
         x_monto_operacion :=
            ROUND (:NEW.monto_dolar * x_tipo_cambio_dolar, 0);
         x_precio_contable := x_tipo_cambio_dolar;
      ELSE
         msj := 'x_tipo_operacion (' || x_tipo_operacion || ')';
         ops$mesacam.m_d (msj, idtrigger, smj);
         x_precio_contable := 1;
      END IF;

      --
      x_precio_contable := ROUND (x_precio_contable, 2);
      -- Precios transferencia
      msj :=
            'Tipo Op.('
         || x_tipo_operacion
         || ') Moneda Transaccion ('
         || x_moneda_transaccion
         || ') paridad_tx ('
         || x_paridad_transaccion
         || ') margen ('
         || :new.precio_mn_tasa
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);
      --
      x_precio_financiero :=
         ROUND (
            x_PRECIO_CONTABLE * (1 - x_dd_vta_mn * fp + x_dd_vta_mx * fd),
            2
         );
      x_monto_pesos := ROUND (:NEW.monto_dolar * x_tipo_cambio_dolar, 0);

      --
      IF x_tipo_operacion IN ('TCD', 'TVD', 'TAC', 'TAV')
         AND (NVL (:new.desc_area_contable, 'X') NOT IN (x_fxSwapS, x_fxSwap))
      THEN
         x_ID_PRODUCTO := 'EMP_NAC';
         x_estado_operacion := 'C';
      ELSIF x_tipo_operacion IN ('CD', 'VD', 'AC', 'AV') AND (NVL (:new.desc_area_contable, 'X') NOT IN (x_fxSwapS, x_fxSwap))
      THEN
         x_ID_PRODUCTO := x_tipo_operacion;
         x_estado_operacion := 'C';
      ELSIF x_tipo_operacion IN ('CD', 'VD', 'AC', 'AV') AND (:new.desc_area_contable IN (x_fxSwapS, x_fxSwap))
      THEN
         x_ID_PRODUCTO := x_tipo_operacion;
         x_estado_operacion := 'C';                      -- Caso Spot del swap
      ELSIF x_tipo_operacion IN ('TCD', 'TVD', 'TAC', 'TAV') AND (:new.desc_area_contable IN (x_fxSwapS, x_fxSwap))
      THEN
         x_ID_PRODUCTO := 'EMP_NAC';
         x_estado_operacion := 'C';                      -- Caso Spot del swap
      ELSE                                                          -- forward
         IF :new.desc_area_contable IN
                  (x_fwd_usd_uf_comp,
                   x_fwd_usd_uf_comp_1,
                   x_fwd_usd_uf_comp_2)
         THEN
            x_id_producto := 'UFCOMP';
            x_tasa_futuro := :NEW.paridad_cliente;
            x_moneda_operacion := 'CLP';
            x_monto_operacion := x_monto_pesos;
            x_paridad_transaccion := :NEW.paridad_cliente;
            x_tasa_tranf_futuro := :new.precio_mn_tasa_esp;
            x_precio_spot := :new.precio_mx_tasa_esp;
            msj := 'UFCOMP';
            ops$mesacam.m_d (msj, idtrigger, smj);
         ELSIF :new.desc_area_contable IN (x_fwd_usd_uf_efis, x_fwd_usd_uf_efis_1)
         THEN
            x_id_producto := 'UFEFIS';
            x_tasa_futuro := :NEW.paridad_cliente;
            x_moneda_operacion := 'CLP';
            x_monto_operacion := x_monto_pesos;
            x_paridad_transaccion := :NEW.paridad_cliente;
            x_tasa_tranf_futuro := :new.precio_mn_tasa_esp;
            x_precio_spot := :new.precio_mx_tasa_esp;
            msj := 'UFEFIS';
            ops$mesacam.m_d (msj, idtrigger, smj);
         ELSIF :new.desc_area_contable = x_fwd_arb_com
         THEN
            x_id_producto := 'COMP';
            x_tasa_futuro := :NEW.paridad_cliente;
            msj := 'COMP';
            ops$mesacam.m_d (msj, idtrigger, smj);
         ELSE                              -- validar aqui los otros Forwards.
            BEGIN
               IF x_tipo_operacion IN ('CF', 'VF')
               THEN                   --- Corregir cuando se determone DO o DM
                  ops$mesacam.t_leer_val_param ('USD',
                                                x_fecha_proceso_base,
                                                x_paridad_dolar_DM,
                                                'BC',
                                                2);
               ELSIF x_tipo_operacion IN ('AFC', 'AFV')
               THEN
                  NULL;
               END IF;

               x_ID_PRODUCTO := :NEW.ID_PRODUCTO;
               x_tasa_futuro := :NEW.tasa_futuro;
               msj := 'Ninguno ' || :new.desc_area_contable;
               ops$mesacam.m_d (msj, idtrigger, smj);
            END;
         END IF;

         x_estado_operacion := 'C';
      END IF;

      -- Modificacion el id_producto para arbitraje forward
      --      if x_tipo_operacion in ('AFC','AFV') then
      --         if x_id_producto in ('EFIS','COMP') then
      --            if nvl(x_tipo_cliente,'X') in ('I','B','K') and x_empresa = '97036000-K' then
      --               x_id_producto := x_id_producto || 'CP';
      --            else
      --               x_id_producto := x_id_producto || 'CC';
      --            end if;
      --            if x_clase_cliente in ('EE','EX') then
      --               x_id_producto := 'ME'||x_id_producto;
      --            else
      --               x_id_producto := 'ML'||x_id_producto;
      --            end if;
      --         end if;
      --      end if;


      ----- Modificacion para TF tipo de operacion
      --  Caso especial agencia,
      IF     :NEW.tipo_operacion IN ('CD', 'VD', 'AC', 'AV')
         AND x_empresa = '96623460-1'
         AND x_origin IN ('MUREX', 'NOCOMEX')
      THEN                  --:NEW.cod_destino = 181 Y <> 181 _ EVB 2013-01-17
         IF :NEW.tipo_operacion IN ('CD', 'VD', 'AC', 'AV')
         THEN
            x_estado_operacion := 'M';
         END IF;
      END IF;



      ----- Modificacion Marcotito para llegada correcta del producto para GAP
      IF x_tipo_operacion IN ('AFC', 'AFV')
      THEN
         IF x_id_producto IN ('EFIS')
         THEN
            IF NVL (x_tipo_cliente, 'X') IN ('I', 'B', 'K')
               AND x_empresa = '97036000-K'
            THEN
               x_id_producto := x_id_producto || 'CP';
            ELSE
               x_id_producto := x_id_producto || 'CC';
            END IF;

            IF x_clase_cliente IN ('EE', 'EX')
            THEN
               x_id_producto := 'ME' || x_id_producto;
            ELSE
               x_id_producto := 'MLEFISCC';
            END IF;
         ELSE
            x_id_producto := 'MLCOMPCC';
         END IF;
      END IF;

      ------
      -- Trae base tasfut
      x_base_tasfut := NULL;

      IF x_tipo_operacion IN ('CF', 'VF')
      THEN
         BEGIN
            SELECT   ind_tc_uf
              INTO   x_base_tasfut
              FROM   ops$mesacam.sub_operacion
             WHERE   tipo_operacion = x_tipo_operacion
                     AND sub_operacion = x_id_producto;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_base_tasfut := 'N';
         END;
      END IF;

      --
      -- Fecha Vencimiento nula? fecha de compensacion
      --
      msj :=
            'Fecha Vencimiento ('
         || TO_CHAR (:NEW.Fecha_vencimiento)
         || ') valuta_mop ('
         || TO_CHAR (:NEW.valuta_mop)
         || ') valuta_mtx ('
         || TO_CHAR (:NEW.valuta_mtx)
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);
      --
      x_fecha_vencimiento := :NEW.Fecha_vencimiento;
      x_fecha_inicio := :NEW.fecha_inicio;

      IF x_tipo_operacion IN ('CF', 'VF', 'AFC', 'AFV')
      THEN
         IF x_fecha_vencimiento IS NULL
         THEN
            IF :new.valuta_mop IS NOT NULL
            THEN
               IF :new.valuta_mtx IS NOT NULL
               THEN
                  IF :new.valuta_mop > :new.valuta_mtx
                  THEN
                     x_fecha_vencimiento := :new.valuta_mtx;
                  ELSE
                     x_fecha_vencimiento := :new.valuta_mop;
                  END IF;
               ELSE
                  x_fecha_vencimiento := :new.valuta_mop;
               END IF;
            ELSE
               IF :new.valuta_mtx IS NOT NULL
               THEN
                  x_fecha_vencimiento := :new.valuta_mtx;
               END IF;
            END IF;
         ELSE
            msj := 'Fecha Vencimiento no nula';
            ops$mesacam.m_d (msj, idtrigger, smj);
         END IF;

         x_fecha_compensacion := x_fecha_vencimiento;
      ELSE
         x_fecha_vencimiento := :NEW.fecha_operacion;
         x_fecha_compensacion := :NEW.fecha_operacion;
         x_fecha_inicio := :NEW.fecha_operacion;
      END IF;

      --
      msj := 'x_fecha_vencimiento (' || TO_CHAR (x_fecha_vencimiento) || ') ';
      ops$mesacam.m_d (msj, idtrigger, smj);
      --
      --
      -- documento_pago_mop ; documento_pago_mtx
      --
      msj :=
            'Antes DocPagos : MonRec ('
         || ') pago_mtx ('
         || :NEW.documento_pago_mtx
         || ') pago_MOP ('
         || :NEW.documento_pago_mop
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);

      --
      --  Formas de Pago
      --
      x_documento_pago_mtx := :NEW.documento_pago_mtx;
      x_documento_pago_mop := :NEW.documento_pago_mop;

      IF x_documento_pago_mtx = 'MA'
      THEN
         x_documento_pago_mtx := 'VV';
      END IF;

      IF x_documento_pago_mop = 'MA'
      THEN
         x_documento_pago_mop := 'VV';
      END IF;

      x_moneda_pago_pag := :NEW.moneda_pago_pag; -- Viene moneda para docto pago (2)
      x_moneda_pago_rec := :NEW.moneda_pago_rec; -- Viene moneda para docto pago (1)

      IF NVL (x_tipo_cliente, 'X') IN ('I', 'B', 'K')
      THEN
         x_cliente := 'BANCO';  -- extranjero o nacional (Incluye financieras)
      ELSIF x_clase_cliente IN ('EE', 'EX')
      THEN
         x_cliente := 'EX'; -- extranjero no banco (puede ser AFP o corredora extranjera)
      ELSIF x_clase_cliente IN ('AF')
      THEN
         x_cliente := 'AFP';                            -- Solo APF nacionales
      ELSIF x_clase_cliente IN ('CO')
      THEN
         x_cliente := 'CORR';                    -- Solo corredoras nacionales
      ELSE
         x_cliente := 'OTRO';                  -- Nacionales, no bancos ni AFP
      END IF;

      -- Corregir para el caso de dias habiles.
      x_dias_mop :=
         OPS$MESACAM.T_VALUTA_DIAS_HABILES (
            x_fecha_vencimiento,
            NVL (:new.valuta_mop, x_fecha_vencimiento)
         );
      x_dias_mtx :=
         OPS$MESACAM.T_VALUTA_DIAS_HABILES (
            x_fecha_vencimiento,
            NVL (:new.valuta_mtx, x_fecha_vencimiento)
         );

      -- Ajuste por documento pago Crearing
      IF x_moneda_transaccion <> x_moneda_pago_pag
      THEN
         x_invertido := 'S';
      ELSE
         x_invertido := 'N';
      END IF;

      --
      IF x_documento_pago_mtx = 'ZX'
      THEN
         IF x_invertido = 'S'
         THEN
            IF x_dias_mop = 0
            THEN
               x_documento_pago_mtx := 'L0';
            ELSIF x_dias_mop = 1
            THEN
               x_documento_pago_mtx := 'L1';
            ELSE                                             -- Valuta 2 o mas
               x_documento_pago_mtx := 'L2';
            END IF;
         ELSE
            IF x_dias_mtx = 0
            THEN
               x_documento_pago_mtx := 'L0';
            ELSIF x_dias_mtx = 1
            THEN
               x_documento_pago_mtx := 'L1';
            ELSE                                             -- Valuta 2 o mas
               x_documento_pago_mtx := 'L2';
            END IF;
         END IF;
      END IF;

      IF x_documento_pago_mop = 'ZX'
      THEN
         IF x_invertido = 'S'
         THEN
            IF x_dias_mtx = 0
            THEN
               x_documento_pago_mop := 'L0';
            ELSIF x_dias_mtx = 1
            THEN
               x_documento_pago_mop := 'L1';
            ELSE                                             -- Valuta 2 o mas
               x_documento_pago_mop := 'L2';
            END IF;
         ELSE
            IF x_dias_mop = 0
            THEN
               x_documento_pago_mop := 'L0';
            ELSIF x_dias_mop = 1
            THEN
               x_documento_pago_mop := 'L1';
            ELSE                                             -- Valuta 2 o mas
               x_documento_pago_mop := 'L2';
            END IF;
         END IF;
      END IF;

      --
      ops$mesacam.t_documentos_pago ('MDX',
                                     x_tipo_operacion,
                                     x_id_producto,
                                     x_cliente,
                                     x_moneda_operacion,
                                     x_moneda_transaccion,
                                     x_dias_mop,
                                     x_dias_mtx,
                                     x_documento_pago_mop,
                                     x_documento_pago_mtx,
                                     x_moneda_pago_pag,
                                     x_moneda_pago_rec,
                                     retorno_rutinas); -- Cero -> OK  ;  Menor que cero -> Error
      --
      msj :=
            'Despues DocPagos : dias_mop ('
         || x_dias_mop
         || ') dias_mtx ('
         || x_dias_mtx
         || ') pago_mtx ('
         || x_documento_pago_mtx
         || ') pago_MOP ('
         || x_documento_pago_mop
         || ') retorno ('
         || retorno_rutinas
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);
      --
      --x_banco_debitar ; x_banco_acreditar
      ops$mesacam.t_bancos_pagadores (x_tipo_operacion,
                                      x_id_cliente,
                                      x_empresa,
                                      x_moneda_operacion,
                                      x_moneda_transaccion,
                                      x_area_contable,
                                      x_documento_pago_mop,
                                      x_documento_pago_mtx,
                                      x_banco_acreditar,
                                      x_banco_debitar,
                                      x_bco_pagador,
                                      retorno_rutinas -- Cero -> OK  ;  Menor que cero -> Error
                                                     );

      IF retorno_rutinas < 0
      THEN
         x_banco_acreditar := NULL;
         x_banco_debitar := NULL;
         x_bco_pagador := NULL;
         msj :=
               'Bancos Pagadores Debitadores en nulo. Operacion ('
            || x_tipo_operacion
            || ') ('
            || retorno_rutinas
            || ')';
         ops$mesacam.m_d (msj, idtrigger, smj);
      ELSE
         msj :=
               '(Pagador;Acreditar;Debitar) --> ('
            || x_bco_pagador
            || ';'
            || x_banco_acreditar
            || ';'
            || x_banco_debitar
            || ')';
         ops$mesacam.m_d (msj, idtrigger, smj);
      END IF;

      -- tasa_mon_nac ; tasa_mon_ext
      x_tasa_mon_nac := 0;
      x_tasa_mon_ext := 0;
      x_PRECIO_MN_TASA := 0;
      x_PRECIO_MN_TASA_ESP := 0;
      --x_PRECIO_MX_TASA := 0;
      x_PRECIO_MX_TASA_ESP := 0;

      -- Calcula precio promedio ponderado spot.
      --
      IF x_empresa <> '97036000-K' AND x_tipo_operacion IN ('VD')
      THEN
         x_tasa_tranf_futuro :=
            ops$mesacam.f_precio_promedio (x_empresa, :NEW.fecha_operacion);

         IF x_tasa_tranf_futuro = 0
         THEN
            x_tasa_tranf_futuro := x_tipo_cambio_dolar;
         END IF;

         -- arreglo precio dolar tasa para distinto de agencia.
         IF x_empresa IN ('96623460-1', '96683200-2')
         THEN
            NULL;                --x_tasa_tranf_futuro := x_precio_dolar_tasa;
         ELSE                                       -- se trata del investment
            x_precio_dolar_tasa := x_tasa_tranf_futuro;
         END IF;
      END IF;

      msj :=
            'x_tasa_tranf_futuro ('
         || TO_CHAR (x_tasa_tranf_futuro)
         || ')  x_bco_pagador '
         || x_bco_pagador;
      ops$mesacam.m_d (msj, idtrigger, smj);
      --
      -- vamos vienen
      x_vamos_vienen_reem := NULL;

      IF x_tipo_operacion IN ('AFC', 'CF', 'CD', 'TCD', 'AC', 'TAC')
      THEN
         x_vamos_vienen_pago := 'I';
      ELSE
         x_vamos_vienen_pago := 'V';
      END IF;

      --
      -- Limpieza final de campos
      x_IND_ACT_LIN_FUT := NULL;
      x_IND_CURSE := NULL;
      x_IND_EXEDE_MARGEN := NULL;
      x_FPAGO_COMPENSACION := NULL;
      x_moneda_pago_pag := NULL;
      x_moneda_pago_rec := NULL;
      x_paridad_remanente := 0;
      x_valor_inicial := 0;
      x_valor_final := 0;
      x_concepto_tran := NULL;
      x_paridad_cliente := NULL;
      x_forma_tasa_pag := NULL;
      x_tipo_dolar := :NEW.TIPO_DOLAR;

      --
      IF x_tipo_operacion IN ('AV')
      THEN
         x_precio_spot := NULL;
      ELSIF x_tipo_operacion IN ('AC')
      THEN
         x_precio_spot := NULL;
      ELSIF x_tipo_operacion IN ('AFV')
      THEN
         x_precio_spot := NULL;

         IF x_id_producto IN ('MLCOMPCC')
         THEN
            x_paridad_cliente := 0;
         END IF;

         x_PRECIO_MN_TASA := NULL;
         x_PRECIO_MN_TASA_ESP := NULL;
         x_PRECIO_MX_TASA := NULL;
         x_PRECIO_MX_TASA_ESP := NULL;
         x_TASA_MON_EXT := NULL;
         x_TASA_MON_NAC := NULL;
         x_TASA_TRANF_FUTURO := NULL;
         x_riesgo_equivalente := 0;
         x_paridad_remanente := NULL;
      ELSIF x_tipo_operacion IN ('AFC')
      THEN
         x_precio_spot := NULL;

         IF x_id_producto IN ('MEEFISCP')
         THEN
            x_paridad_cliente := 0;
         END IF;

         x_PRECIO_MN_TASA := NULL;
         x_PRECIO_MN_TASA_ESP := NULL;
         x_PRECIO_MX_TASA := NULL;
         x_PRECIO_MX_TASA_ESP := NULL;
         x_TASA_MON_EXT := NULL;
         x_TASA_MON_NAC := NULL;
         x_TASA_TRANF_FUTURO := NULL;
         x_riesgo_equivalente := 0;
         x_paridad_remanente := NULL;
      ELSIF x_tipo_operacion IN ('TAV')
      THEN
         x_precio_spot := NULL;
      ELSIF x_tipo_operacion IN ('TAC')
      THEN
         x_precio_spot := NULL;
      ELSIF x_tipo_operacion IN ('CD')
      THEN
         x_precio_spot := NULL;

         IF x_empresa <> '97036000-K'
         THEN
            IF :new.monto_dolar >= 10000
            THEN
               x_forma_tasa_pag := 'F';
            ELSE
               x_forma_tasa_pag := 'B';
            END IF;
         END IF;

         IF x_empresa in ('96623460-1')
         THEN
            x_PRECIO_MX_TASA := x_precio_dolar_tasa;
         ELSE
            x_PRECIO_MX_TASA := NULL;
         END IF;

         x_PRECIO_MN_TASA_ESP := NULL;
         x_PRECIO_MN_TASA := NULL;
         x_PRECIO_MX_TASA_ESP := NULL;
         x_TASA_MON_EXT := NULL;
         x_TASA_MON_NAC := NULL;
         x_TASA_TRANF_FUTURO := NULL;
         x_TIPO_DOLAR := x_moneda_transaccion;
      ELSIF x_tipo_operacion IN ('VD')
      THEN
         x_precio_spot := NULL;

         IF x_empresa <> '97036000-K'
         THEN
            IF :new.monto_dolar >= 10000
            THEN
               x_forma_tasa_pag := 'F';
            ELSE
               x_forma_tasa_pag := 'B';
            END IF;
         ELSE
            x_TASA_TRANF_FUTURO := NULL;
         END IF;


         IF x_empresa in ('96623460-1')
         THEN
            x_PRECIO_MX_TASA :=  x_precio_dolar_tasa;
         ELSE
            x_PRECIO_MX_TASA := NULL;
         END IF;

         x_PRECIO_MN_TASA_ESP := NULL;
         x_PRECIO_MN_TASA := NULL;
         x_PRECIO_MX_TASA_ESP := NULL;
         x_TASA_MON_EXT := NULL;
         x_TASA_MON_NAC := NULL;
         x_TIPO_DOLAR := x_moneda_transaccion;
      ELSIF x_tipo_operacion IN ('TCD')
      THEN
         x_precio_spot := NULL;
         x_TIPO_DOLAR := x_moneda_transaccion;
      ELSIF x_tipo_operacion IN ('TVD')
      THEN
         x_precio_spot := NULL;
         x_TIPO_DOLAR := x_moneda_transaccion;
      ELSIF x_tipo_operacion IN ('CF')
      THEN
         -- Valor Inicial
         x_dias_efectivos := x_fecha_vencimiento - :new.fecha_operacion;

         IF x_BASE_TASFUT = 'C'
         THEN
            x_Valor_Inicial :=
               ROUND (:new.MONTO_TRANSACCION * x_TIPO_CAMBIO_DOLAR, 0);
         ELSE
            ops$mesacam.t_leer_val_param ('UF',
                                          x_fecha_proceso_base,
                                          x_valor_uf,
                                          'BC',
                                          2);
            x_Valor_Inicial :=
               ROUND ( (x_Dias_Efectivos * x_TIPO_CAMBIO_DOLAR) / x_Valor_UF,
                      4);
         END IF;

         -- Valor Final
         IF x_BASE_TASFUT = 'C'
         THEN
            x_valor_final := ROUND (:new.MONTO_TRANSACCION * x_TASA_FUTURO, 0);
         ELSE
            x_valor_final :=
               ROUND (
                  ( (x_Dias_Efectivos * x_TASA_FUTURO / 36000) + 1)
                  * x_VALOR_INICIAL,
                  4
               );
         END IF;

         IF :new.desc_area_contable IN
                  (x_fwd_usd_uf_comp,
                   x_fwd_usd_uf_comp_1,
                   x_fwd_usd_uf_comp_2,
                   x_fwd_usd_uf_efis,
                   x_fwd_usd_uf_efis_1)
         THEN
            x_Valor_Inicial := ROUND (:new.valor_inicial, 4);
            x_valor_final := ROUND (:new.utilidad_resultado, 4); -- compra sale amount vienen las UF.
         END IF;

         x_riesgo_equivalente := 0;

         IF x_id_producto IN ('MNEFIS', 'MNCOMP')
         THEN
            x_paridad_cliente := 0;
         END IF;
      ELSIF x_tipo_operacion IN ('VF')
      THEN
         -- Valor Inicial
         x_dias_efectivos := x_fecha_vencimiento - :new.fecha_operacion;

         IF x_BASE_TASFUT = 'C'
         THEN
            x_Valor_Inicial :=
               ROUND (:new.MONTO_TRANSACCION * x_TIPO_CAMBIO_DOLAR, 0);
         ELSE
            ops$mesacam.t_leer_val_param ('UF',
                                          x_fecha_proceso_base,
                                          x_valor_uf,
                                          'BC',
                                          2);
            x_Valor_Inicial :=
               ROUND ( (x_Dias_Efectivos * x_TIPO_CAMBIO_DOLAR) / x_Valor_UF,
                      4);
         END IF;

         -- Valor Final
         IF x_BASE_TASFUT = 'C'
         THEN
            x_valor_final := ROUND (:new.MONTO_TRANSACCION * x_TASA_FUTURO, 0);
         ELSE
            x_valor_final :=
               ROUND (
                  ( (x_Dias_Efectivos * x_TASA_FUTURO / 36000) + 1)
                  * x_VALOR_INICIAL,
                  4
               );
         END IF;

         IF :new.desc_area_contable IN
                  (x_fwd_usd_uf_comp,
                   x_fwd_usd_uf_comp_1,
                   x_fwd_usd_uf_comp_2,
                   x_fwd_usd_uf_efis,
                   x_fwd_usd_uf_efis_1)
         THEN
            x_Valor_Inicial := ROUND (:new.valor_inicial, 4);
            x_valor_final := ROUND (:new.utilidad_devengo, 4); -- compra sale amount vienen las UF.
         END IF;

         x_riesgo_equivalente := 0;

         IF x_id_producto IN ('MNEFIS', 'MNCOMP')
         THEN
            x_paridad_cliente := 0;
         END IF;
      ELSE
         --
         msj := 'Error de operacion en limpieza de cambios';
         ops$mesacam.m_d (msj, idtrigger, smj);
      --
      END IF;

      -- Borro utilidad devengo
      x_utilidad_devengo := NULL;
      x_utilidad_resultado := NULL;
      x_valuta_mop := :new.valuta_mop;
      x_valuta_mtx := :new.valuta_mtx;
      x_c_mt300 := NULL;

      -- Limpio desc area contable para TCD, TVD, TAC, TAV
      IF x_tipo_operacion IN ('TCD', 'TVD', 'TAC', 'TAV')
      THEN
         x_desc_area_contable := 'MECO';
      ELSE
         x_desc_area_contable := NULL;
      END IF;

      --  Valida Fecha de vencimiento no en nulo.
      IF :new.fecha_vencimiento IS NULL
      THEN
         msj := 'Fecha de vencimiento en nulo. No se graba operacion';
         ops$mesacam.m_d (msj, idtrigger, smj);
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      END IF;

      --
      -- Valido Precio Spot <> 0 para CF y VF
      IF NVL (x_precio_spot, 0) = 0
         AND NVL (x_tipo_operacion, 'X') IN ('CF', 'VF')
      THEN
         msj := 'Precio Spot en cero. No se graba operacion';
         ops$mesacam.m_d (msj, idtrigger, smj);
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      END IF;

      -- Validacion TCD TVD TAC TAV
      --      if nvl(x_tipo_operacion,'X') in ('AC','AV','CD','VD') then
      IF NVL (x_tipo_operacion, 'X') IN ('AC', 'AV', 'CD', 'VD')
         AND x_empresa <> '96623460-1'
      THEN
         msj :=
            '*** Operacion Spot no es TAC, TAV, TCD, TVD. No se graba ****';
         ops$mesacam.m_d (msj, idtrigger, smj);
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      END IF;

      --
      -- Validacion Fwd Banco.
      IF NVL (x_empresa, 'X') IN ('97036000-K')
         AND NVL (x_tipo_operacion, 'X') IN ('CF', 'VF', 'AFC', 'AFV')
      THEN
         msj := '*** Operacion Fwd Banco cambia estado a M';
         ops$mesacam.m_d (msj, idtrigger, smj);
         x_estado_operacion := 'M';
      END IF;

      --
      --
      msj := 'Antes del insert ';
      ops$mesacam.m_d (msj, idtrigger, smj);

      --
      INSERT INTO ops$mesacam.OPERACIONES (id_operacion,
                                           tipo_operacion,
                                           fecha_operacion,
                                           fecha_inicio,
                                           fecha_compensacion,
                                           fecha_vencimiento,
                                           monto_operacion,
                                           codigo_area,
                                           id_cliente,
                                           paridad_transaccion,
                                           monto_dolar,
                                           monto_pesos,
                                           paridad_operacion,
                                           id_oper_origen,
                                           moneda_operacion,
                                           id_trader,
                                           ind_contrato,
                                           tipo_cambio_dolar,
                                           estado_operacion,
                                           id_producto,
                                           precio_mx_tasa_esp,
                                           cod_destino,
                                           cod_sucursal,
                                           observacion,
                                           documento_pago_mop,
                                           documento_pago_mtx,
                                           fecha_crea,
                                           id_usuario_crea,
                                           empresa,
                                           sub_empresa,
                                           crea_tibco,
                                           -- 2
                                           fecha_act,
                                           id_usuario_act,
                                           act_tibco,
                                           ind_excede_entrega,
                                           ind_exede_margen,
                                           moneda_transaccion,
                                           monto_transaccion,
                                           precio_financiero,
                                           precio_mn_tasa_esp,
                                           valuta_mop,
                                           valuta_mtx,
                                           vamos_vienen_pago,
                                           banco_acreditar,
                                           banco_debitar,
                                           codigo_area_ing,
                                           DELTA_COMP,
                                           MONTO_AJUSTE,
                                           MONTO_CAPITAL_PAG,
                                           MONTO_CAPITAL_REC,
                                           MONTO_DEVENGO,
                                           MONTO_FINAL_PESOS,
                                           PARIDAD_CLIENTE,
                                           PARIDAD_NEGOCIO,
                                           PARIDAD_COMPENSACION,
                                           PARIDAD_REMANENTE,
                                           PERDIDA_DEVENGO,
                                           PERDIDA_RESULTADO,
                                           PERIODO_COMP,
                                           PRECIO_CONTABLE,
                                           PRECIO_MN_TASA,
                                           -- 3
                                           PRECIO_MX_TASA,
                                           PRECIO_OPERACION,
                                           PRECIO_SPOT,
                                           PRECIO_TRAN,
                                           REAJUSTE_ACTIVO,
                                           PRECIO_VENCIMIENTO,
                                           REAJUSTE_PASIVO,
                                           RESULTADO_BASILEA,
                                           RIESGO_EQUIVALENTE,
                                           TASA_COMPENSACION,
                                           TASA_FUTURO,
                                           TASA_TRANF_FUTURO,
                                           TIPO_CAMBIO_COMP,
                                           UTILIDAD_DEVENGO,
                                           VALOR_FINAL,
                                           UTILIDAD_RESULTADO,
                                           VALOR_INICIAL,
                                           BASE_CALCULO,
                                           BASE_CALCULO_VARIABLE,
                                           ID_CRITERIO,
                                           ID_GLOSA,
                                           N_OPER_COMEX,
                                           F_ABONO,
                                           F_DEBITO,
                                           F_MT300,
                                           FECHA_DEVENGO,
                                           FECHA_ING_CUSTODIA,
                                           -- 4
                                           FECHA_SAL_CUSTODIA,
                                           VALUTA_COMP,
                                           BASE_TASFUT,
                                           C_ABONO,
                                           C_DEBITO,
                                           C_MT300,
                                           COMISION,
                                           CONFIRMA,
                                           IND_ABNOPB,
                                           IND_ACT_LIN_FUT,
                                           IND_CURSE,
                                           IND_DOCVAL,
                                           IND_EXCEDE_CLIENTE,
                                           IND_EXCEDE_GRUPO,
                                           IND_EXCEDE_ACTUALIZA,
                                           IND_EXCEDE_PRODUC,
                                           IND_EXEDE_LINEA,
                                           IND_EXCEDE_SUBPROD,
                                           IND_FAX,
                                           IND_FICHA,
                                           IND_IMPRESION_VVISTA,
                                           IND_LIQUIDACION,
                                           IND_PAPELETA,
                                           IND_PLANILLA,
                                           IND_SWIFT,
                                           IND_VCTO,
                                           TIPO_CALCULO,
                                           -- 5
                                           VAMOS_VIENEN_REEM,
                                           CODIGO_CHEQUE,
                                           ID_CLIENTE_SUC,
                                           NUM_SUCURSAL,
                                           AREA_CONTABLE,
                                           FPAGO_COMPENSACION,
                                           TIPO_CARTERA,
                                           DESCRIBE_ATRIBUTO,
                                           TIPO_DOLAR_CALCULO,
                                           CONCEPTO_COMERCIO,
                                           PAGADOR_ACREDITAR,
                                           PAGADOR_DEBITAR,
                                           DESC_AREA_CONTABLE,
                                           INSTRUMENTO,
                                           FORMA_TASA_PAG,
                                           FORMA_TASA_REC,
                                           MONEDA_PAGO_PAG,
                                           MONEDA_PAGO_REC,
                                           TIPO_DOLAR,
                                           TIPO_INTERES_PAG,
                                           TIPO_INTERES_REC,
                                           TIPO_TASA_PAG,
                                           TIPO_TASA_REC,
                                           COD_COMERCIO,
                                           TIPO_ESTRATEGIA,
                                           IND_OFFSHORE,
                                           UNIDAD_ORGANIZACIONAL,
                                           TASA_MON_NAC,
                                           TASA_MON_EXT,
                                           FX_SWAP,
                                           --6
                                           PRECIO_DOLAR_TASA,
                                           BCO_PAGADOR,
                                           CONCEPTO_TRAN,
                                           NUMERO_PROPUESTA,
                                           ORIGIN,
                                           GESTOR)
        VALUES   (folio,
                  x_tipo_operacion,
                  :NEW.fecha_operacion,
                  x_fecha_inicio,
                  x_fecha_compensacion,
                  x_fecha_vencimiento,
                  x_monto_operacion,
                  x_codigo_area,
                  x_id_cliente,
                  x_paridad_transaccion,
                  :NEW.monto_dolar,
                  x_monto_pesos,
                  x_paridad_operacion,
                  :NEW.id_oper_origen,
                  x_moneda_operacion,
                  :NEW.id_trader,
                  :NEW.ind_contrato,
                  x_tipo_cambio_dolar,
                  x_estado_operacion,
                  x_ID_PRODUCTO,
                  x_precio_mx_tasa_esp,
                  :NEW.cod_destino,
                  :NEW.cod_sucursal,
                  :NEW.observacion,
                  x_documento_pago_mop,
                  x_documento_pago_mtx,
                  SYSDATE,
                  x_id_trader,
                  x_empresa,
                  x_empresa,
                  'S',
                  -- 2
                  SYSDATE,
                  x_id_trader,
                  NULL,
                  :NEW.ind_excede_entrega,
                  x_ind_exede_margen,
                  x_moneda_transaccion,
                  :NEW.monto_transaccion,
                  x_precio_financiero,
                  x_precio_mn_tasa_esp,
                  x_valuta_mop,
                  x_valuta_mtx,
                  x_vamos_vienen_pago,
                  x_banco_acreditar,
                  x_banco_debitar,
                  x_codigo_area_ing,
                  :NEW.DELTA_COMP,
                  :NEW.MONTO_AJUSTE,
                  :NEW.MONTO_CAPITAL_PAG,
                  :NEW.MONTO_CAPITAL_REC,
                  :NEW.MONTO_DEVENGO,
                  :NEW.MONTO_FINAL_PESOS,
                  x_PARIDAD_CLIENTE,
                  :NEW.PARIDAD_NEGOCIO,
                  :NEW.PARIDAD_COMPENSACION,
                  x_PARIDAD_REMANENTE,
                  :NEW.PERDIDA_DEVENGO,
                  :NEW.PERDIDA_RESULTADO,
                  :NEW.PERIODO_COMP,
                  x_PRECIO_CONTABLE,
                  x_precio_mn_tasa,
                  -- 3
                  x_PRECIO_MX_TASA,
                  :NEW.PRECIO_OPERACION,
                  x_PRECIO_SPOT,
                  :NEW.PRECIO_TRAN,
                  :NEW.REAJUSTE_ACTIVO,
                  :NEW.PRECIO_VENCIMIENTO,
                  :NEW.REAJUSTE_PASIVO,
                  :NEW.RESULTADO_BASILEA,
                  x_RIESGO_EQUIVALENTE,
                  :NEW.TASA_COMPENSACION,
                  x_TASA_FUTURO,
                  x_TASA_TRANF_FUTURO,
                  :NEW.TIPO_CAMBIO_COMP,
                  x_UTILIDAD_DEVENGO,
                  x_VALOR_FINAL,
                  x_UTILIDAD_RESULTADO,
                  x_VALOR_INICIAL,
                  :NEW.BASE_CALCULO,
                  :NEW.BASE_CALCULO_VARIABLE,
                  :NEW.ID_CRITERIO,
                  :NEW.ID_GLOSA,
                  :NEW.N_OPER_COMEX,
                  :NEW.F_ABONO,
                  :NEW.F_DEBITO,
                  :NEW.F_MT300,
                  :NEW.FECHA_DEVENGO,
                  :NEW.FECHA_ING_CUSTODIA,
                  -- 4
                  :NEW.FECHA_SAL_CUSTODIA,
                  :NEW.VALUTA_COMP,
                  x_BASE_TASFUT,
                  :NEW.C_ABONO,
                  :NEW.C_DEBITO,
                  x_C_MT300,
                  :NEW.COMISION,
                  :NEW.CONFIRMA,
                  :NEW.IND_ABNOPB,
                  x_IND_ACT_LIN_FUT,
                  x_IND_CURSE,
                  :NEW.IND_DOCVAL,
                  :NEW.IND_EXCEDE_CLIENTE,
                  :NEW.IND_EXCEDE_GRUPO,
                  :NEW.IND_EXCEDE_ACTUALIZA,
                  :NEW.IND_EXCEDE_PRODUC,
                  :NEW.IND_EXEDE_LINEA,
                  :NEW.IND_EXCEDE_SUBPROD,
                  :NEW.IND_FAX,
                  :NEW.IND_FICHA,
                  :NEW.IND_IMPRESION_VVISTA,
                  :NEW.IND_LIQUIDACION,
                  :NEW.IND_PAPELETA,
                  :NEW.IND_PLANILLA,
                  :NEW.IND_SWIFT,
                  :NEW.IND_VCTO,
                  :NEW.TIPO_CALCULO,
                  -- 5
                  x_VAMOS_VIENEN_REEM,
                  :NEW.CODIGO_CHEQUE,
                  :NEW.ID_CLIENTE_SUC,
                  :NEW.NUM_SUCURSAL,
                  x_AREA_CONTABLE,
                  x_FPAGO_COMPENSACION,
                  :NEW.TIPO_CARTERA,
                  :NEW.DESCRIBE_ATRIBUTO,
                  :NEW.TIPO_DOLAR_CALCULO,
                  :NEW.CONCEPTO_COMERCIO,
                  :NEW.PAGADOR_ACREDITAR,
                  :NEW.PAGADOR_DEBITAR,
                  x_DESC_AREA_CONTABLE,
                  :NEW.INSTRUMENTO,
                  x_FORMA_TASA_PAG,
                  :NEW.FORMA_TASA_REC,
                  x_MONEDA_PAGO_PAG,
                  x_MONEDA_PAGO_REC,
                  x_TIPO_DOLAR,
                  :NEW.TIPO_INTERES_PAG,
                  :NEW.TIPO_INTERES_REC,
                  :NEW.TIPO_TASA_PAG,
                  :NEW.TIPO_TASA_REC,
                  :NEW.COD_COMERCIO,
                  x_TIPO_ESTRATEGIA,
                  :NEW.IND_OFFSHORE,
                  x_UNIDAD_ORGANIZACIONAL,
                  x_tasa_mon_nac,
                  x_tasa_mon_ext,
                  :NEW.FX_SWAP,
                  --6
                  x_PRECIO_DOLAR_TASA,
                  x_bco_pagador,
                  x_concepto_tran,
                  :new.pagador_acreditar,
                  x_origin,
                  x_gestor);

      -- graba el numero de operacion del sistema de la mesa
      msj :=
            'Actualizado el id_operacion ==>'
         || folio
         || ' para folio tibco ==>'
         || :NEW.folio_tibco
         || ';  empresa==>'
         || x_empresa
         || '; id_oper_orig new ==>'
         || :NEW.id_oper_origen
         || '  old ==>'
         || :OLD.id_oper_origen;
      ops$mesacam.m_d (msj, idtrigger, smj);

      --
      -- Motor de Pagos Insert
      BEGIN
         msj :=
               'Actualiza motor de pagos ingreso ('
            || :NEW.id_operacion
            || ')  empresa ('
            || x_empresa
            || ')';
         ops$mesacam.m_d (msj, idtrigger, smj);
         ops$mesacam.Pr_motor_pagos_mx (:NEW.id_operacion,
                                        x_empresa,
                                        'I',
                                        'M',
                                        retorno_rutinas);

         IF (retorno_rutinas <> 0)
         THEN
            msj :=
                  'Error actualiza motor de pagos ingreso. Error ('
               || retorno_rutinas
               || ')  op. ('
               || :NEW.id_operacion
               || ')  empresa ('
               || x_empresa
               || ')';
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            msj := 'Error al actualizar motor de pagos. Ingreso';
            ops$mesacam.m_d (msj, idtrigger, smj);
            msj := SQLERRM;
            ops$mesacam.m_d (msj, idtrigger, smj);
      END;
   --
   END IF;

   --
   msj := 'fin del trigger';
   ops$mesacam.m_d (msj, idtrigger, smj);
--
-- el deleting y el updating son siempre nulos, en caso contrario,
-- se pueden crear condiciones de loop con modificaciones manuales
-- o de mantenimiento.
--
EXCEPTION
   WHEN salida
   THEN
      --
      msj :=
            'salida en exception final folio_tibco = '
         || :NEW.folio_tibco
         || ' folio Op ('
         || :NEW.id_operacion
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);
   --
   WHEN OTHERS
   THEN
      --
      msj := :NEW.id_operacion || ' ' || SQLERRM;
      ops$mesacam.m_d (msj, idtrigger, smj);
      msj := '**** Error de sistema. Falla del Trigger. ****';
      ops$mesacam.m_p (:NEW.empresa,
                       :NEW.id_operacion,
                       msj,
                       idtrigger,
                       smj);
--
END;
/


