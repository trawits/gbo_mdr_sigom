DROP TRIGGER OPS$MESACAM.T_OPERACIONES_RXC;

CREATE OR REPLACE TRIGGER OPS$MESACAM.T_OPERACIONES_RXC
BEFORE INSERT
ON OPS$MESACAM.OPERACIONES REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
NEW.tipo_operacion IN  ('CD','VD','TCD','TVD','AC','AV','TAC','TAV','AFC','AFV','CF','VF')
      AND NEW.id_operacion = NEW.id_oper_origen
      AND NVL(NEW.area_contable,'*') NOT IN ('B','D')
      )
DECLARE
   x_markup       NUMBER;
   x_ejec_esp     NUMBER:=0;
   x_ejec_esp_fwd NUMBER:=0;
BEGIN
   -- Asigna valor 9999 por defecto, en caso de venir en Nulo 
   IF :NEW.gestor IS NULL THEN 
      :NEW.gestor := 9999;
   END IF;
   -- Busca Codigo Ejecutivo
   IF :NEW.area_contable IN ('S','W') THEN
      BEGIN
         IF NVL(:NEW.cod_captadora,0) = 0 THEN
            BEGIN
               SELECT NVL(cod_ejecutivo,9999)
                 INTO :NEW.gestor
                 FROM ops$siadfi.nivel_trader
                WHERE trader  = :NEW.id_trader
                  AND empresa = :NEW.empresa;
            EXCEPTION
               WHEN OTHERS THEN
                  NULL;
            END;
         ELSE
            :NEW.gestor := :NEW.cod_captadora;
         END IF;
      EXCEPTION
         WHEN OTHERS THEN
             NULL;
      END;
   ELSIF :NEW.ind_new_tf = 'S' AND
         :NEW.tipo_operacion IN ('CF','VF') THEN
      BEGIN
         SELECT NVL(cod_ejecutivo,9999)
           INTO :NEW.gestor
           FROM ops$mesacam.OPER_SUCURSAL_FWD
          WHERE numero_operacion = :NEW.id_operacion
            AND empresa          = :NEW.empresa;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
             :NEW.gestor := 9999;
         WHEN OTHERS THEN
             NULL;
      END;
   ELSE
      BEGIN
         SELECT NVL(cod_ejecutivo,9999)
           INTO :NEW.gestor
           FROM ops$siadfi.nivel_trader
          WHERE trader  = :NEW.id_trader
            AND empresa = :NEW.empresa;
      EXCEPTION
         WHEN OTHERS THEN
             NULL;
      END;
   END IF;
   
   -- Identifica si el ejecutivo es especial o normal.
   BEGIN
   SELECT COUNT(*)
    INTO
     x_ejec_esp
    FROM  OPER_SUCURSAL_EJEC_ESP
     WHERE numero_operacion = :NEW.ID_OPERACION;
   EXCEPTION WHEN OTHERS THEN
   x_ejec_esp:= 0;
   END;
  -- Fin identificacion.

  -- Identifica si el ejecutivo es especial o normal para fwd.
   BEGIN
   SELECT COUNT(*)
    INTO
     x_ejec_esp_fwd
    FROM  OPER_SUCURSAL_FWD_EJEC_ESP
     WHERE numero_operacion = :NEW.ID_OPERACION;
   EXCEPTION WHEN OTHERS THEN
   x_ejec_esp_fwd:= 0;
   END;
  -- Fin identificacion.
 
   
   -- Calcula Mark Up
   IF :NEW.tipo_operacion IN ('CD','TCD') THEN

          IF  x_ejec_esp = 0 THEN
           x_markup            := :NEW.monto_transaccion * (:NEW.precio_dolar_tasa - :NEW.paridad_transaccion);
          ELSE
           x_markup            := :NEW.monto_transaccion * (:NEW.precio_mx_tasa - :NEW.paridad_transaccion);
          END IF;
         :NEW.mark_up        := ROUND(NVL(x_markup,0));
         :NEW.divisa_mark_up := 'CLP';
   ELSIF :NEW.tipo_operacion IN ('AC','TAC','AFC') THEN
          IF  x_ejec_esp = 0 THEN
               x_markup            := ops$mesacam.F_Modalidad_Calculo(:NEW.monto_transaccion,:NEW.precio_mx_tasa,:NEW.moneda_transaccion)
                                    - ops$mesacam.F_Modalidad_Calculo(:NEW.monto_transaccion,:NEW.paridad_transaccion,:NEW.moneda_transaccion);
          ELSE
               x_markup            := ops$mesacam.F_Modalidad_Calculo(:NEW.monto_transaccion,:NEW.precio_dolar_tasa,:NEW.moneda_transaccion)
                                    - ops$mesacam.F_Modalidad_Calculo(:NEW.monto_transaccion,:NEW.paridad_transaccion,:NEW.moneda_transaccion);
           END IF;                        
          :NEW.mark_up        := ROUND(NVL(x_markup,0),2);
          :NEW.divisa_mark_up := 'USD';
   ELSIF :NEW.tipo_operacion IN ('CF') THEN
           ---x_markup            := :NEW.monto_transaccion * (:NEW.precio_transf_fwd - :NEW.tasa_futuro);  --- Cambio c�lculo Markup Forward LPL 10-02-2014
          IF x_ejec_esp_fwd = 0 THEN
          x_markup            := :NEW.monto_transaccion * ((:NEW.precio_spot + :NEW.puntos_fwd) - :NEW.tasa_futuro);  
          ELSE
          x_markup            := :NEW.monto_transaccion * (:NEW.precio_transf_fwd - :NEW.tasa_futuro);
          END IF;
          :NEW.mark_up        := ROUND(NVL(x_markup,0),0);
          :NEW.divisa_mark_up := 'CLP';
              
   ELSIF :NEW.tipo_operacion IN ('VD','TVD') THEN
       IF  x_ejec_esp = 0 THEN
           x_markup            := :NEW.monto_transaccion * (:NEW.paridad_transaccion - :NEW.precio_dolar_tasa);
       ELSE
           x_markup            := :NEW.monto_transaccion * (:NEW.paridad_transaccion - :NEW.precio_mx_tasa);
       END IF;   
          :NEW.mark_up        := ROUND(NVL(x_markup,0));
          :NEW.divisa_mark_up := 'CLP';
   ELSIF :NEW.tipo_operacion IN ('AV','TAV','AFV') THEN
        IF  x_ejec_esp = 0 THEN
            x_markup            := ops$mesacam.F_Modalidad_Calculo(:NEW.monto_transaccion,:NEW.paridad_transaccion,:NEW.moneda_transaccion)
                                - ops$mesacam.F_Modalidad_Calculo(:NEW.monto_transaccion,:NEW.precio_mx_tasa,:NEW.moneda_transaccion);
        ELSE
            x_markup            := ops$mesacam.F_Modalidad_Calculo(:NEW.monto_transaccion,:NEW.paridad_transaccion,:NEW.moneda_transaccion)
                                - ops$mesacam.F_Modalidad_Calculo(:NEW.monto_transaccion,:NEW.precio_dolar_tasa,:NEW.moneda_transaccion);
           
        END IF;                                            
          :NEW.mark_up        := ROUND(NVL(x_markup,0),2);
          :NEW.divisa_mark_up := 'USD';
          
   ELSIF :NEW.tipo_operacion IN ('VF') THEN
          ---x_markup            :=  (:NEW.tasa_futuro - :NEW.precio_transf_fwd) * :NEW.monto_transaccion;    ---Cambio c�lculo Markup Forward LPL 10-02-2014
        IF x_ejec_esp_fwd = 0 THEN
          x_markup            :=  (:NEW.tasa_futuro - (:NEW.precio_spot +:NEW.puntos_fwd)) * :NEW.monto_transaccion; 
        ELSE
          x_markup            :=   :NEW.monto_transaccion * (:NEW.tasa_futuro - :NEW.precio_transf_fwd);
        END IF;
          :NEW.mark_up        := ROUND(NVL(x_markup,0),0);
          :NEW.divisa_mark_up := 'CLP';
          
   END IF;
EXCEPTION
    WHEN OTHERS THEN
         NULL;
END;
/


