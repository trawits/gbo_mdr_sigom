DROP TRIGGER OPS$MESACAM.T_OPERACIONES;

CREATE OR REPLACE TRIGGER OPS$MESACAM.T_OPERACIONES
AFTER DELETE OR INSERT OR UPDATE
ON OPS$MESACAM.OPERACIONES 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
new.tipo_operacion in  ('CD','VD','TCD','TVD','AC','AV','TAC','TAV','AFC','AFV','CF','VF')
  and new.id_producto not in ('UFPARC','AEFIS','ACOMP')
  and new.id_operacion = new.id_oper_origen
  and NEW.estado_operacion in ('N','E','C','F','M','X')
  and NEW.empresa <> '96643070-2'
      )
DECLARE
   -- variables para el debug
   idtrigger                 VARCHAR2 (40) := 'T_OPERACIONES';
   msj                       VARCHAR2 (255);
   smj                       NUMBER := 0;
   --
   vID_OPERACION             NUMBER;
   vEMPRESA                  VARCHAR2 (15);
   vSUB_EMPRESA              VARCHAR2 (15);
   x_cont_lines              NUMBER;
   salida exception;
   foliotb                   NUMBER;
   operacion_nueva           CHAR;                      
   eliminada_de_tibco        VARCHAR2 (1);
   x_id_operacion            NUMBER;
   x_id_operacion_tibco      VARCHAR2 (255);
   x_estado_respo            VARCHAR2 (5);
   x_estado_tibco            VARCHAR2 (5);
   x_razon_social            VARCHAR2 (40);
   x_id_cliente_rut          VARCHAR2 (15);
   x_FIELD21                 VARCHAR2 (15);
   x_fk_parent               NUMBER;
   x_aliascode               VARCHAR2 (80);
   x_id_cliente_paso         VARCHAR2 (15);
   x_precio_dolar_tasa       NUMBER;
   x_ciudad                  VARCHAR2 (15);
   x_comuna                  VARCHAR2 (15);
   x_direccion               VARCHAR2 (40);           -- Usada para portafolio.
   x_telefono                VARCHAR2 (20);
   x_empresa                 VARCHAR2 (15);
   x_empresa_origen          VARCHAR2 (15);
   x_ind_mult_divi           VARCHAR2 (1);
   x_bloqueo_oper            VARCHAR2 (1);
   x_dummy                   VARCHAR2 (80);
   x_codigo_area             NUMBER;
   x_paridad_transaccion     NUMBER;
   x_paridad_operacion       NUMBER;
   x_PORTAFOLIO              VARCHAR2 (40);
   x_ID_TRADER               VARCHAR2 (15);
   x_MESA                    VARCHAR2 (3);
   x_TIPO_ESTRATEGIA         VARCHAR2 (4);
   x_UNIDAD_ORGANIZACIONAL   VARCHAR2 (2);
   x_MARCA_CONTABLE          VARCHAR2 (1);
   x_DOCUMENTO_PAGO_MOP      VARCHAR2 (2);
   x_DOCUMENTO_PAGO_MTX      VARCHAR2 (2);
   x_fecha_proceso_base      DATE;
   x_valuta_mop              DATE;
   x_valuta_mtx              DATE;
   x_fecha_aux               DATE;
   x_number_aux              NUMBER;
   x_precio_spot             NUMBER;
   x_monto_OPERACION         NUMBER;
   x_monto_pesos             NUMBER;
   x_utilidad_resultado      NUMBER;
   x_precio_mx_tasa          NUMBER;
   x_emp_murex               VARCHAR2 (3);
   x_ORIGIN                  VARCHAR2 (12);
   banco                     VARCHAR2 (10) := '97036000-K';
   corredora                 VARCHAR2 (10) := '96683200-2';                
   agencia                   VARCHAR2 (10) := '96623460-1';            -- obsoleta
   investment                VARCHAR2 (10) := '96556210-9';
   new_investment            VARCHAR2 (10) := '96643070-2';
   x_fecha_fixing            DATE;
   x_banco_debitar           VARCHAR2 (10);                       --adomingues
   x_banco_acreditar         VARCHAR2 (10);                       --adomingues
   x_tipo_operacion          VARCHAR2 (5);                         --adomingues
   x_area_contable           VARCHAR2 (2);                        --adomimgues
   x_tipo_cambio_dolar       NUMBER;                              --adomingues
   x_valor_inicial           NUMBER;                              --adomingues

   v_paridad_transaccion     NUMBER;
   v_precio_dolar_tasa       NUMBER;
   v_paridad_operacion       NUMBER;
   v_monto_pesos             NUMBER;
   v_monto_operacion         NUMBER;
   v_puntos_fwd_limpio       NUMBER; 
   
   v_origen                  NUMBER;                                --mmoreno
BEGIN
   -- controla bloqueo de operaciones para no publicar por efectos del devengo
   --    msj := 'inicio. '||:new.id_operacion||' ; empresa '||:NEW.empresa;
   --    ops$mesacam.m_d(msj,idtrigger,smj);

   -- No manda mensajes.
   BEGIN
      SELECT   bloqueo_operaciones, fecha
        INTO   x_bloqueo_oper, x_fecha_proceso_base
        FROM   ops$mesacam.control_procesos
       WHERE   empresa = :NEW.empresa AND mesa = 'MDX';

      IF x_bloqueo_oper = 'B'
      THEN
         msj :=
               'Sistema sun bloqueado. '
            || :new.id_operacion
            || ' ; empresa '
            || :NEW.empresa;
         ops$mesacam.m_d (msj, idtrigger, smj);
         RAISE salida;
      END IF;



      IF :NEW.fecha_operacion <> x_fecha_proceso_base
      THEN
         msj :=
               'Fecha operacion distinta a fecha de proceso ('
            || x_fecha_proceso_base
            || ')';
         ops$mesacam.m_d (msj, idtrigger, smj);
         RAISE salida;
      END IF;


      IF TRUNC (SYSDATE) <> x_fecha_proceso_base
      THEN
         msj :=
               'Fecha de proceso distinta a fecha calendario  ('
            || x_fecha_proceso_base
            || ')';
      --************************************************
      --********  Sacar al llevar a produccion
      --************************************************
              ops$mesacam.m_d(msj,idtrigger,smj);
              raise salida;
      --************************************************
      --************************************************
      --************************************************
      END IF;

      -- filtro estructuras.
      IF :new.tipo_operacion IN ('CF', 'VF')
      THEN
         IF :new.id_producto IN ('UFCOMP', 'UFEFIS')
         THEN
            msj :=
                  'Filtro estructuras = ('
               || :new.tipo_operacion
               || ') ('
               || :new.id_producto
               || ')';
            ops$mesacam.m_d (msj, idtrigger, smj);
            RAISE salida;
         END IF;
      END IF;

      IF :new.tipo_operacion IN ('AFC', 'AFV')
      THEN
         IF :new.id_producto IN
                  ('MLCOMPCC', 'MLCOMPCP', 'MECOMPCC', 'MECOMPCP')
         THEN
            msj :=
                  'Filtro estructuras = ('
               || :new.tipo_operacion
               || ') ('
               || :new.id_producto
               || ')';
            ops$mesacam.m_d (msj, idtrigger, smj);
            RAISE salida;
         END IF;
      END IF;
   EXCEPTION
      WHEN salida
      THEN
         RAISE salida;
      WHEN OTHERS
      THEN
         --
         msj := 'others en Control bloqueo: ' || SQLERRM;
         ops$mesacam.m_d (msj, idtrigger, smj);
         msj :=
               'others en Control bloqueo: ('
            || x_bloqueo_oper
            || ') ('
            || x_fecha_proceso_base
            || ') '
            || :NEW.id_operacion
            || ' '
            || :new.empresa;
         ops$mesacam.m_d (msj, idtrigger, smj);
         msj := '**** Error de sistema en el Control de Bloqueo. ****';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
   --
   END;

   -- definimos la empresa (SUN ==> entrega el rut)
   x_empresa_origen := ops$mesacam.t_empresa (:NEW.empresa, 'MDR');
   -- Define portaforlio para MDR
   x_id_trader := :NEW.ID_TRADER;
   x_empresa := :NEW.empresa;

   IF :new.codigo_area_ing IS NULL
   THEN
      x_codigo_area := :new.codigo_area;
   ELSIF :new.codigo_area_ing <> :new.codigo_area
   THEN
      x_codigo_area := :new.codigo_area_ing;
   ELSE
      x_codigo_area := :new.codigo_area;
   END IF;
   
   -- Cambia portafolio para operaciones de terminal financiero.
   --if nvl(:NEW.AREA_CONTABLE,'X') in ('S','W') or :new.ind_new_tf = 'S' then    --CARMEN
   IF NVL (:NEW.AREA_CONTABLE, 'X') IN ('S', 'W', 'C')
      OR:new.ind_new_tf = 'S'
   THEN
      IF :new.tipo_operacion IN ('CD', 'VD', 'TCD', 'TVD')
      THEN
         IF x_empresa = banco
         THEN
            IF :NEW.moneda_transaccion = 'USD'
            THEN
               x_direccion := 'V_MM_SPOT_STD';
            ELSE
               x_direccion := 'T_ME_FX_STD';
            END IF;
         ELSIF x_empresa = agencia
         THEN
            x_direccion := 'V_MM_SPOT_AGV';
         ELSE
            msj := '1.- Portfolio de TF DESCONOCIDO.';
            ops$mesacam.m_d (msj, idtrigger, smj);
            x_direccion := 'DESCONOCIDO';
         END IF;
      ELSIF :new.tipo_operacion IN ('AC', 'AV', 'TAC', 'TAV')
      THEN
         IF x_empresa = banco
         THEN
            x_direccion := 'T_ME_FX_STD';
         ELSIF x_empresa = agencia
         THEN
            x_direccion := 'T_MM_FWD_EF_AGV';
         ELSE
            msj := '2.- Portfolio de TF DESCONOCIDO.';
            ops$mesacam.m_d (msj, idtrigger, smj);
            x_direccion := 'DESCONOCIDO';
         END IF;
      ELSIF :new.tipo_operacion IN ('CF', 'VF')
      THEN
         IF x_empresa = banco
         THEN
            IF :new.id_producto IN
                     ('MLCOMPCC',
                      'MLCOMPCP',
                      'MECOMPCC',
                      'MECOMPCP',
                      'UFCOMP',
                      'MNCOMP')
            THEN
               x_direccion := 'T_LD_FWD_ND_STD';
            ELSE
               x_direccion := 'T_LD_FWD_EF_STD';
            END IF;
         ELSIF x_empresa = new_investment
         THEN                                                 -- Nueva empresa
            x_direccion := 'T_FWD_SANIL';                     -- Nueva empresa
         ELSE
            msj := '3.- Portfolio de TF DESCONOCIDO.';
            ops$mesacam.m_d (msj, idtrigger, smj);
            x_direccion := 'DESCONOCIDO';
         END IF;
      ELSIF :new.tipo_operacion IN ('AFC', 'AFV') AND x_empresa = banco AND :new.id_producto IN ('MLEFISCC', 'MLEFISCP', 'MEEFISCC', 'MEEFISCP')
      THEN
         x_direccion := 'T_ME_FWD_EF_STD';
      ELSE
         msj := '4.- Portfolio de TF DESCONOCIDO.';
         ops$mesacam.m_d (msj, idtrigger, smj);
         x_direccion := 'DESCONOCIDO';
      END IF;
   ELSIF x_codigo_area IN (8, 9, 10, 11, 12, 13) AND NVL (:NEW.AREA_CONTABLE, 'X') NOT IN ('B', 'D')
   THEN
      IF :new.tipo_operacion IN ('CD', 'VD', 'TCD', 'TVD')
      THEN
         IF x_empresa = banco
         THEN
            IF :NEW.moneda_transaccion = 'USD'
            THEN
               x_direccion := 'V_MM_SPOT_STD';
            ELSE
               x_direccion := 'T_ME_FX_STD';
            END IF;
         ELSIF x_empresa = agencia
         THEN
            x_direccion := 'V_MM_SPOT_AGV';
         ELSE
            msj := '5.- Portfolio de Mesa Distribucion DESCONOCIDO.';
            ops$mesacam.m_d (msj, idtrigger, smj);
            x_direccion := 'DESCONOCIDO';
         END IF;
      ELSIF :new.tipo_operacion IN ('AC', 'AV', 'TAC', 'TAV')
      THEN
         IF x_empresa = banco
         THEN
            x_direccion := 'T_ME_FX_STD';
         ELSE
            msj := '6.- Portfolio de Mesa Distribucion DESCONOCIDO.';
            ops$mesacam.m_d (msj, idtrigger, smj);
            x_direccion := 'DESCONOCIDO';
         END IF;
      ELSIF :new.tipo_operacion IN ('AFC', 'AFV') AND x_empresa = banco AND :new.id_producto IN ('MLEFISCC', 'MLEFISCP', 'MEEFISCC', 'MEEFISCP')
      THEN
         x_direccion := 'T_MM_FWD_EF_STD';
      ELSE
         msj := '7.- Portfolio de TF DESCONOCIDO.';
         ops$mesacam.m_d (msj, idtrigger, smj);
         x_direccion := 'DESCONOCIDO';
      END IF;
   ELSIF NVL (:NEW.AREA_CONTABLE, 'X') IN ('B', 'D')
   THEN
      x_TIPO_ESTRATEGIA := 'X';                          -- Para igualar tabla
      x_UNIDAD_ORGANIZACIONAL := 'X';
      x_MARCA_CONTABLE := 'X';
      x_number_aux := 0;

      IF x_ID_TRADER = 'OPS$FVERGARAMX'
      THEN
         x_id_trader := 'OPS$LAPEREZMX';
      END IF;

      ops$mesacam.t_codigo_area (x_empresa,
                                 'MDR',
                                 x_number_aux,
                                 x_portafolio,
                                 x_ID_TRADER,
                                 'MDX',
                                 x_TIPO_ESTRATEGIA,
                                 x_UNIDAD_ORGANIZACIONAL,
                                 x_MARCA_CONTABLE);
      x_direccion := x_portafolio;

      IF x_direccion = 'DESCONOCIDO'
      THEN
         msj := '8.- Portfolio Bolsa Datatec indefinido. DESCONOCIDO.';
         ops$mesacam.m_d (msj, idtrigger, smj);
      END IF;
   ELSE
      msj := '9.- Portfolio indefinido. DESCONOCIDO.';
      ops$mesacam.m_d (msj, idtrigger, smj);
      x_direccion := 'DESCONOCIDO';
   END IF;

   -- averigua si es una operacion que existe en la tabla tibco
   -- Parche Fwd Americano
   IF     :new.tipo_operacion IN ('CF', 'VF')
      AND x_empresa = banco
      AND:new.id_producto IN ('AEFIS', 'ACOMP')
   THEN
      msj := 'Portfolio Actualizado para Fwd Americano.';
      ops$mesacam.m_d (msj, idtrigger, smj);
      x_direccion := 'T_LD_FWD_AM_STD';
   END IF;

   BEGIN
      operacion_nueva := 'N';
      x_id_operacion := NULL;

      SELECT   id_operacion
        INTO   x_id_operacion
        FROM   ops$mesacam.operaciones_tibco
       WHERE   id_operacion = :NEW.id_operacion AND empresa = :NEW.empresa;

      --
      msj := 'Op. ' || x_id_operacion || ' existe en tibco';
      ops$mesacam.m_d (msj, idtrigger, smj);
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         operacion_nueva := 'S';
         --
         msj := 'Op. ' || :NEW.id_operacion || ' es nueva';
         ops$mesacam.m_d (msj, idtrigger, smj);
      --
      WHEN TOO_MANY_ROWS
      THEN
         --
         msj :=
            'Op. ' || :NEW.id_operacion || ' existe en tibco (too_many_rows)';
         ops$mesacam.m_d (msj, idtrigger, smj);

         --
         -- voy a buscar el max si existen muchos id.
         BEGIN
            SELECT   MAX (id_operacion)
              INTO   x_id_operacion
              FROM   ops$mesacam.operaciones_tibco
             WHERE   id_operacion = :NEW.id_operacion
                     AND empresa = :NEW.empresa;
         EXCEPTION
            WHEN OTHERS
            THEN
               --
               msj := 'others en select del max() op.tibco ' || SQLERRM;
               ops$mesacam.m_d (msj, idtrigger, smj);
               msj :=
                  '**** Error de sistema al averiguar si la operacion ya ha sido publicada. ****';
               ops$mesacam.m_p (:NEW.empresa,
                                :NEW.id_operacion,
                                msj,
                                idtrigger,
                                smj);
               RAISE salida;
         --
         END;
      WHEN OTHERS
      THEN
         --
         msj := 'others en select op.tibco ' || SQLERRM;
         ops$mesacam.m_d (msj, idtrigger, smj);
         msj :=
            '**** Error de sistema al averiguar si la operacion ya ha sido publicada. ****';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
   --
   END;

   -- obtenemos los datos del cliente.
   BEGIN
      x_id_cliente_paso := :NEW.id_cliente;

      SELECT   id_cliente_rut,
               razon_social,
               ciudad,
               SUBSTR (comuna, 1, 15),
               SUBSTR (fono, 1, 15)
        INTO   x_id_cliente_rut,
               x_razon_social,
               x_ciudad,
               x_comuna,
               x_telefono
        FROM   clientes
       WHERE   id_cliente = x_id_cliente_paso;

      x_FIELD21 := x_id_cliente_rut;
   EXCEPTION
      WHEN OTHERS
      THEN
         --
         msj := 'others al obtener cliente ' || SQLERRM;
         ops$mesacam.m_d (msj, idtrigger, smj);
         msj := '**** Error al tratar de obtener datos de cliente ****';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
   --
   END;

   --
   -- Cambio el Cliente Generico.
   BEGIN
      IF :new.tipo_operacion NOT IN
               ('CD', 'VD', 'TCD', 'TVD', 'AC', 'AV', 'TAC', 'TAV')
      THEN
         IF x_id_cliente_rut = '90001-K'
         THEN
            msj := 'Cliente 90001-K no autorizado para operatoria No Spot';
            ops$mesacam.m_d (msj, idtrigger, smj);
            ops$mesacam.m_p (:NEW.empresa,
                             :NEW.id_operacion,
                             msj,
                             idtrigger,
                             smj);
            RAISE salida;
         END IF;
      ELSE
         IF x_id_cliente_rut = '90001-K'
         THEN
            msj :=
               'Cambio Cliente 90001-K a cliente generico. Requerimiento KGL';
            ops$mesacam.m_d (msj, idtrigger, smj);
            --
            x_id_cliente_paso := '9RCH';
            x_id_cliente_rut := '222222-1';
            x_FIELD21 := x_id_cliente_rut;
         --
         ELSE
            -- pregunto por aliascode Oracle
            SELECT   s.fk_parent
              INTO   x_fk_parent
              FROM   PGT_TRD.T_PGT_OBJ_SOURCE_S@base_mdr s
             WHERE       s.fk_source = 22.4
                     AND s.fk_owner_obj = 1448.4
                     AND s.aliascode = x_id_cliente_rut;

            -- pregunto por aliascode Murex
            SELECT   s.aliascode
              INTO   x_aliascode
              FROM   PGT_TRD.T_PGT_OBJ_SOURCE_S@base_mdr s
             WHERE       s.fk_source = 9.4
                     AND s.fk_owner_obj = 1448.4
                     AND s.fk_parent = x_fk_parent;
         END IF;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --
         msj := 'Cambio Cliente ' || x_id_cliente_rut || ' a cliente generico';
         ops$mesacam.m_d (msj, idtrigger, smj);
         --
         x_id_cliente_paso := '9RCH';
         x_id_cliente_rut := '222222-1';
      --
      WHEN TOO_MANY_ROWS
      THEN
         --
         msj := 'too_many_rows select aliascode cliente ' || SQLERRM;
         ops$mesacam.m_d (msj, idtrigger, smj);
         msj :=
            '**** Error al tratar de obtener AliasCode de cliente en MDR ****';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
      --
      WHEN salida
      THEN
         RAISE salida;
      WHEN OTHERS
      THEN
         --
         msj := 'others select aliascode cliente ' || SQLERRM;
         ops$mesacam.m_d (msj, idtrigger, smj);
         msj :=
            '**** Error al tratar de obtener AliasCode de cliente en MDR ****';
         ops$mesacam.m_p (:NEW.empresa,
                          :NEW.id_operacion,
                          msj,
                          idtrigger,
                          smj);
         RAISE salida;
   --
   END;

   -- Logica del ind_mult_divi.
   IF :new.tipo_operacion IN ('VD', 'TAC', 'T', 'AC', 'TVD')
   THEN
      IF (:NEW.moneda_transaccion IN ('EUR', 'GBP', 'AUD', 'DEG')
          OR:NEW.moneda_operacion IN ('EUR', 'GBP', 'AUD', 'DEG'))
         AND (:NEW.moneda_transaccion <> ('CLP')
              AND:NEW.moneda_operacion <> ('CLP'))
      THEN
         x_ind_mult_divi := 'M';
      ELSE
         x_ind_mult_divi := 'D';
      END IF;
   ELSIF :new.tipo_operacion IN ('CD', 'TAV', 'AV', 'TCD')
   THEN
      IF (:NEW.moneda_transaccion IN ('EUR', 'GBP', 'AUD', 'DEG')
          OR:NEW.moneda_operacion IN ('EUR', 'GBP', 'AUD', 'DEG'))
         AND (:NEW.moneda_transaccion <> ('CLP')
              AND:NEW.moneda_operacion <> ('CLP'))
      THEN
         x_ind_mult_divi := 'D';
      ELSE
         x_ind_mult_divi := 'M';
      END IF;
   ELSIF :new.tipo_operacion IN ('VF', 'AFC')
   THEN
      IF :NEW.moneda_transaccion IN ('EUR', 'GBP', 'AUD', 'DEG')
         OR:NEW.moneda_operacion IN ('EUR', 'GBP', 'AUD', 'DEG')
      THEN
         x_ind_mult_divi := 'M';
      ELSE
         x_ind_mult_divi := 'D';
      END IF;
   ELSIF :new.tipo_operacion IN ('CF', 'AFV')
   THEN
      IF :NEW.moneda_transaccion IN ('EUR', 'GBP', 'AUD', 'DEG')
         OR:NEW.moneda_operacion IN ('EUR', 'GBP', 'AUD', 'DEG')
      THEN
         x_ind_mult_divi := 'D';
      ELSE
         x_ind_mult_divi := 'M';
      END IF;
   ELSE
      x_ind_mult_divi := 'M';
   END IF;


   -- Correccion por viaje Precio Cierre a Murex
   v_paridad_transaccion    := :new.paridad_transaccion;
   v_precio_dolar_tasa      := :new.precio_dolar_tasa;
   v_paridad_operacion      := :new.paridad_operacion;
   v_monto_pesos            := :new.MONTO_PESOS;
   v_monto_operacion        := :new.monto_OPERACION;

   x_paridad_transaccion    := :new.paridad_transaccion;
   x_precio_dolar_tasa      := :new.precio_dolar_tasa;

   x_precio_spot            := :new.precio_spot;
   x_paridad_operacion      := :new.paridad_operacion;
   x_monto_OPERACION        := :new.monto_OPERACION;
   x_monto_pesos            := :new.MONTO_PESOS;

   IF :new.tipo_operacion IN
            ('AC', 'TAC', 'AV', 'TAV', 'CD', 'TCD', 'VD', 'TVD')
      AND:new.empresa IN ('97036000-K', '96623460-1')
      AND NVL (:NEW.AREA_CONTABLE, 'X') NOT IN ('B', 'D')
   THEN
      IF :new.tipo_operacion IN ('CD', 'TCD', 'VD', 'TVD')
      THEN
         IF :new.codigo_area_ing IS NULL
         THEN
            --             if nvl(:new.codigo_area,0) in (8,9,10,11,12,13) or (nvl(:NEW.AREA_CONTABLE,'X') in ('S','W') or :new.ind_new_tf = 'S') then --CARMEN
            IF NVL (:new.codigo_area, 0) IN (8, 9, 10, 11, 12, 13)
               OR (NVL (:NEW.AREA_CONTABLE, 'X') IN ('S', 'W', 'C')
                   OR:new.ind_new_tf = 'S')
            THEN
               x_precio_dolar_tasa :=
                  NVL (x_precio_dolar_tasa, x_paridad_transaccion);
               -- Recalcular Montos en Pesos.
               x_monto_OPERACION :=
                  ROUND (:new.monto_transaccion * x_precio_dolar_tasa, 0);
               x_monto_pesos := x_monto_OPERACION;
               --
               x_paridad_transaccion := x_precio_dolar_tasa;
               x_precio_spot := x_paridad_transaccion;
               x_paridad_operacion := x_paridad_transaccion;
            END IF;
         ELSE
            --             if :new.codigo_area_ing in (8,9,10,11,12,13) or (nvl(:NEW.AREA_CONTABLE,'X') in ('S','W') or :new.ind_new_tf = 'S') then  --CARMEN
            IF :new.codigo_area_ing IN (8, 9, 10, 11, 12, 13)
               OR (NVL (:NEW.AREA_CONTABLE, 'X') IN ('S', 'W', 'C')
                   OR:new.ind_new_tf = 'S')
            THEN
               x_precio_dolar_tasa :=
                  NVL (x_precio_dolar_tasa, x_paridad_transaccion);
               -- Recalcular Montos en Pesos.
               x_monto_OPERACION :=
                  ROUND (:new.monto_transaccion * x_precio_dolar_tasa, 0);
               x_monto_pesos := x_monto_OPERACION;
               --
               x_paridad_transaccion := x_precio_dolar_tasa;
               x_precio_spot := x_paridad_transaccion;
               x_paridad_operacion := x_paridad_transaccion;
            END IF;
         END IF;
      ELSE
         IF :new.codigo_area_ing IS NULL
         THEN
            --             if nvl(:new.codigo_area,0) in (8,9,10,11,12,13) or (nvl(:NEW.AREA_CONTABLE,'X') in ('S','W') or :new.ind_new_tf = 'S') then  --CARMEN
            IF NVL (:new.codigo_area, 0) IN (8, 9, 10, 11, 12, 13)
               OR (NVL (:NEW.AREA_CONTABLE, 'X') IN ('S', 'W', 'C')
                   OR:new.ind_new_tf = 'S')
            THEN
               x_precio_dolar_tasa :=
                  NVL (x_precio_dolar_tasa, x_paridad_transaccion);
               x_precio_spot := x_precio_dolar_tasa;
            END IF;
         ELSE
            --             if nvl(:new.codigo_area_ing,0) in (8,9,10,11,12,13) or (nvl(:NEW.AREA_CONTABLE,'X') in ('S','W') or :new.ind_new_tf = 'S') then  --CARMEN
            IF NVL (:new.codigo_area_ing, 0) IN (8, 9, 10, 11, 12, 13)
               OR (NVL (:NEW.AREA_CONTABLE, 'X') IN ('S', 'W', 'C')
                   OR:new.ind_new_tf = 'S')
            THEN
               x_precio_dolar_tasa :=
                  NVL (x_precio_dolar_tasa, x_paridad_transaccion);
               x_precio_spot := x_precio_dolar_tasa;
            END IF;
         END IF;
      END IF;
   ELSIF :new.tipo_operacion IN ('AC', 'TAC', 'AV', 'TAV', 'CD', 'TCD', 'VD', 'TVD') AND :new.empresa IN ('97036000-K', '96623460-1') AND NVL (:NEW.AREA_CONTABLE, 'X') IN ('B', 'D')
   THEN
      IF :new.tipo_operacion IN ('CD', 'TCD', 'VD', 'TVD')
      THEN
         x_precio_dolar_tasa := x_paridad_transaccion;
         x_precio_spot := x_paridad_transaccion;
         x_paridad_operacion := x_paridad_transaccion;
      ELSE
         x_precio_dolar_tasa :=
            NVL (x_precio_dolar_tasa, x_paridad_transaccion);
         x_precio_spot := x_precio_dolar_tasa;
      END IF;
   END IF;

   IF :new.tipo_operacion IN ('AFC', 'AFV')
      AND:new.empresa IN ('97036000-K', '96623460-1')
   THEN
      x_precio_spot := x_precio_dolar_tasa;
   END IF;

    /*** Agrega viaje a Murex Precio de Cierre 10-07-2013  ***/
    if :new.tipo_operacion in ('TAC','CD','TCD','TAV','VD','TVD') and :new.empresa IN ('97036000-K', '96623460-1') then
        begin
            x_paridad_transaccion  := v_paridad_transaccion;
            x_paridad_operacion    := v_paridad_transaccion;
            x_precio_spot          := v_precio_dolar_tasa;
            x_precio_dolar_tasa    := v_precio_dolar_tasa;

            x_monto_pesos          := v_monto_pesos;
            x_monto_operacion      := v_monto_operacion;
        end;
    end if;

   msj :=
         'Precio_dolar_tasa ('
      || x_precio_dolar_tasa
      || ') paridad_transaccion ('
      || x_paridad_transaccion
      || ') x_precio_mx_tasa ('
      || x_precio_mx_tasa
      || ') x_precio_spot ('
      || x_precio_spot
      || ') cod_area ('
      || :new.codigo_area
      || ') area_ing ('
      || :new.codigo_area_ing
      || ') Dcto pago, (mop)-(mxt) ('
      || :NEW.DOCUMENTO_PAGO_MOP
      || ')-('
      || :NEW.DOCUMENTO_PAGO_MTX
      || ')';
   ops$mesacam.m_d (msj, idtrigger, smj);

   --
   -- Formas de pago por defecto :
   IF NVL (:NEW.DOCUMENTO_PAGO_MOP, ' ') = ' '
      OR NVL (:NEW.DOCUMENTO_PAGO_MOP, '  ') = '  '
   THEN
      IF :NEW.moneda_operacion = 'CLP'
      THEN
         x_DOCUMENTO_PAGO_MOP := 'EF';                             -- efectivo
      ELSE
         x_DOCUMENTO_PAGO_MOP := 'CE';                       -- corresponsales
      END IF;
   ELSE
      x_DOCUMENTO_PAGO_MOP := :NEW.DOCUMENTO_PAGO_MOP;
   END IF;

   --
   IF NVL (:NEW.DOCUMENTO_PAGO_MTX, ' ') = ' '
      OR NVL (:NEW.DOCUMENTO_PAGO_MTX, '  ') = '  '
   THEN
      IF :NEW.moneda_transaccion = 'CLP'
      THEN
         x_DOCUMENTO_PAGO_MTX := 'EF';                             -- efectivo
      ELSE
         x_DOCUMENTO_PAGO_MTX := 'CE';                       -- corresponsales
      END IF;
   ELSE
      x_DOCUMENTO_PAGO_MTX := :NEW.DOCUMENTO_PAGO_MTX;
   END IF;

   --
   -- lleno el folio de tibco
   IF x_empresa = banco
   THEN
      x_emp_murex := 'STD';
   ELSIF x_empresa = corredora
   THEN
      x_emp_murex := 'CDB';
   ELSIF x_empresa = agencia
   THEN
      x_emp_murex := 'AGV';
   ELSIF x_empresa = investment
   THEN
      x_emp_murex := 'INV';
   ELSIF x_empresa = new_investment
   THEN
      x_emp_murex := 'SIL';                                   -- Nueva empresa
   ELSE
      x_emp_murex := 'XXX';
   END IF;  

   -- if nvl(:NEW.AREA_CONTABLE,'X') in ('B','D','S','W') then  --CARMEN
   IF NVL (:NEW.AREA_CONTABLE, 'X') IN ('B', 'D', 'S', 'W', 'C')
   THEN
      x_ID_OPERACION_TIBCO :=
            :NEW.AREA_CONTABLE
         || '-'
         || :new.id_operacion
         || '-mx-'
         || x_emp_murex;

      IF NVL (:NEW.AREA_CONTABLE, 'X') IN ('B')
      THEN
         x_ORIGIN := 'DIRECT';
      ELSIF NVL (:NEW.AREA_CONTABLE, 'X') IN ('D')
      THEN
         x_ORIGIN := 'DIRECT';
      ELSIF NVL (:NEW.AREA_CONTABLE, 'X') IN ('S')
      THEN
         x_ORIGIN := 'RED';   --'RED|TF'; 
         --Se agrega validación para identificar las operaciones de Caja    
         --mmoreno
        BEGIN
            SELECT COUNT(OS.NUMERO_OPERACION) 
            INTO v_origen 
            FROM OPER_SUCURSAL OS
            WHERE OS.NUMERO_OPERACION = :NEW.ID_OPERACION 
            AND   OS.ORIGEN_OPERACION = 'CAJA';
          
        EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        v_origen := NULL;
        WHEN OTHERS
        THEN
        v_origen := NULL;
        END;
         
         IF (v_origen >= 1) AND (NVL(:new.ind_new_tf,'*') <> 'S')
         THEN   
            x_ORIGIN := 'RED-CAJA'; 
         END IF;
      ELSIF NVL (:NEW.AREA_CONTABLE, 'X') IN ('C')
      THEN
         x_ORIGIN := 'CARMEN';
      ELSE
         x_ORIGIN := 'WEB';
      END IF;
   ELSIF :new.ind_new_tf = 'S'
   THEN
      x_ID_OPERACION_TIBCO :=
         'S-' || :new.id_operacion || '-mx-' || x_emp_murex;
      x_ORIGIN := 'RED';
   ELSE
      x_ID_OPERACION_TIBCO := :new.id_operacion || '-mx-' || x_emp_murex;
      x_ORIGIN := 'MESA';
   END IF; 


   IF :new.valuta_mop IS NULL
   THEN
      BEGIN
         x_fecha_aux := :new.fecha_vencimiento + 1;

         WHILE NOT ops$mesacam.pr_fecha_habil (x_fecha_aux, 'CHILE')
         LOOP
            x_fecha_aux := x_fecha_aux + 1;
            msj := 'fecha (' || TO_CHAR (x_fecha_aux) || ')';
            ops$mesacam.m_d (msj, idtrigger, smj);
         END LOOP;
      END;

      x_valuta_mop := x_fecha_aux;
   ELSE
      x_valuta_mop := :new.valuta_mop;
   END IF;

   -- Fecha de fixing para NDF
   IF :new.tipo_operacion IN ('CF', 'VF')
   THEN
      IF :new.id_producto IN
               ('MLCOMPCC',
                'MLCOMPCP',
                'MECOMPCC',
                'MECOMPCP',
                'UFCOMP',
                'MNCOMP')
      THEN
         BEGIN
            x_fecha_aux := :new.fecha_vencimiento - 1;

            WHILE NOT ops$mesacam.pr_fecha_habil (x_fecha_aux, 'CHILE')
            LOOP
               x_fecha_aux := x_fecha_aux - 1;
               msj := 'fecha fixing (' || TO_CHAR (x_fecha_aux) || ')';
               ops$mesacam.m_d (msj, idtrigger, smj);
            END LOOP;
         END;

         x_fecha_fixing := x_fecha_aux;
      END IF;
   END IF;

   --
   msj := 'valuta (' || TO_CHAR (x_valuta_mop) || ')';
   ops$mesacam.m_d (msj, idtrigger, smj);

   -- Cambio del trader
   IF x_id_trader NOT IN
            ('OPS$AMALFANC',
             'OPS$CRISLEANMX',
             'OPS$CPAULMX',
             'OPS$EALVAOYMX',
             'OPS$JBUSTAMRMX',
             'OPS$LMONTTLOMX',
             'OPS$OSALASFMN',
             'OPS$RODMACHOMX',
             'OPS$MDERURAMX',
             'OPS$AARAYACOMX',
             'OPS$APARDOGIMX',
             'OPS$JBARRIOPMX',
             'OPS$LTORRESMMX',
             'OPS$MACEVEDSMX',
             'OPS$CJIMENEUMX',
             'OPS$LAPEREZMX',
             'OPS$JLLARGOMX',
             'OPS$MRINGELIMX',
             'OPS$JBANDMX',
             'OPS$DBOCAZMA',
             'OPS$FBUSTAMAMX',
             'OPS$PABALLATMX',
             'OPS$RRIVACOMX',
             'OPS$LSILVASMX',
             'OPS$MABUSLEMX',
             'OPS$SGUZMANMX',
             'OPS$MBURGOSMX',
             'OPS$GRODRISMX',
             'OPS$RLARENAMX',
             'OPS$CMUNIZMX',
             'OPS$SIDEMX',
             'OPS$AVIVESMX',
             'OPS$JGREDILMX',
             'OPS$SBURMESMX',
             'OPS$RSCHEHIMX',
             'OPS$MLOPEZCMX',
             'OPS$RMOGROVMX',
             'OPS$AVASQ01MX',
             'OPS$BDELICMX',
             'OPS$SMAUREIMX',
             'OPS$JSANCHEMX',
             'OPS$MDIAZDMX',
             'OPS$MCHEMSIMX',
             'OPS$MZARATEMX',
             'OPS$JFREYMX',
             'OPS$RARTECHMX',
             'OPS$VNUNEZCMX',
             'OPS$MLUQUEMX',
             'OPS$FDELACBMX',
             'OPS$CRAMIRDMX',
             'OPS$JALIDMX',
             'OPS$JMORENOMX',
             'OPS$JEDWARSMX',
             'OPS$SWATSONMX',
             'OPS$MOLIV01MX',
             'OPS$AVALENDMX',
             'OPS$KCORADIMX',
             'OPS$FMANCCIMX',
             'OPS$PMALDOPMX',
             'OPS$JURZUAVMX',
             'OPS$ADIAZVMX',
             'OPS$JCORONAMX',
             'OPS$MGARCESMX',
             'OPS$CGOME02MX',
             'OPS$SESCOBAMX',
             'OPS$SRIVE01MX',
             'OPS$RVILLANMX',
             'OPS$AVICUNAMX',
             'OPS$FREIDMX',
             'OPS$CHERRESMX',
             'OPS$TNAUDONMX',
             'OPS$ACORRERMX',
             'OPS$MFUENZIMX',
             'OPS$MGRAVETMX',
             'OPS$GMARTICMX',
             'OPS$AROSSDEMX',
             'OPS$XDEARDEMX'
)
   THEN
      x_id_trader := 'OPS$FBUSTAMAMX';
   END IF;

   --
   IF INSERTING
   THEN
      -- Aqui va la logica que decide si la operacion va a ser enviada o no.
      -- Si se trata de algo grabado por el adaptador (crea_tibco = 'S')
      -- no enviamos. Si se crea Formalizada, se envia tambien sino
      -- no va.
      --
      msj :=
            'Dentro del inserting op('
         || operacion_nueva
         || ')   crea_tibco ('
         || :NEW.crea_tibco
         || ')   estado ('
         || :NEW.estado_operacion
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);

      --
      IF operacion_nueva = 'S' AND NVL (:new.crea_tibco, 'N') <> 'S'
         AND--      (:NEW.estado_operacion in ('F','M','C','N','E') or  nvl(:NEW.AREA_CONTABLE,'X') in ('D','B','S','W') or :new.ind_new_tf = 'S')  --CARMEN
            (   :NEW.estado_operacion IN ('F', 'M', 'C', 'N', 'E')
             OR NVL (:NEW.AREA_CONTABLE, 'X') IN ('D', 'B', 'S', 'W', 'C')
             OR:new.ind_new_tf = 'S')
      THEN
         -- pide numero de folio tibco para los registros.
         SELECT   sec_folio_tibco.NEXTVAL INTO foliotb FROM DUAL;

         --
         msj := 'Antes del insert foilo_tibco ==>' || foliotb;
         ops$mesacam.m_d (msj, idtrigger, smj);

         --
         -- Filtro portafolio desconocido.
         IF x_direccion = 'DESCONOCIDO'
         THEN
            x_estado_tibco := 'PORTA'; -- no publica portafolios desconocidos.
            msj :=
                  'Portafolio Desconocido. No se publica '
               || :NEW.id_operacion
               || ' Estado Tibco era ENVIA';
            ops$mesacam.m_d (msj, idtrigger, smj);
            msj :=
               'Error de portafolio. Estructura de la operacion no permite definirlo.';
            ops$mesacam.m_p (:NEW.empresa,
                             :NEW.id_operacion,
                             msj,
                             idtrigger,
                             smj);
         ELSE
            x_estado_tibco := 'ENVIA'; -- no publica portafolios desconocidos.
         END IF;

         -- modificacion para diferenciar llegada a MDR de llegada a Murex
         IF :new.tipo_operacion IN ('CD', 'TCD', 'VD', 'TVD')
             THEN
                x_utilidad_resultado := :new.monto_operacion;
                --x_precio_mx_tasa := :new.Precio_dolar_tasa;
                x_precio_mx_tasa := :new.precio_mx_tasa;
                x_precio_spot := :new.precio_mx_tasa;
                --Mejoras TF: GCP - ALSC
                IF (:new.area_contable = 'W')
                THEN
                   x_precio_mx_tasa := :new.Precio_dolar_tasa;
                END IF;
         END IF;

         IF :new.tipo_operacion IN ('CF','VF')
             THEN
                v_puntos_fwd_limpio := :new.PRECIO_TRANSF_FWD - :new.TASA_FUTURO;
                x_PRECIO_SPOT := :NEW.PRECIO_TRANSF_SPOT;
             ELSE
                v_puntos_fwd_limpio := :new.TASA_TRANF_FUTURO - x_PRECIO_SPOT;
             END IF;    
                     
         
         INSERT INTO ops$mesacam.OPERACIONES_TIBCO (id_operacion_tibco,
                                                    tipo_operacion,
                                                    fecha_operacion,
                                                    monto_operacion,
                                                    id_producto,
                                                    id_cliente,
                                                    moneda_operacion,
                                                    id_operacion,
                                                    estado_tibco,
                                                    folio_tibco,
                                                    estado_responder,
                                                    ind_contrato,
                                                    estado_operacion,
                                                    tipo_cambio_dolar,
                                                    cod_sucursal,
                                                    precio_mx_tasa_esp,
                                                    precio_financiero,
                                                    id_oper_origen,
                                                    codigo_area,
                                                    ID_TRADER,
                                                    cod_destino,
                                                    observacion,
                                                    documento_pago_mop,
                                                    monto_transaccion,
                                                    documento_pago_mtx,
                                                    fecha_crea,
                                                    id_usuario_crea,
                                                    ind_excede_entrega,
                                                    ind_exede_margen,
                                                    moneda_transaccion,
                                                    valuta_mop,
                                                    valuta_mtx,
                                                    vamos_vienen_pago,
                                                    empresa_origen,
                                                    empresa,
                                                    sub_empresa,
                                                    -- 2
                                                    banco_acreditar,
                                                    banco_debitar,
                                                    codigo_area_ing,
                                                    paridad_operacion,
                                                    monto_dolar,
                                                    monto_pesos,
                                                    paridad_transaccion,
                                                    fecha_act,
                                                    id_usuario_act,
                                                    razon_social,
                                                    ciudad,
                                                    comuna,
                                                    direccion,
                                                    telefono,
                                                    id_cliente_rut,
                                                    DELTA_COMP,
                                                    MONTO_AJUSTE,
                                                    MONTO_CAPITAL_PAG,
                                                    MONTO_CAPITAL_REC,
                                                    MONTO_DEVENGO,
                                                    MONTO_FINAL_PESOS,
                                                    PARIDAD_CLIENTE,
                                                    PARIDAD_NEGOCIO,
                                                    PARIDAD_COMPENSACION,
                                                    PARIDAD_REMANENTE,
                                                    PERDIDA_DEVENGO,
                                                    PERDIDA_RESULTADO,
                                                    PERIODO_COMP,
                                                    PRECIO_CONTABLE,
                                                    PRECIO_MN_TASA,
                                                    -- 3
                                                    PRECIO_MX_TASA,
                                                    PRECIO_OPERACION,
                                                    PRECIO_SPOT,
                                                    PRECIO_TRAN,
                                                    REAJUSTE_ACTIVO,
                                                    PRECIO_VENCIMIENTO,
                                                    REAJUSTE_PASIVO,
                                                    RESULTADO_BASILEA,
                                                    RIESGO_EQUIVALENTE,
                                                    TASA_COMPENSACION,
                                                    TASA_FUTURO,
                                                    TASA_MON_EXT,
                                                    TASA_MON_NAC,
                                                    TASA_TRANF_FUTURO,
                                                    TIPO_CAMBIO_COMP,
                                                    UTILIDAD_DEVENGO,
                                                    VALOR_FINAL,
                                                    UTILIDAD_RESULTADO,
                                                    VALOR_INICIAL,
                                                    BASE_CALCULO,
                                                    BASE_CALCULO_VARIABLE,
                                                    ID_CRITERIO,
                                                    ID_GLOSA,
                                                    N_OPER_COMEX,
                                                    F_ABONO,
                                                    F_DEBITO,
                                                    F_MT300,
                                                    FECHA_COMPENSACION,
                                                    FECHA_DEVENGO,
                                                    FECHA_ING_CUSTODIA,
                                                    -- 4
                                                    FECHA_INICIO,
                                                    FECHA_VENCIMIENTO,
                                                    FECHA_SAL_CUSTODIA,
                                                    VALUTA_COMP,
                                                    BASE_TASFUT,
                                                    C_ABONO,
                                                    C_DEBITO,
                                                    C_MT300,
                                                    COMISION,
                                                    CONFIRMA,
                                                    IND_ABNOPB,
                                                    IND_ACT_LIN_FUT,
                                                    IND_CURSE,
                                                    IND_DOCVAL,
                                                    IND_MULT_DIVI,
                                                    IND_EXCEDE_CLIENTE,
                                                    IND_EXCEDE_GRUPO,
                                                    IND_EXCEDE_ACTUALIZA,
                                                    IND_EXCEDE_PRODUC,
                                                    IND_EXEDE_LINEA,
                                                    IND_EXCEDE_SUBPROD,
                                                    IND_FAX,
                                                    IND_FICHA,
                                                    IND_IMPRESION_VVISTA,
                                                    IND_LIQUIDACION,
                                                    IND_PAPELETA,
                                                    IND_PLANILLA,
                                                    IND_SWIFT,
                                                    IND_VCTO,
                                                    TIPO_CALCULO,
                                                    -- 5
                                                    VAMOS_VIENEN_REEM,
                                                    CODIGO_CHEQUE,
                                                    ID_CLIENTE_SUC,
                                                    NUM_SUCURSAL,
                                                    AREA_CONTABLE,
                                                    FPAGO_COMPENSACION,
                                                    TIPO_CARTERA,
                                                    DESCRIBE_ATRIBUTO,
                                                    TIPO_DOLAR_CALCULO,
                                                    CONCEPTO_COMERCIO,
                                                    PAGADOR_DEBITAR,
                                                    DESC_AREA_CONTABLE,
                                                    INSTRUMENTO,
                                                    FORMA_TASA_PAG,
                                                    FORMA_TASA_REC,
                                                    MONEDA_PAGO_PAG,
                                                    MONEDA_PAGO_REC,
                                                    TIPO_DOLAR,
                                                    TIPO_INTERES_PAG,
                                                    TIPO_INTERES_REC,
                                                    TIPO_TASA_PAG,
                                                    TIPO_TASA_REC,
                                                    COD_COMERCIO,
                                                    PRECIO_DOLAR_TASA,
                                                    pagador_acreditar,
                                                    GESTOR,
                                                    MARK_UP,
                                                    divisa_mark_up,
                                                    ORIGIN,
                                                    PRECIO_TRANSF_SPOT,
                                                    PRECIO_TRANSF_FWD,
                                                    PUNTOS_FWD, PUNTOS_FWD_LIMPIO)
           VALUES   (x_ID_OPERACION_TIBCO,
                     :NEW.tipo_operacion,
                     :NEW.fecha_operacion,
                     X_monto_operacion,
                     :NEW.id_producto,
                     x_id_cliente_paso,
                     :NEW.moneda_operacion,
                     :NEW.id_operacion,
                     x_estado_tibco,
                     foliotb,
                     'NUEVA',
                     :NEW.ind_contrato,
                     :NEW.estado_operacion,
                     :NEW.tipo_cambio_dolar,
                     :NEW.cod_sucursal,
                     :NEW.precio_mx_tasa_esp,
                     :NEW.precio_financiero,
                     :NEW.id_oper_origen,
                     :NEW.codigo_area,
                     x_id_trader,
                     :NEW.cod_destino,
                     SUBSTR (:NEW.observacion, 1, 70),
                     x_documento_pago_mop,
                     :NEW.monto_transaccion,
                     x_documento_pago_mtx,
                     :NEW.fecha_crea,
                     :NEW.id_usuario_crea,
                     :NEW.ind_excede_entrega,
                     :NEW.ind_exede_margen,
                     :NEW.moneda_transaccion,
                     x_valuta_mop,
                     :NEW.valuta_mtx,
                     :NEW.vamos_vienen_pago,
                     x_empresa_origen,
                     :NEW.empresa,
                     :NEW.sub_empresa,
                     -- 2
                     :NEW.banco_acreditar,
                     :NEW.banco_debitar,
                     :NEW.codigo_area_ing,
                     x_paridad_operacion,
                     :NEW.monto_dolar,
                     X_monto_pesos,
                     x_paridad_transaccion,
                     SYSDATE,
                     :NEW.id_usuario_crea,
                     x_razon_social,
                     x_ciudad,
                     x_comuna,
                     x_direccion,
                     x_telefono,
                     x_id_cliente_rut,
                     :NEW.DELTA_COMP,
                     :NEW.MONTO_AJUSTE,
                     :NEW.MONTO_CAPITAL_PAG,
                     :NEW.MONTO_CAPITAL_REC,
                     :NEW.MONTO_DEVENGO,
                     :NEW.MONTO_FINAL_PESOS,
                     :NEW.PARIDAD_CLIENTE,
                     :NEW.PARIDAD_NEGOCIO,
                     :NEW.PARIDAD_COMPENSACION,
                     :NEW.PARIDAD_REMANENTE,
                     :NEW.PERDIDA_DEVENGO,
                     :NEW.PERDIDA_RESULTADO,
                     :NEW.PERIODO_COMP,
                     :NEW.PRECIO_CONTABLE,
                     :NEW.PRECIO_MN_TASA,
                     -- 3
                     X_PRECIO_MX_TASA,
                     :NEW.PRECIO_OPERACION,
                     x_PRECIO_SPOT,
                     :NEW.PRECIO_TRAN,
                     :NEW.REAJUSTE_ACTIVO,
                     :NEW.PRECIO_VENCIMIENTO,
                     :NEW.REAJUSTE_PASIVO,
                     :NEW.RESULTADO_BASILEA,
                     :NEW.RIESGO_EQUIVALENTE,
                     :NEW.TASA_COMPENSACION,
                     :NEW.TASA_FUTURO,
                     :NEW.TASA_MON_EXT,
                     :NEW.TASA_MON_NAC,
                     :NEW.TASA_TRANF_FUTURO,
                     :NEW.TIPO_CAMBIO_COMP,
                     :NEW.UTILIDAD_DEVENGO,
                     :NEW.VALOR_FINAL,
                     X_UTILIDAD_RESULTADO,
                     :NEW.VALOR_INICIAL,
                     :NEW.BASE_CALCULO,
                     :NEW.BASE_CALCULO_VARIABLE,
                     :NEW.ID_CRITERIO,
                     :NEW.ID_GLOSA,
                     :NEW.N_OPER_COMEX,
                     :NEW.F_ABONO,
                     :NEW.F_DEBITO,
                     x_fecha_fixing,
                     :NEW.FECHA_COMPENSACION,
                     :NEW.FECHA_DEVENGO,
                     :NEW.FECHA_ING_CUSTODIA,
                     -- 4
                     :NEW.FECHA_INICIO,
                     :NEW.FECHA_VENCIMIENTO,
                     :NEW.FECHA_SAL_CUSTODIA,
                     :NEW.VALUTA_COMP,
                     :NEW.BASE_TASFUT,
                     :NEW.C_ABONO,
                     :NEW.C_DEBITO,
                     :NEW.C_MT300,
                     :NEW.COMISION,
                     :NEW.CONFIRMA,
                     :NEW.IND_ABNOPB,
                     :NEW.IND_ACT_LIN_FUT,
                     :NEW.IND_CURSE,
                     :NEW.IND_DOCVAL,
                     x_ind_mult_divi,
                     :NEW.IND_EXCEDE_CLIENTE,
                     :NEW.IND_EXCEDE_GRUPO,
                     :NEW.IND_EXCEDE_ACTUALIZA,
                     :NEW.IND_EXCEDE_PRODUC,
                     :NEW.IND_EXEDE_LINEA,
                     :NEW.IND_EXCEDE_SUBPROD,
                     :NEW.IND_FAX,
                     :NEW.IND_FICHA,
                     :NEW.IND_IMPRESION_VVISTA,
                     :NEW.IND_LIQUIDACION,
                     :NEW.IND_PAPELETA,
                     :NEW.IND_PLANILLA,
                     :NEW.IND_SWIFT,
                     :NEW.IND_VCTO,
                     :NEW.TIPO_CALCULO,
                     -- 5
                     :NEW.VAMOS_VIENEN_REEM,
                     x_FIELD21,
                     :NEW.ID_CLIENTE_SUC,
                     :NEW.NUM_SUCURSAL,
                     :NEW.AREA_CONTABLE,
                     :NEW.FPAGO_COMPENSACION,
                     :NEW.TIPO_CARTERA,
                     :NEW.DESCRIBE_ATRIBUTO,
                     :NEW.TIPO_DOLAR_CALCULO,
                     :NEW.CONCEPTO_COMERCIO,
                     :NEW.PAGADOR_DEBITAR,
                     :NEW.DESC_AREA_CONTABLE,
                     :NEW.INSTRUMENTO,
                     :NEW.FORMA_TASA_PAG,
                     :NEW.FORMA_TASA_REC,
                     :NEW.MONEDA_PAGO_PAG,
                     :NEW.MONEDA_PAGO_REC,
                     :NEW.TIPO_DOLAR,
                     :NEW.TIPO_INTERES_PAG,
                     :NEW.TIPO_INTERES_REC,
                     :NEW.TIPO_TASA_PAG,
                     :NEW.TIPO_TASA_REC,
                     :NEW.COD_COMERCIO,
                     x_precio_dolar_tasa,
                     :NEW.NUMERO_PROPUESTA,
                     :NEW.GESTOR,
                     :NEW.MARK_UP,
                     :NEW.divisa_mark_up,
                     DECODE(x_ORIGIN,'RED','RED-TF',x_ORIGIN),
                     :NEW.PRECIO_TRANSF_SPOT,
                     :NEW.PRECIO_TRANSF_FWD,
                     :NEW.PUNTOS_FWD,
                     v_puntos_fwd_limpio
                     --:NEW.TASA_TRANF_FUTURO - x_PRECIO_SPOT
                     );

         -- mensaje de prueba
         msj := 'Fin del insert del Insert';
         ops$mesacam.m_d (msj, idtrigger, smj);

         --
         ---------------------------------------------------------------------------------------------------------------------------------------------
         ---------------------------------------------------------------------------------------------------------------------------------------------
         ------------ Nuevo insert de SPOT INTERNA ---------------------------------------------------------------------------------------------------
         IF :new.tipo_operacion IN ('CF', 'VF') AND x_empresa = banco
         THEN
            BEGIN
               -- pide numero de folio tibco para los registros.
               SELECT   sec_folio_tibco.NEXTVAL INTO foliotb FROM DUAL;

               --
               msj := 'Antes del insert foilo_tibco ==>' || foliotb;
               ops$mesacam.m_d (msj, idtrigger, smj);

               -- foliotb

               --- para tipos  S- W- B- D- -----

               IF SUBSTR (x_ID_OPERACION_TIBCO, 2, 1) = '-'
               THEN
                  x_ID_OPERACION_TIBCO :=
                     'I'
                     || SUBSTR (x_ID_OPERACION_TIBCO,
                                2,
                                LENGTH (x_id_operacion_tibco));
               ELSE
                  x_ID_OPERACION_TIBCO := 'I-' || x_ID_OPERACION_TIBCO;
               END IF;

               x_id_cliente_rut := '222222-1';
               x_id_cliente_paso := '9RCH';

               -- averigurar el real ---
               --                  x_ciudad    :=   x_direccion;
               --                  x_direccion :=  'V_MM_SPOT_STD';
               x_ciudad := 'V_MM_SPOT_STD';

               IF x_ind_mult_divi = 'D'
               THEN
                  x_ind_mult_divi := 'M';
               ELSE
                  x_ind_mult_divi := 'D';
               END IF;

               IF :new.tipo_operacion = 'CF'
               THEN
                  x_tipo_operacion := 'VD';
                  x_banco_debitar := 'BANKERS';
               ELSE
                  x_tipo_operacion := 'CD';
                  x_banco_acreditar := 'BANKERS';
               END IF;


               x_id_operacion := -:NEW.id_operacion;
               x_DOCUMENTO_PAGO_MOP := NULL;
               x_DOCUMENTO_PAGO_MTX := NULL;

               --x_precio_mx_tasa     := 1;

               x_precio_dolar_tasa := x_precio_spot;
               x_precio_mx_tasa := x_precio_spot;
               x_tipo_cambio_dolar := x_precio_spot;
               x_paridad_operacion := x_precio_spot;
               x_paridad_transaccion := x_precio_spot; -- Arreglo de precio transferencia.

               
--               IF x_ORIGIN = 'RED'
               -- JPR 26062019   INI
               IF (x_ORIGIN = 'RED' OR x_ORIGIN = 'RED-CAJA')   
               -- JPR 26062019   FIN
               THEN
                  x_area_contable := 'S';
               ELSE
                  x_area_contable := NULL;
               END IF;

               BEGIN
                  x_fecha_aux := :new.fecha_operacion + 1;

                  WHILE NOT ops$mesacam.pr_fecha_habil (x_fecha_aux, 'CHILE')
                  LOOP
                     x_fecha_aux := x_fecha_aux + 1;
                     msj := 'fecha (' || TO_CHAR (x_fecha_aux) || ')';
                     ops$mesacam.m_d (msj, idtrigger, smj);
                  END LOOP;
               END;

               x_valuta_mop := x_fecha_aux;
               x_valuta_mtx := x_fecha_aux;             -- Arreglo de valutas.

               --
               msj := 'valuta spot Interna (' || TO_CHAR (x_valuta_mop) || ')';
               ops$mesacam.m_d (msj, idtrigger, smj);
               --

               x_utilidad_resultado :=
                  ROUND (:new.monto_transaccion * x_precio_spot, 0);
               x_monto_operacion :=
                  ROUND (:new.monto_transaccion * x_precio_spot, 0);
               x_valor_inicial :=
                  ROUND (:new.monto_transaccion * x_precio_spot, 0);

               --
               msj :=
                     'Monto Operacion v/s Nuevo Monto :('
                  || TO_CHAR (:new.monto_operacion)
                  || ' ; '
                  || TO_CHAR (x_monto_operacion)
                  || ')';
               ops$mesacam.m_d (msj, idtrigger, smj);
                           

               INSERT INTO ops$mesacam.OPERACIONES_TIBCO (
                                                             id_operacion_tibco,
                                                             tipo_operacion,
                                                             fecha_operacion,
                                                             monto_operacion,
                                                             id_producto,
                                                             id_cliente,
                                                             moneda_operacion,
                                                             id_operacion,
                                                             estado_tibco,
                                                             folio_tibco,
                                                             estado_responder,
                                                             ind_contrato,
                                                             estado_operacion,
                                                             tipo_cambio_dolar,
                                                             cod_sucursal,
                                                             precio_mx_tasa_esp,
                                                             precio_financiero,
                                                             id_oper_origen,
                                                             codigo_area,
                                                             ID_TRADER,
                                                             cod_destino,
                                                             observacion,
                                                             documento_pago_mop,
                                                             monto_transaccion,
                                                             documento_pago_mtx,
                                                             fecha_crea,
                                                             id_usuario_crea,
                                                             ind_excede_entrega,
                                                             ind_exede_margen,
                                                             moneda_transaccion,
                                                             valuta_mop,
                                                             valuta_mtx,
                                                             vamos_vienen_pago,
                                                             empresa_origen,
                                                             empresa,
                                                             sub_empresa,
                                                             -- 2
                                                             banco_acreditar,
                                                             banco_debitar,
                                                             codigo_area_ing,
                                                             paridad_operacion,
                                                             monto_dolar,
                                                             monto_pesos,
                                                             paridad_transaccion,
                                                             fecha_act,
                                                             id_usuario_act,
                                                             razon_social,
                                                             ciudad,
                                                             comuna,
                                                             direccion,
                                                             telefono,
                                                             id_cliente_rut,
                                                             DELTA_COMP,
                                                             MONTO_AJUSTE,
                                                             MONTO_CAPITAL_PAG,
                                                             MONTO_CAPITAL_REC,
                                                             MONTO_DEVENGO,
                                                             MONTO_FINAL_PESOS,
                                                             PARIDAD_CLIENTE,
                                                             PARIDAD_NEGOCIO,
                                                             PARIDAD_COMPENSACION,
                                                             PARIDAD_REMANENTE,
                                                             PERDIDA_DEVENGO,
                                                             PERDIDA_RESULTADO,
                                                             PERIODO_COMP,
                                                             PRECIO_CONTABLE,
                                                             PRECIO_MN_TASA,
                                                             -- 3
                                                             PRECIO_MX_TASA,
                                                             PRECIO_OPERACION,
                                                             PRECIO_SPOT,
                                                             PRECIO_TRAN,
                                                             REAJUSTE_ACTIVO,
                                                             PRECIO_VENCIMIENTO,
                                                             REAJUSTE_PASIVO,
                                                             RESULTADO_BASILEA,
                                                             RIESGO_EQUIVALENTE,
                                                             TASA_COMPENSACION,
                                                             TASA_FUTURO,
                                                             TASA_MON_EXT,
                                                             TASA_MON_NAC,
                                                             TASA_TRANF_FUTURO,
                                                             TIPO_CAMBIO_COMP,
                                                             UTILIDAD_DEVENGO,
                                                             VALOR_FINAL,
                                                             UTILIDAD_RESULTADO,
                                                             VALOR_INICIAL,
                                                             BASE_CALCULO,
                                                             BASE_CALCULO_VARIABLE,
                                                             ID_CRITERIO,
                                                             ID_GLOSA,
                                                             N_OPER_COMEX,
                                                             F_ABONO,
                                                             F_DEBITO,
                                                             F_MT300,
                                                             FECHA_COMPENSACION,
                                                             FECHA_DEVENGO,
                                                             FECHA_ING_CUSTODIA,
                                                             -- 4
                                                             FECHA_INICIO,
                                                             FECHA_VENCIMIENTO,
                                                             FECHA_SAL_CUSTODIA,
                                                             VALUTA_COMP,
                                                             BASE_TASFUT,
                                                             C_ABONO,
                                                             C_DEBITO,
                                                             C_MT300,
                                                             COMISION,
                                                             CONFIRMA,
                                                             IND_ABNOPB,
                                                             IND_ACT_LIN_FUT,
                                                             IND_CURSE,
                                                             IND_DOCVAL,
                                                             IND_MULT_DIVI,
                                                             IND_EXCEDE_CLIENTE,
                                                             IND_EXCEDE_GRUPO,
                                                             IND_EXCEDE_ACTUALIZA,
                                                             IND_EXCEDE_PRODUC,
                                                             IND_EXEDE_LINEA,
                                                             IND_EXCEDE_SUBPROD,
                                                             IND_FAX,
                                                             IND_FICHA,
                                                             IND_IMPRESION_VVISTA,
                                                             IND_LIQUIDACION,
                                                             IND_PAPELETA,
                                                             IND_PLANILLA,
                                                             IND_SWIFT,
                                                             IND_VCTO,
                                                             TIPO_CALCULO,
                                                             -- 5
                                                             VAMOS_VIENEN_REEM,
                                                             CODIGO_CHEQUE,
                                                             ID_CLIENTE_SUC,
                                                             NUM_SUCURSAL,
                                                             AREA_CONTABLE,
                                                             FPAGO_COMPENSACION,
                                                             TIPO_CARTERA,
                                                             DESCRIBE_ATRIBUTO,
                                                             TIPO_DOLAR_CALCULO,
                                                             CONCEPTO_COMERCIO,
                                                             PAGADOR_DEBITAR,
                                                             DESC_AREA_CONTABLE,
                                                             INSTRUMENTO,
                                                             FORMA_TASA_PAG,
                                                             FORMA_TASA_REC,
                                                             MONEDA_PAGO_PAG,
                                                             MONEDA_PAGO_REC,
                                                             TIPO_DOLAR,
                                                             TIPO_INTERES_PAG,
                                                             TIPO_INTERES_REC,
                                                             TIPO_TASA_PAG,
                                                             TIPO_TASA_REC,
                                                             COD_COMERCIO,
                                                             PRECIO_DOLAR_TASA,
                                                             pagador_acreditar,
                                                             GESTOR,
                                                             MARK_UP,
                                                             divisa_mark_up,
                                                             ORIGIN
                          )
                 VALUES   (x_ID_OPERACION_TIBCO,
                           x_tipo_operacion,
                           :NEW.fecha_operacion,
                           X_monto_operacion,
                           'EMP_NAC',
                           x_id_cliente_paso,
                           :NEW.moneda_operacion,
                           x_id_operacion,
                           x_estado_tibco,
                           foliotb,
                           'NUEVA',
                           :NEW.ind_contrato,
                           :NEW.estado_operacion,
                           x_tipo_cambio_dolar,
                           :NEW.cod_sucursal,
                           NULL,
                           0,
                           '-' || :NEW.id_oper_origen,
                           :NEW.codigo_area,
                           x_id_trader,
                           :NEW.cod_destino,
                           SUBSTR (:new.observacion, 1, 70),
                           x_documento_pago_mop,
                           :NEW.monto_transaccion,
                           x_documento_pago_mtx,
                           :NEW.fecha_crea,
                           :NEW.id_usuario_crea,
                           :NEW.ind_excede_entrega,
                           :NEW.ind_exede_margen,
                           :NEW.moneda_transaccion,
                           x_valuta_mop,
                           x_valuta_mtx,
                           :NEW.vamos_vienen_pago,
                           x_empresa_origen,
                           :NEW.empresa,
                           :NEW.sub_empresa,
                           -- 2
                           x_banco_acreditar,
                           x_banco_debitar,
                           :NEW.codigo_area_ing,
                           x_paridad_operacion,
                           :NEW.monto_dolar,
                           X_monto_pesos,
                           x_paridad_transaccion,
                           SYSDATE,
                           :NEW.id_usuario_crea,
                           x_razon_social,
                           x_ciudad,
                           x_comuna,
                           x_direccion,
                           x_telefono,
                           x_id_cliente_rut,
                           :NEW.DELTA_COMP,
                           :NEW.MONTO_AJUSTE,
                           :NEW.MONTO_CAPITAL_PAG,
                           :NEW.MONTO_CAPITAL_REC,
                           :NEW.MONTO_DEVENGO,
                           NULL,
                           :NEW.PARIDAD_CLIENTE,
                           :NEW.PARIDAD_NEGOCIO,
                           :NEW.PARIDAD_COMPENSACION,
                           :NEW.PARIDAD_REMANENTE,
                           :NEW.PERDIDA_DEVENGO,
                           :NEW.PERDIDA_RESULTADO,
                           :NEW.PERIODO_COMP,
                           :NEW.PRECIO_CONTABLE,
                           0,
                           -- 3
                           X_PRECIO_MX_TASA,
                           :NEW.PRECIO_OPERACION,
                           x_PRECIO_SPOT,
                           :NEW.PRECIO_TRAN,
                           :NEW.REAJUSTE_ACTIVO,
                           :NEW.PRECIO_VENCIMIENTO,
                           :NEW.REAJUSTE_PASIVO,
                           :NEW.RESULTADO_BASILEA,
                           :NEW.RIESGO_EQUIVALENTE,
                           :NEW.TASA_COMPENSACION,
                           :NEW.TASA_FUTURO,
                           NULL,
                           NULL,
                           0,
                           :NEW.TIPO_CAMBIO_COMP,
                           :NEW.UTILIDAD_DEVENGO,
                           :NEW.VALOR_FINAL,
                           X_UTILIDAD_RESULTADO,
                           x_VALOR_INICIAL,
                           NULL,
                           :NEW.BASE_CALCULO_VARIABLE,
                           :NEW.ID_CRITERIO,
                           NULL,
                           :NEW.N_OPER_COMEX,
                           :NEW.F_ABONO,
                           :NEW.F_DEBITO,
                           NULL,
                           :NEW.fecha_operacion,
                           :NEW.FECHA_DEVENGO,
                           :NEW.FECHA_ING_CUSTODIA,
                           -- 4
                           :NEW.fecha_operacion,
                           :NEW.fecha_operacion,
                           :NEW.FECHA_SAL_CUSTODIA,
                           :NEW.VALUTA_COMP,
                           :NEW.BASE_TASFUT,
                           :NEW.C_ABONO,
                           :NEW.C_DEBITO,
                           :NEW.C_MT300,
                           :NEW.COMISION,
                           :NEW.CONFIRMA,
                           :NEW.IND_ABNOPB,
                           NULL,
                           '*',
                           :NEW.IND_DOCVAL,
                           x_ind_mult_divi,
                           :NEW.IND_EXCEDE_CLIENTE,
                           :NEW.IND_EXCEDE_GRUPO,
                           :NEW.IND_EXCEDE_ACTUALIZA,
                           :NEW.IND_EXCEDE_PRODUC,
                           :NEW.IND_EXEDE_LINEA,
                           :NEW.IND_EXCEDE_SUBPROD,
                           :NEW.IND_FAX,
                           :NEW.IND_FICHA,
                           :NEW.IND_IMPRESION_VVISTA,
                           :NEW.IND_LIQUIDACION,
                           :NEW.IND_PAPELETA,
                           :NEW.IND_PLANILLA,
                           :NEW.IND_SWIFT,
                           :NEW.IND_VCTO,
                           :NEW.TIPO_CALCULO,
                           -- 5
                           :NEW.VAMOS_VIENEN_REEM,
                           x_FIELD21,
                           :NEW.ID_CLIENTE_SUC,
                           :NEW.NUM_SUCURSAL,
                           x_AREA_CONTABLE,
                           :NEW.FPAGO_COMPENSACION,
                           NULL,
                           :NEW.DESCRIBE_ATRIBUTO,
                           NULL,
                           NULL,
                           :NEW.PAGADOR_DEBITAR,
                           NULL,
                           :NEW.INSTRUMENTO,
                           :NEW.FORMA_TASA_PAG,
                           :NEW.FORMA_TASA_REC,
                           :NEW.MONEDA_PAGO_PAG,
                           :NEW.MONEDA_PAGO_REC,
                           :NEW.TIPO_DOLAR,
                           :NEW.TIPO_INTERES_PAG,
                           :NEW.TIPO_INTERES_REC,
                           :NEW.TIPO_TASA_PAG,
                           :NEW.TIPO_TASA_REC,
                           :NEW.COD_COMERCIO,
                           x_precio_dolar_tasa,
                           :NEW.NUMERO_PROPUESTA,
                           :NEW.GESTOR,
                           0,
                           :NEW.divisa_mark_up,              -- Markup en cero
                           DECODE(x_ORIGIN,'RED','RED-TF',x_ORIGIN)  -- JPR 26062019
                           );

               -- mensaje de prueba
               msj := 'Fin del insert del espejo en el Insert';
               ops$mesacam.m_d (msj, idtrigger, smj);
            EXCEPTION
               WHEN OTHERS
               THEN
                  --
                  msj := ' Op (' || :NEW.id_operacion || ') ' || SQLERRM;
                  ops$mesacam.m_d (msj, idtrigger, smj);
                  msj :=
                     '**** Error al crear operacion espejo del forward (Inserting). ****';
                  ops$mesacam.m_p (:NEW.empresa,
                                   :NEW.id_operacion,
                                   msj,
                                   idtrigger,
                                   smj);
                  RAISE salida;
            END;
         ----------------------------------------------------------------------------------------------------------------------------------------------
         ----------------------------------------------------------------------------------------------------------------------------------------------
         ------------  Fin  insert de SPOT INTERNA ----------------------------------------------------------------------------------------------------
         END IF;
      END IF;
   END IF;

   --
   IF UPDATING
   THEN
      -- Aqui va la logica que decide si la operacion va a ser enviada o no.
      -- Si se trata de algo grabado por el adaptador, lo enviamos
      -- siempre que el BackOffice haya Formalizado o si el update es por la
      -- eliminacion de la operacion (si ya esta eliminada no se envia dos veces).
      --
      msj :=
            'dentro del Updating  estado [ old ('
         || :OLD.estado_operacion
         || ')  new ('
         || :NEW.estado_operacion
         || ')]  opnueva ('
         || operacion_nueva
         || ')';
      ops$mesacam.m_d (msj, idtrigger, smj);

      IF   (:NEW.estado_operacion IN ('F', 'M'))
        OR (:NEW.estado_operacion = 'C' AND :OLD.estado_operacion = 'Z')
        OR (operacion_nueva = 'N' AND NVL (:NEW.estado_operacion, '.') = 'X' AND NVL (:OLD.estado_operacion, '.') <> 'X')

      THEN
         -- pide numero de folio tibco para los registros.
         SELECT   sec_folio_tibco.NEXTVAL INTO foliotb FROM DUAL;

         --
         --  Fue eliminada de tibco ?
         IF NVL (:NEW.estado_operacion, '.') = 'X'
         THEN
            BEGIN
               --
               msj := 'Op. ' || :NEW.id_operacion || ' antes de la pregunta';
               ops$mesacam.m_d (msj, idtrigger, smj);
               --
               eliminada_de_tibco := 'S';

               -- lo elimino tibco.
               -- esto controla cuando la modificacion ocurre por el trigger t_operaciones_tibco.
               IF NVL (:NEW.act_tibco, 'N') <> 'S'
               THEN
                  SELECT   tipo_operacion
                    INTO   x_dummy
                    FROM   ops$mesacam.operaciones_tibco
                   WHERE       id_operacion = :NEW.id_operacion
                           AND empresa = :NEW.empresa
                           AND estado_tibco = 'DELET';
               ELSE
                  --
                  msj :=
                     'Op. ' || :NEW.id_operacion || ' eliminada desde tibco';
                  ops$mesacam.m_d (msj, idtrigger, smj);
               --
               END IF;

               --
               msj := 'Op. ' || :NEW.id_operacion || ' eliminada desde tibco';
               ops$mesacam.m_d (msj, idtrigger, smj);
            --
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  eliminada_de_tibco := 'N';
                  --
                  msj :=
                        'Op. '
                     || :NEW.id_operacion
                     || ' no eliminada desde tibco';
                  ops$mesacam.m_d (msj, idtrigger, smj);
               --
               WHEN TOO_MANY_ROWS
               THEN
                  --
                  msj :=
                        'Op. '
                     || :NEW.id_operacion
                     || ' eliminada desde tibco, too_many_rows';
                  ops$mesacam.m_d (msj, idtrigger, smj);
               --
               WHEN OTHERS
               THEN
                  --
                  msj :=
                     'others en select del si fue delete de tibco '
                     || SQLERRM;
                  ops$mesacam.m_d (msj, idtrigger, smj);
                  msj :=
                     '**** Error de sistema al intentar anular una operacion. ****';
                  ops$mesacam.m_p (:NEW.empresa,
                                   :NEW.id_operacion,
                                   msj,
                                   idtrigger,
                                   smj);
                  RAISE salida;
            --
            END;
         --
         END IF;

         -- decide si envia un delete o una nueva operacion para tibco o una
         -- modificacion de alguna existente.
         x_estado_tibco := 'ENVIA';

         IF :NEW.estado_operacion = 'X'
         THEN
            IF eliminada_de_tibco = 'N'
            THEN
               x_estado_respo := 'DELET';
            ELSE
               x_estado_respo := 'DELET';
               x_estado_tibco := 'X';            -- no publica modificaciones.
            END IF;
         ELSE
            IF operacion_nueva = 'S'
            THEN
               x_estado_respo := 'NUEVA';
            ELSE
               x_estado_respo := 'MODIF';
               x_estado_tibco := 'X';            -- no publica modificaciones.
            END IF;
         END IF;

         -- PREPARA SALIDA SI ES QUE SE TRATA DE UNA MODIFICACION
         IF x_estado_tibco = 'X'
         THEN
            --
            msj := 'No se inserta, estado_tibco = X';
            ops$mesacam.m_d (msj, idtrigger, smj);
            RAISE salida;
         --
         END IF;

         --
         msj := 'Antes del insert del Update ';
         ops$mesacam.m_d (msj, idtrigger, smj);

         --
         -- Filtro portafolio desconocido.
         IF x_direccion = 'DESCONOCIDO'
         THEN
            IF x_estado_respo = 'DELET' AND x_estado_tibco = 'ENVIA'
            THEN
               x_estado_tibco := 'PORTA'; -- no publica portafolios desconocidos.
               msj :=
                  'Update Portafolio Desconocido. Es Delete, pero no se publica. ('
                  || :NEW.id_operacion
                  || ')';
               ops$mesacam.m_d (msj, idtrigger, smj);
            ELSE
               x_estado_tibco := 'PORTA'; -- no publica portafolios desconocidos.
               msj :=
                     'Update Portafolio Desconocido. No se publica ('
                  || :NEW.id_operacion
                  || ') estado_tibco original ('
                  || x_estado_tibco
                  || ')';
               ops$mesacam.m_d (msj, idtrigger, smj);
               msj :=
                  'Error de portafolio. Estructura de la operacion no permite definirlo.';
               ops$mesacam.m_p (:NEW.empresa,
                                :NEW.id_operacion,
                                msj,
                                idtrigger,
                                smj);
            END IF;
         END IF;

         -- modificacion para diferenciar llegada a MDR de llegada a Murex
         IF :new.tipo_operacion IN ('CD', 'TCD', 'VD', 'TVD')
         THEN
            x_utilidad_resultado := :new.monto_operacion;
             --x_precio_mx_tasa := :new.Precio_dolar_tasa;
            x_precio_mx_tasa := :new.precio_mx_tasa;
            x_precio_spot := :new.precio_mx_tasa;
         END IF;


         IF :new.tipo_operacion IN ('CF','VF')
             THEN
                v_puntos_fwd_limpio := :new.PRECIO_TRANSF_FWD - :new.TASA_FUTURO;
                x_PRECIO_SPOT := :NEW.PRECIO_TRANSF_SPOT;
             ELSE
                v_puntos_fwd_limpio := :new.TASA_TRANF_FUTURO - x_PRECIO_SPOT;
             END IF;  
                  

         INSERT INTO ops$mesacam.OPERACIONES_TIBCO (id_operacion_tibco,
                                                    tipo_operacion,
                                                    fecha_operacion,
                                                    monto_operacion,
                                                    id_producto,
                                                    id_cliente,
                                                    moneda_operacion,
                                                    id_operacion,
                                                    estado_tibco,
                                                    folio_tibco,
                                                    ind_contrato,
                                                    estado_operacion,
                                                    estado_responder,
                                                    tipo_cambio_dolar,
                                                    cod_sucursal,
                                                    precio_mx_tasa_esp,
                                                    precio_financiero,
                                                    id_oper_origen,
                                                    codigo_area,
                                                    id_trader,
                                                    cod_destino,
                                                    observacion,
                                                    documento_pago_mop,
                                                    monto_transaccion,
                                                    documento_pago_mtx,
                                                    fecha_crea,
                                                    id_usuario_crea,
                                                    ind_excede_entrega,
                                                    ind_exede_margen,
                                                    moneda_transaccion,
                                                    valuta_mop,
                                                    valuta_mtx,
                                                    vamos_vienen_pago,
                                                    empresa_origen,
                                                    empresa,
                                                    sub_empresa,
                                                    -- 2
                                                    banco_acreditar,
                                                    banco_debitar,
                                                    codigo_area_ing,
                                                    paridad_operacion,
                                                    monto_dolar,
                                                    monto_pesos,
                                                    paridad_transaccion,
                                                    fecha_act,
                                                    id_usuario_act,
                                                    razon_social,
                                                    ciudad,
                                                    comuna,
                                                    direccion,
                                                    telefono,
                                                    id_cliente_rut,
                                                    DELTA_COMP,
                                                    MONTO_AJUSTE,
                                                    MONTO_CAPITAL_PAG,
                                                    MONTO_CAPITAL_REC,
                                                    MONTO_DEVENGO,
                                                    MONTO_FINAL_PESOS,
                                                    PARIDAD_CLIENTE,
                                                    PARIDAD_NEGOCIO,
                                                    PARIDAD_COMPENSACION,
                                                    PARIDAD_REMANENTE,
                                                    PERDIDA_DEVENGO,
                                                    PERDIDA_RESULTADO,
                                                    PERIODO_COMP,
                                                    PRECIO_CONTABLE,
                                                    PRECIO_MN_TASA,
                                                    -- 3
                                                    PRECIO_MX_TASA,
                                                    PRECIO_OPERACION,
                                                    PRECIO_SPOT,
                                                    PRECIO_TRAN,
                                                    REAJUSTE_ACTIVO,
                                                    PRECIO_VENCIMIENTO,
                                                    REAJUSTE_PASIVO,
                                                    RESULTADO_BASILEA,
                                                    RIESGO_EQUIVALENTE,
                                                    TASA_COMPENSACION,
                                                    TASA_FUTURO,
                                                    TASA_MON_EXT,
                                                    TASA_MON_NAC,
                                                    TASA_TRANF_FUTURO,
                                                    TIPO_CAMBIO_COMP,
                                                    UTILIDAD_DEVENGO,
                                                    VALOR_FINAL,
                                                    UTILIDAD_RESULTADO,
                                                    VALOR_INICIAL,
                                                    BASE_CALCULO,
                                                    BASE_CALCULO_VARIABLE,
                                                    ID_CRITERIO,
                                                    ID_GLOSA,
                                                    N_OPER_COMEX,
                                                    F_ABONO,
                                                    F_DEBITO,
                                                    F_MT300,
                                                    FECHA_COMPENSACION,
                                                    FECHA_DEVENGO,
                                                    FECHA_ING_CUSTODIA,
                                                    -- 4
                                                    FECHA_INICIO,
                                                    FECHA_VENCIMIENTO,
                                                    FECHA_SAL_CUSTODIA,
                                                    VALUTA_COMP,
                                                    BASE_TASFUT,
                                                    C_ABONO,
                                                    C_DEBITO,
                                                    C_MT300,
                                                    COMISION,
                                                    CONFIRMA,
                                                    IND_ABNOPB,
                                                    IND_ACT_LIN_FUT,
                                                    IND_CURSE,
                                                    IND_DOCVAL,
                                                    IND_MULT_DIVI,
                                                    IND_EXCEDE_CLIENTE,
                                                    IND_EXCEDE_GRUPO,
                                                    IND_EXCEDE_ACTUALIZA,
                                                    IND_EXCEDE_PRODUC,
                                                    IND_EXEDE_LINEA,
                                                    IND_EXCEDE_SUBPROD,
                                                    IND_FAX,
                                                    IND_FICHA,
                                                    IND_IMPRESION_VVISTA,
                                                    IND_LIQUIDACION,
                                                    IND_PAPELETA,
                                                    IND_PLANILLA,
                                                    IND_SWIFT,
                                                    IND_VCTO,
                                                    TIPO_CALCULO,
                                                    -- 5
                                                    VAMOS_VIENEN_REEM,
                                                    CODIGO_CHEQUE,
                                                    ID_CLIENTE_SUC,
                                                    NUM_SUCURSAL,
                                                    AREA_CONTABLE,
                                                    FPAGO_COMPENSACION,
                                                    TIPO_CARTERA,
                                                    DESCRIBE_ATRIBUTO,
                                                    TIPO_DOLAR_CALCULO,
                                                    CONCEPTO_COMERCIO,
                                                    PAGADOR_DEBITAR,
                                                    DESC_AREA_CONTABLE,
                                                    INSTRUMENTO,
                                                    FORMA_TASA_PAG,
                                                    FORMA_TASA_REC,
                                                    MONEDA_PAGO_PAG,
                                                    MONEDA_PAGO_REC,
                                                    TIPO_DOLAR,
                                                    TIPO_INTERES_PAG,
                                                    TIPO_INTERES_REC,
                                                    TIPO_TASA_PAG,
                                                    TIPO_TASA_REC,
                                                    COD_COMERCIO,
                                                    PRECIO_DOLAR_TASA,
                                                    pagador_acreditar,
                                                    GESTOR,
                                                    MARK_UP,
                                                    divisa_mark_up,
                                                    ORIGIN,
                                                    PRECIO_TRANSF_SPOT,
                                                    PRECIO_TRANSF_FWD,
                                                    PUNTOS_FWD,PUNTOS_FWD_LIMPIO
                                                    )
           VALUES   (x_id_operacion_tibco,
                     :NEW.tipo_operacion,
                     :NEW.fecha_operacion,
                     X_monto_operacion,
                     :NEW.id_producto,
                     x_id_cliente_paso,
                     :NEW.moneda_operacion,
                     :NEW.id_operacion,
                     x_estado_tibco,
                     foliotb,
                     :NEW.ind_contrato,
                     :NEW.estado_operacion,
                     x_estado_respo,
                     :NEW.tipo_cambio_dolar,
                     :NEW.cod_sucursal,
                     :NEW.precio_mx_tasa_esp,
                     :NEW.precio_financiero,
                     :NEW.id_oper_origen,
                     :NEW.codigo_area,
                     x_id_trader,
                     :NEW.cod_destino,
                     SUBSTR (:NEW.observacion, 1, 70),
                     x_documento_pago_mop,
                     :NEW.monto_transaccion,
                     x_documento_pago_mtx,
                     :NEW.fecha_crea,
                     :NEW.id_usuario_crea,
                     :NEW.ind_excede_entrega,
                     :NEW.ind_exede_margen,
                     :NEW.moneda_transaccion,
                     x_valuta_mop,
                     :NEW.valuta_mtx,
                     :NEW.vamos_vienen_pago,
                     x_empresa_origen,
                     :NEW.empresa,
                     :NEW.sub_empresa,
                     -- 2
                     :NEW.banco_acreditar,
                     :NEW.banco_debitar,
                     :NEW.codigo_area_ing,
                     x_paridad_operacion,
                     :NEW.monto_dolar,
                     X_monto_pesos,
                     x_paridad_transaccion,
                     SYSDATE,
                     :NEW.id_usuario_crea,
                     x_razon_social,
                     x_ciudad,
                     x_comuna,
                     x_direccion,
                     x_telefono,
                     x_id_cliente_rut,
                     :NEW.DELTA_COMP,
                     :NEW.MONTO_AJUSTE,
                     :NEW.MONTO_CAPITAL_PAG,
                     :NEW.MONTO_CAPITAL_REC,
                     :NEW.MONTO_DEVENGO,
                     :NEW.MONTO_FINAL_PESOS,
                     :NEW.PARIDAD_CLIENTE,
                     :NEW.PARIDAD_NEGOCIO,
                     :NEW.PARIDAD_COMPENSACION,
                     :NEW.PARIDAD_REMANENTE,
                     :NEW.PERDIDA_DEVENGO,
                     :NEW.PERDIDA_RESULTADO,
                     :NEW.PERIODO_COMP,
                     :NEW.PRECIO_CONTABLE,
                     :NEW.PRECIO_MN_TASA,
                     -- 3
                     X_PRECIO_MX_TASA,
                     :NEW.PRECIO_OPERACION,
                     x_PRECIO_SPOT,
                     :NEW.PRECIO_TRAN,
                     :NEW.REAJUSTE_ACTIVO,
                     :NEW.PRECIO_VENCIMIENTO,
                     :NEW.REAJUSTE_PASIVO,
                     :NEW.RESULTADO_BASILEA,
                     :NEW.RIESGO_EQUIVALENTE,
                     :NEW.TASA_COMPENSACION,
                     :NEW.TASA_FUTURO,
                     :NEW.TASA_MON_EXT,
                     :NEW.TASA_MON_NAC,
                     :NEW.TASA_TRANF_FUTURO,
                     :NEW.TIPO_CAMBIO_COMP,
                     :NEW.UTILIDAD_DEVENGO,
                     :NEW.VALOR_FINAL,
                     X_UTILIDAD_RESULTADO,
                     :NEW.VALOR_INICIAL,
                     :NEW.BASE_CALCULO,
                     :NEW.BASE_CALCULO_VARIABLE,
                     :NEW.ID_CRITERIO,
                     :NEW.ID_GLOSA,
                     :NEW.N_OPER_COMEX,
                     :NEW.F_ABONO,
                     :NEW.F_DEBITO,
                     x_fecha_fixing,
                     :NEW.FECHA_COMPENSACION,
                     :NEW.FECHA_DEVENGO,
                     :NEW.FECHA_ING_CUSTODIA,
                     -- 4
                     :NEW.FECHA_INICIO,
                     :NEW.FECHA_VENCIMIENTO,
                     :NEW.FECHA_SAL_CUSTODIA,
                     :NEW.VALUTA_COMP,
                     :NEW.BASE_TASFUT,
                     :NEW.C_ABONO,
                     :NEW.C_DEBITO,
                     :NEW.C_MT300,
                     :NEW.COMISION,
                     :NEW.CONFIRMA,
                     :NEW.IND_ABNOPB,
                     :NEW.IND_ACT_LIN_FUT,
                     :NEW.IND_CURSE,
                     :NEW.IND_DOCVAL,
                     x_ind_mult_divi,
                     :NEW.IND_EXCEDE_CLIENTE,
                     :NEW.IND_EXCEDE_GRUPO,
                     :NEW.IND_EXCEDE_ACTUALIZA,
                     :NEW.IND_EXCEDE_PRODUC,
                     :NEW.IND_EXEDE_LINEA,
                     :NEW.IND_EXCEDE_SUBPROD,
                     :NEW.IND_FAX,
                     :NEW.IND_FICHA,
                     :NEW.IND_IMPRESION_VVISTA,
                     :NEW.IND_LIQUIDACION,
                     :NEW.IND_PAPELETA,
                     :NEW.IND_PLANILLA,
                     :NEW.IND_SWIFT,
                     :NEW.IND_VCTO,
                     :NEW.TIPO_CALCULO,
                     -- 5
                     :NEW.VAMOS_VIENEN_REEM,
                     x_FIELD21,
                     :NEW.ID_CLIENTE_SUC,
                     :NEW.NUM_SUCURSAL,
                     :NEW.AREA_CONTABLE,
                     :NEW.FPAGO_COMPENSACION,
                     :NEW.TIPO_CARTERA,
                     :NEW.DESCRIBE_ATRIBUTO,
                     :NEW.TIPO_DOLAR_CALCULO,
                     :NEW.CONCEPTO_COMERCIO,
                     :NEW.PAGADOR_DEBITAR,
                     :NEW.DESC_AREA_CONTABLE,
                     :NEW.INSTRUMENTO,
                     :NEW.FORMA_TASA_PAG,
                     :NEW.FORMA_TASA_REC,
                     :NEW.MONEDA_PAGO_PAG,
                     :NEW.MONEDA_PAGO_REC,
                     :NEW.TIPO_DOLAR,
                     :NEW.TIPO_INTERES_PAG,
                     :NEW.TIPO_INTERES_REC,
                     :NEW.TIPO_TASA_PAG,
                     :NEW.TIPO_TASA_REC,
                     :NEW.COD_COMERCIO,
                     x_precio_dolar_tasa,
                     :NEW.NUMERO_PROPUESTA,
                     :NEW.GESTOR,
                     :NEW.MARK_UP,
                     :NEW.divisa_mark_up,
                     DECODE(x_ORIGIN,'RED','RED-TF',x_ORIGIN),
                     :NEW.PRECIO_TRANSF_SPOT,
                     :NEW.PRECIO_TRANSF_FWD,
                     :NEW.PUNTOS_FWD,
                     v_puntos_fwd_limpio
                     --:NEW.TASA_TRANF_FUTURO - x_PRECIO_SPOT
                     );

         -- mensaje de prueba
         msj := 'Fin del insert del Update';
         ops$mesacam.m_d (msj, idtrigger, smj);

         ----------------------------------------------------------------------------------------------------------------------------------------------
         ----------------------------------------------------------------------------------------------------------------------------------------------
         ------------ Nuevo insert de SPOT INTERNA (Updating) ----------------------------------------------------------------------------------------------------
         IF :new.tipo_operacion IN ('CF', 'VF') AND x_empresa = banco
         THEN
            BEGIN
               -- pide numero de folio tibco para los registros.
               SELECT   sec_folio_tibco.NEXTVAL INTO foliotb FROM DUAL;

               --
               msj := 'Antes del insert updating  foilo_tibco ==>' || foliotb;
               ops$mesacam.m_d (msj, idtrigger, smj);

               -- foliotb

               --- para tipos  S- W- B- D- -----

               IF SUBSTR (x_ID_OPERACION_TIBCO, 2, 1) = '-'
               THEN
                  x_ID_OPERACION_TIBCO :=
                     'I'
                     || SUBSTR (x_ID_OPERACION_TIBCO,
                                2,
                                LENGTH (x_id_operacion_tibco));
               ELSE
                  x_ID_OPERACION_TIBCO := 'I-' || x_ID_OPERACION_TIBCO;
               END IF;

               x_id_cliente_rut := '222222-1';
               x_id_cliente_paso := '9RCH';

               -- averigurar el real  ---
               --                  x_ciudad    :=   x_direccion;
               --                  x_direccion :=  'V_MM_SPOT_STD';
               x_ciudad := 'V_MM_SPOT_STD';

               IF x_ind_mult_divi = 'D'
               THEN
                  x_ind_mult_divi := 'M';
               ELSE
                  x_ind_mult_divi := 'D';
               END IF;


               IF :new.tipo_operacion = 'CF'
               THEN
                  x_tipo_operacion := 'VD';
                  x_banco_debitar := 'BANKERS';
               ELSE
                  x_tipo_operacion := 'CD';
                  x_banco_acreditar := 'BANKERS';
               END IF;

               x_id_operacion := -:NEW.id_operacion;
               x_DOCUMENTO_PAGO_MOP := NULL;
               x_DOCUMENTO_PAGO_MTX := NULL;

               --x_precio_mx_tasa     := 1;

               x_PRECIO_DOLAR_TASA := x_precio_spot;
               x_PRECIO_MX_TASA := x_precio_spot;
               x_tipo_cambio_dolar := x_precio_spot;
               x_paridad_operacion := x_precio_spot;
               x_paridad_transaccion := x_precio_spot; -- Arreglo de precio transferencia.

--               IF x_ORIGIN = 'RED'
               -- JPR 26062019   INI
               IF (x_ORIGIN = 'RED' OR x_ORIGIN = 'RED-CAJA')  
               -- JPR 26062019   FIN
               THEN
                  x_area_contable := 'S';
               ELSE
                  x_area_contable := NULL;
               END IF;

               BEGIN
                  x_fecha_aux := :new.fecha_operacion + 1;

                  WHILE NOT ops$mesacam.pr_fecha_habil (x_fecha_aux, 'CHILE')
                  LOOP
                     x_fecha_aux := x_fecha_aux + 1;
                     msj := 'fecha (' || TO_CHAR (x_fecha_aux) || ')';
                     ops$mesacam.m_d (msj, idtrigger, smj);
                  END LOOP;
               END;

               x_valuta_mop := x_fecha_aux;
               x_valuta_mtx := x_fecha_aux;             -- Arreglo de valutas.

               --
               msj := 'valuta spot Interna (' || TO_CHAR (x_valuta_mop) || ')';
               ops$mesacam.m_d (msj, idtrigger, smj);
               --

               x_utilidad_resultado :=
                  ROUND (:new.monto_transaccion * x_precio_spot, 0);
               x_monto_operacion :=
                  ROUND (:new.monto_transaccion * x_precio_spot, 0);
               x_valor_inicial :=
                  ROUND (:new.monto_transaccion * x_precio_spot, 0);

               --
               msj :=
                     'Monto Operacion v/s Nuevo Monto :('
                  || TO_CHAR (:new.monto_operacion)
                  || ' ; '
                  || TO_CHAR (x_monto_operacion)
                  || ')';
               ops$mesacam.m_d (msj, idtrigger, smj); 

               --

               INSERT INTO ops$mesacam.OPERACIONES_TIBCO (
                                                             id_operacion_tibco,
                                                             tipo_operacion,
                                                             fecha_operacion,
                                                             monto_operacion,
                                                             id_producto,
                                                             id_cliente,
                                                             moneda_operacion,
                                                             id_operacion,
                                                             estado_tibco,
                                                             folio_tibco,
                                                             estado_responder,
                                                             ind_contrato,
                                                             estado_operacion,
                                                             tipo_cambio_dolar,
                                                             cod_sucursal,
                                                             precio_mx_tasa_esp,
                                                             precio_financiero,
                                                             id_oper_origen,
                                                             codigo_area,
                                                             ID_TRADER,
                                                             cod_destino,
                                                             observacion,
                                                             documento_pago_mop,
                                                             monto_transaccion,
                                                             documento_pago_mtx,
                                                             fecha_crea,
                                                             id_usuario_crea,
                                                             ind_excede_entrega,
                                                             ind_exede_margen,
                                                             moneda_transaccion,
                                                             valuta_mop,
                                                             valuta_mtx,
                                                             vamos_vienen_pago,
                                                             empresa_origen,
                                                             empresa,
                                                             sub_empresa,
                                                             -- 2
                                                             banco_acreditar,
                                                             banco_debitar,
                                                             codigo_area_ing,
                                                             paridad_operacion,
                                                             monto_dolar,
                                                             monto_pesos,
                                                             paridad_transaccion,
                                                             fecha_act,
                                                             id_usuario_act,
                                                             razon_social,
                                                             ciudad,
                                                             comuna,
                                                             direccion,
                                                             telefono,
                                                             id_cliente_rut,
                                                             DELTA_COMP,
                                                             MONTO_AJUSTE,
                                                             MONTO_CAPITAL_PAG,
                                                             MONTO_CAPITAL_REC,
                                                             MONTO_DEVENGO,
                                                             MONTO_FINAL_PESOS,
                                                             PARIDAD_CLIENTE,
                                                             PARIDAD_NEGOCIO,
                                                             PARIDAD_COMPENSACION,
                                                             PARIDAD_REMANENTE,
                                                             PERDIDA_DEVENGO,
                                                             PERDIDA_RESULTADO,
                                                             PERIODO_COMP,
                                                             PRECIO_CONTABLE,
                                                             PRECIO_MN_TASA,
                                                             -- 3
                                                             PRECIO_MX_TASA,
                                                             PRECIO_OPERACION,
                                                             PRECIO_SPOT,
                                                             PRECIO_TRAN,
                                                             REAJUSTE_ACTIVO,
                                                             PRECIO_VENCIMIENTO,
                                                             REAJUSTE_PASIVO,
                                                             RESULTADO_BASILEA,
                                                             RIESGO_EQUIVALENTE,
                                                             TASA_COMPENSACION,
                                                             TASA_FUTURO,
                                                             TASA_MON_EXT,
                                                             TASA_MON_NAC,
                                                             TASA_TRANF_FUTURO,
                                                             TIPO_CAMBIO_COMP,
                                                             UTILIDAD_DEVENGO,
                                                             VALOR_FINAL,
                                                             UTILIDAD_RESULTADO,
                                                             VALOR_INICIAL,
                                                             BASE_CALCULO,
                                                             BASE_CALCULO_VARIABLE,
                                                             ID_CRITERIO,
                                                             ID_GLOSA,
                                                             N_OPER_COMEX,
                                                             F_ABONO,
                                                             F_DEBITO,
                                                             F_MT300,
                                                             FECHA_COMPENSACION,
                                                             FECHA_DEVENGO,
                                                             FECHA_ING_CUSTODIA,
                                                             -- 4
                                                             FECHA_INICIO,
                                                             FECHA_VENCIMIENTO,
                                                             FECHA_SAL_CUSTODIA,
                                                             VALUTA_COMP,
                                                             BASE_TASFUT,
                                                             C_ABONO,
                                                             C_DEBITO,
                                                             C_MT300,
                                                             COMISION,
                                                             CONFIRMA,
                                                             IND_ABNOPB,
                                                             IND_ACT_LIN_FUT,
                                                             IND_CURSE,
                                                             IND_DOCVAL,
                                                             IND_MULT_DIVI,
                                                             IND_EXCEDE_CLIENTE,
                                                             IND_EXCEDE_GRUPO,
                                                             IND_EXCEDE_ACTUALIZA,
                                                             IND_EXCEDE_PRODUC,
                                                             IND_EXEDE_LINEA,
                                                             IND_EXCEDE_SUBPROD,
                                                             IND_FAX,
                                                             IND_FICHA,
                                                             IND_IMPRESION_VVISTA,
                                                             IND_LIQUIDACION,
                                                             IND_PAPELETA,
                                                             IND_PLANILLA,
                                                             IND_SWIFT,
                                                             IND_VCTO,
                                                             TIPO_CALCULO,
                                                             -- 5
                                                             VAMOS_VIENEN_REEM,
                                                             CODIGO_CHEQUE,
                                                             ID_CLIENTE_SUC,
                                                             NUM_SUCURSAL,
                                                             AREA_CONTABLE,
                                                             FPAGO_COMPENSACION,
                                                             TIPO_CARTERA,
                                                             DESCRIBE_ATRIBUTO,
                                                             TIPO_DOLAR_CALCULO,
                                                             CONCEPTO_COMERCIO,
                                                             PAGADOR_DEBITAR,
                                                             DESC_AREA_CONTABLE,
                                                             INSTRUMENTO,
                                                             FORMA_TASA_PAG,
                                                             FORMA_TASA_REC,
                                                             MONEDA_PAGO_PAG,
                                                             MONEDA_PAGO_REC,
                                                             TIPO_DOLAR,
                                                             TIPO_INTERES_PAG,
                                                             TIPO_INTERES_REC,
                                                             TIPO_TASA_PAG,
                                                             TIPO_TASA_REC,
                                                             COD_COMERCIO,
                                                             PRECIO_DOLAR_TASA,
                                                             pagador_acreditar,
                                                             GESTOR,
                                                             MARK_UP,
                                                             divisa_mark_up,
                                                             ORIGIN
                          )
                 VALUES   (x_ID_OPERACION_TIBCO,
                           x_tipo_operacion,
                           :NEW.fecha_operacion,
                           X_monto_operacion,
                           'EMP_NAC',
                           x_id_cliente_paso,
                           :NEW.moneda_operacion,
                           x_id_operacion,
                           x_estado_tibco,
                           foliotb,
                           x_estado_respo,
                           :NEW.ind_contrato,
                           :NEW.estado_operacion,
                           x_tipo_cambio_dolar,
                           :NEW.cod_sucursal,
                           NULL,
                           0,
                           '-' || :NEW.id_oper_origen,
                           :NEW.codigo_area,
                           x_id_trader,
                           :NEW.cod_destino,
                           SUBSTR (:NEW.observacion, 1, 70),
                           x_documento_pago_mop,
                           :NEW.monto_transaccion,
                           x_documento_pago_mtx,
                           :NEW.fecha_crea,
                           :NEW.id_usuario_crea,
                           :NEW.ind_excede_entrega,
                           :NEW.ind_exede_margen,
                           :NEW.moneda_transaccion,
                           x_valuta_mop,
                           x_valuta_mtx,
                           :NEW.vamos_vienen_pago,
                           x_empresa_origen,
                           :NEW.empresa,
                           :NEW.sub_empresa,
                           -- 2
                           x_banco_acreditar,
                           x_banco_debitar,
                           :NEW.codigo_area_ing,
                           x_paridad_operacion,
                           :NEW.monto_dolar,
                           X_monto_pesos,
                           x_paridad_transaccion,
                           SYSDATE,
                           :NEW.id_usuario_crea,
                           x_razon_social,
                           x_ciudad,
                           x_comuna,
                           x_direccion,
                           x_telefono,
                           x_id_cliente_rut,
                           :NEW.DELTA_COMP,
                           :NEW.MONTO_AJUSTE,
                           :NEW.MONTO_CAPITAL_PAG,
                           :NEW.MONTO_CAPITAL_REC,
                           :NEW.MONTO_DEVENGO,
                           NULL,
                           :NEW.PARIDAD_CLIENTE,
                           :NEW.PARIDAD_NEGOCIO,
                           :NEW.PARIDAD_COMPENSACION,
                           :NEW.PARIDAD_REMANENTE,
                           :NEW.PERDIDA_DEVENGO,
                           :NEW.PERDIDA_RESULTADO,
                           :NEW.PERIODO_COMP,
                           :NEW.PRECIO_CONTABLE,
                           0,
                           -- 3
                           X_PRECIO_MX_TASA,
                           :NEW.PRECIO_OPERACION,
                           x_PRECIO_SPOT,
                           :NEW.PRECIO_TRAN,
                           :NEW.REAJUSTE_ACTIVO,
                           :NEW.PRECIO_VENCIMIENTO,
                           :NEW.REAJUSTE_PASIVO,
                           :NEW.RESULTADO_BASILEA,
                           :NEW.RIESGO_EQUIVALENTE,
                           :NEW.TASA_COMPENSACION,
                           :NEW.TASA_FUTURO,
                           NULL,
                           NULL,
                           0,
                           :NEW.TIPO_CAMBIO_COMP,
                           :NEW.UTILIDAD_DEVENGO,
                           :NEW.VALOR_FINAL,
                           X_UTILIDAD_RESULTADO,
                           x_VALOR_INICIAL,
                           NULL,
                           :NEW.BASE_CALCULO_VARIABLE,
                           :NEW.ID_CRITERIO,
                           NULL,
                           :NEW.N_OPER_COMEX,
                           :NEW.F_ABONO,
                           :NEW.F_DEBITO,
                           NULL,
                           :NEW.fecha_operacion,
                           :NEW.FECHA_DEVENGO,
                           :NEW.FECHA_ING_CUSTODIA,
                           -- 4
                           :NEW.fecha_operacion,
                           :NEW.fecha_operacion,
                           :NEW.FECHA_SAL_CUSTODIA,
                           :NEW.VALUTA_COMP,
                           :NEW.BASE_TASFUT,
                           :NEW.C_ABONO,
                           :NEW.C_DEBITO,
                           :NEW.C_MT300,
                           :NEW.COMISION,
                           :NEW.CONFIRMA,
                           :NEW.IND_ABNOPB,
                           NULL,
                           '*',
                           :NEW.IND_DOCVAL,
                           x_ind_mult_divi,
                           :NEW.IND_EXCEDE_CLIENTE,
                           :NEW.IND_EXCEDE_GRUPO,
                           :NEW.IND_EXCEDE_ACTUALIZA,
                           :NEW.IND_EXCEDE_PRODUC,
                           :NEW.IND_EXEDE_LINEA,
                           :NEW.IND_EXCEDE_SUBPROD,
                           :NEW.IND_FAX,
                           :NEW.IND_FICHA,
                           :NEW.IND_IMPRESION_VVISTA,
                           :NEW.IND_LIQUIDACION,
                           :NEW.IND_PAPELETA,
                           :NEW.IND_PLANILLA,
                           :NEW.IND_SWIFT,
                           :NEW.IND_VCTO,
                           :NEW.TIPO_CALCULO,
                           -- 5
                           :NEW.VAMOS_VIENEN_REEM,
                           x_FIELD21,
                           :NEW.ID_CLIENTE_SUC,
                           :NEW.NUM_SUCURSAL,
                           x_AREA_CONTABLE,
                           :NEW.FPAGO_COMPENSACION,
                           NULL,
                           :NEW.DESCRIBE_ATRIBUTO,
                           NULL,
                           NULL,
                           :NEW.PAGADOR_DEBITAR,
                           NULL,
                           :NEW.INSTRUMENTO,
                           :NEW.FORMA_TASA_PAG,
                           :NEW.FORMA_TASA_REC,
                           :NEW.MONEDA_PAGO_PAG,
                           :NEW.MONEDA_PAGO_REC,
                           :NEW.TIPO_DOLAR,
                           :NEW.TIPO_INTERES_PAG,
                           :NEW.TIPO_INTERES_REC,
                           :NEW.TIPO_TASA_PAG,
                           :NEW.TIPO_TASA_REC,
                           :NEW.COD_COMERCIO,
                           x_precio_dolar_tasa,
                           :NEW.NUMERO_PROPUESTA,
                           :NEW.GESTOR,
                           0,
                           :NEW.divisa_mark_up,              -- Markup en cero
                           DECODE(x_ORIGIN,'RED','RED-TF',x_ORIGIN) -- JPR 26062019
                           );

               -- mensaje de prueba
               msj := 'Fin del insert del espejo en el Insert (Updating) ';
               ops$mesacam.m_d (msj, idtrigger, smj);
            EXCEPTION
               WHEN OTHERS
               THEN
                  --
                  msj := ' Op (' || :NEW.id_operacion || ') ' || SQLERRM;
                  ops$mesacam.m_d (msj, idtrigger, smj);
                  msj :=
                     '**** Error al crear operacion espejo del forward (Updating). ****';
                  ops$mesacam.m_p (:NEW.empresa,
                                   :NEW.id_operacion,
                                   msj,
                                   idtrigger,
                                   smj);
                  RAISE salida;
            END;
         END IF;
      ----------------------------------------------------------------------------------------------------------------------------------------------
      ----------------------------------------------------------------------------------------------------------------------------------------------
      ------------  Fin  insert UPDATING  de SPOT INTERNA ----------------------------------------------------------------------------------------------------
      --
      END IF;
   END IF;

   --
   IF DELETING
   THEN
      -- Aqui va la logica que decide si la operacion va a ser enviada o no.
      NULL;                 -- Por el momento, en mesacam no se borran las op.
   END IF;

   msj := 'Salimos del trigger';
   ops$mesacam.m_d (msj, idtrigger, smj);

EXCEPTION
   WHEN salida
   THEN
      --
      NULL;
   --    msj := 'salida forzada (no necesariamente por error. Op ('||:NEW.id_operacion||')';
   --    ops$mesacam.m_d(msj,idtrigger,smj);
   --
   WHEN OTHERS
   THEN
      --
      msj := ' Op (' || :NEW.id_operacion || ') ' || SQLERRM;
      ops$mesacam.m_d (msj, idtrigger, smj);
      msj := '**** Error de sistema. Falla del Trigger. ****';
      ops$mesacam.m_p (:NEW.empresa,
                       :NEW.id_operacion,
                       msj,
                       idtrigger,
                       smj);

--
END;
/


