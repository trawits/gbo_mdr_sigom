DROP PACKAGE BODY CHI_CON.PKG_ACCT_DOLAR_MERCADO;

CREATE OR REPLACE PACKAGE BODY CHI_CON."PKG_ACCT_DOLAR_MERCADO" IS

PROCEDURE p_GetAcctInterestValues_Loc (     num_Event               IN  NUMBER,
                                        num_Header                              IN  NUMBER,
                                        dte_Reference                   IN  DATE,
                                        num_Branch                              IN  NUMBER,
                                        num_Instrument                  IN  NUMBER,
                                        num_Currency                    IN  NUMBER,
                                        num_Folder                              IN  NUMBER DEFAULT NULL,
                                        num_LocalCurr                   IN  NUMBER,
                                        num_Properties                  IN  NUMBER,
                                        num_Direction                   IN  NUMBER,
                                        num_TAccrual                    IN  NUMBER,
                                        num_CAccrual                    IN  NUMBER,
                                        num_TTransitPayment             IN  NUMBER,
                                        num_CTransitPayment             IN  NUMBER,
                                        num_AccrBenCCY                  OUT NUMBER,
                                        num_AccrPerCCY                  OUT NUMBER,
                                        num_AccrBenLoc                  OUT NUMBER,
                                        num_AccrPerLoc                  OUT NUMBER,
                                        num_AccrRevBen                  OUT NUMBER,
                                        num_AccrRevPer                  OUT NUMBER,
                                        num_AccrRevBenInt               OUT NUMBER,
                                        num_AccrRevPerInt               OUT NUMBER,
                                        num_AccrRevBenCash              OUT NUMBER,
                                        num_AccrRevPerCash              OUT NUMBER,
                                        num_AccrPendCobroCCY    OUT NUMBER,
                                        num_AccrPendCobroLoc    OUT NUMBER,
                                        num_AccrPendPagoCCY             OUT NUMBER,
                                        num_AccrPendPagoLoc             OUT NUMBER)
IS
/* **************************************************************************** */
/* Declaración de variables...                                                                                                  */
/* **************************************************************************** */
num_ErrCode             NUMBER;
str_ErrText             VARCHAR2(200);
str_GeneErr             VARCHAR2(100);
str_TextErr             VARCHAR2(350);
num_PKCalendar  NUMBER;
num_DateConv    NUMBER;
dte_MISAcct             DATE;
/* **************************************************************************** */
/* Variables para saldos contables                                              */
/* **************************************************************************** */
num_Key                         NUMBER;
num_AccrCCYCr           NUMBER;
num_AccrCCYDb           NUMBER;
num_AccrLocCr           NUMBER;
num_AccrLocDb           NUMBER;
num_AccrBalCCY          NUMBER;
num_AccrBalLoc          NUMBER;
num_KeyCash                     NUMBER;
num_AccrCashCCYCr       NUMBER;
num_AccrCashCCYDb       NUMBER;
num_AccrCashLocCr       NUMBER;
num_AccrCashLocDb       NUMBER;
num_AccrCashBalCCY      NUMBER;
num_AccrCashBalLoc      NUMBER;
num_Quantity            NUMBER;
num_Reg                         NUMBER;
num_AccrMISLoc          NUMBER;
num_AccrMISCCY          NUMBER;
num_MISAccrPayCCY       NUMBER;
num_MISAccrPayLoc       NUMBER;
num_MISAccrCashCCY      NUMBER;
num_MISAccrCashLoc      NUMBER;
num_MISAccrAdvCCY       NUMBER;
num_MISAccrAdvLoc       NUMBER;
num_MISAccrCashExDif    NUMBER;
num_AccrDifLoc          NUMBER;
num_AccrDifCCY          NUMBER;
num_AccrTempDifLoc      NUMBER;
num_AccrTempDifCCY      NUMBER;
num_TempDifLoc          NUMBER;
num_TempDifCCY          NUMBER;
num_AccrDifFX           NUMBER;
num_AccrDifFXInt        NUMBER;
num_AccrDifFXCash       NUMBER;
num_Error                       NUMBER;

p_mercado			NUMBER := -1;
p_Fecha 				VARCHAR2(10);
--p_source Number  := 2.24; --Desarrollo
p_source NUMBER  := 122.4; --Produccion y Test

p_verificar		   NUMBER;
TAsset			   NUMBER;
CAsset			   NUMBER;
TLiab			   NUMBER;
CLiab			   NUMBER;
num_BalanceCCY	   NUMBER;
num_BalanceLoc	   NUMBER;
num_AssetCCYCr     NUMBER;
num_AssetCCYDb	   NUMBER;
num_AssetLocCr	   NUMBER;
num_AssetLocDb	   NUMBER;
num_LiabCCYCr      NUMBER;
num_LiabCCYDb	   NUMBER;
num_LiabLocCr	   NUMBER;
num_LiabLocDb	   NUMBER;

p_CF			   NUMBER;
p_ValorCCY		   NUMBER;
p_ValorLoc		   NUMBER;
p_Moneda		   NUMBER;
p_ValorAntCCY	   NUMBER;
p_pkcash		   NUMBER;

BEGIN
        /************************************************/
        /* Retorna quando a Execucao nao Esta Permitida */
        /************************************************/
        IF PGT_PRG.Pkg_Acctgeneral.EXECUTE_PROCESS = 0 THEN
                RETURN;
        END IF;
		IF PGT_PRG.Pkg_Acctgeneral.dte_SessionMISAcct IS NULL THEN
        /* Obtenemos la fecha en la que debemos ir a buscar los datos en el MIS*/
 		   BEGIN
   		     SELECT FK_CALENDAR
                INTO num_PKCalendar
     		   FROM PGT_STC.T_PGT_BRANCH_S
		        WHERE PK=num_Branch;
  	       EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        PGT_PRG.Pkg_Pgterror.p_RaiseError( 1.4, 'Calendar of Branch: '|| num_Branch);
           END;
           num_DateConv    := 189.4; -- Modify Following
  		   PGT_PRG.Pkg_Acctgeneral.p_GetAccrualAcctDate (dte_Reference,
                                        num_PKCalendar,
                                        num_DateConv,
                                        dte_MISAcct);
		ELSE
        	dte_MISAcct :=     PGT_PRG.Pkg_Acctgeneral.dte_SessionMISAcct;
		END IF;

		IF num_Branch = 20010.4 and num_Instrument = 20.4 and num_Currency <> 172.4 THEN
--		IF num_Branch = 20010.4 AND num_Instrument IN (20092.4) AND num_Currency <> 172.4 THEN
           SELECT TO_CHAR(dte_MISAcct,'dd/mm/yyyy')
           INTO   p_Fecha
           FROM dual;

		   p_mercado := chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),p_source,p_Fecha,1);

		END IF;
        /* **************************************************************************** */
        /* Busca los valores calculados en el MIS con la fecha del proceso ...                  */
        /* Calculamos                                                                                                                                   */
        /*              A       = num_MISAccrCashCCY                                                                                            */
        /*              AL      = num_MISAccrCashLoc                                                                                            */
        /*              AL'     = num_MISAccrCashExDif                                                                                          */
        /*              B       = num_MISAccrPayCCY                                                                                                     */
        /*              BL      = num_MISAccrPayLoc                                                                                                     */
        /*              E       = num_AccrMISCCY                                                                                                        */
        /*              EL      = num_AccrMISLoc                                                                                                        */
        /*              I       = num_MISAccrAdvCCY                                                                                                     */
        /*              IL      = num_MISAccrAdvLoc                                                                                                     */
        /* **************************************************************************** */
     IF num_Instrument = 20092.4 THEN -- IRS / CMS
                 PGT_PRG.Pkg_Miscdmgeneral.p_GetSWAPLastInterest(       dte_MISAcct             ,
                                                        num_Header                      ,
                                                        num_Direction           ,
                                                        num_AccrMISCCY          ,
                                                        num_AccrMISLoc          ,
                                                        num_MISAccrPayCCY       ,
                                                        num_MISAccrPayLoc       ,
                                                        num_MISAccrCashCCY      ,
                                                        num_MISAccrCashLoc      ,
                            num_MISAccrAdvCCY   ,
                            num_MISAccrAdvLoc   ,
                                                        num_MISAccrCashExDif,
                                                        num_Error       );
     ELSIF num_Instrument = 2.4 THEN -- Money Market
         PGT_PRG.Pkg_Miscdmgeneral.p_GetMMInterest(     dte_MISAcct             ,
                                                num_Header              ,
                                                num_AccrMISCCY          ,
                                                num_AccrMISLoc          ,
                                                num_MISAccrPayCCY       ,
                                                num_MISAccrPayLoc       ,
                                                num_MISAccrCashCCY      ,
                                                num_MISAccrCashLoc      ,
                                                num_MISAccrAdvCCY       ,
                        num_MISAccrAdvLoc       ,
                                                num_MISAccrCashExDif    );
     ELSE  -- CCS
                PGT_PRG.Pkg_Miscdmgeneral.p_GetCCSLastInterest( dte_MISAcct             ,
                                                        num_Header                      ,
                                                        num_Direction           ,
                                                        num_AccrMISCCY          ,
                                                        num_AccrMISLoc          ,
                                                        num_MISAccrPayCCY       ,
                                                        num_MISAccrPayLoc       ,
                                                        num_MISAccrCashCCY      ,
                                                        num_MISAccrCashLoc      ,
                            num_MISAccrAdvCCY   ,
                            num_MISAccrAdvLoc   ,
                                                        num_MISAccrCashExDif,
                                                        num_Error       );

			 p_CF := 0;
			 p_ValorCCY := 0;
			 p_ValorLoc := 0;
			 p_Moneda := 0;

	         BEGIN
			 SELECT COUNT(*)
			 INTO p_CF
			 FROM CHI_CON.V_CURRENCY_CHANGE A
			 WHERE A.PKHEADER = num_Header
			 AND   A.ccashflowdate <= dte_Reference
			 AND   A.CCASHFLOWDATE > pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,dte_Reference,-1)
			-- and   A.ccashflowdate = A.pprevsettdate
			 AND   A.EVENTDATECF = A.PPREVSETTDATE ;
			 END;

			 IF p_CF > 0 THEN
			    p_ValorAntCCY := 0;

			    BEGIN
			 	SELECT A.INTERESTCASH
				INTO p_ValorAntCCY
				FROM DEVENG.T_PGT_SWFINANCST_S A
				WHERE A.DDATE        = pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,dte_Reference,-1)
				AND   A.DEAL_ID      = num_Header
				AND   A.FK_DIRECTION = num_Direction;
				EXCEPTION WHEN OTHERS THEN
				   p_ValorAntCCY := 0;
				END;
				
			    BEGIN
			    SELECT NVL(MIN(PK),0)
				INTO p_pkcash
				FROM PGT_TRD.T_PGT_CASH_FLOWS_S    A
				WHERE A.FK_REGISTRY = num_Header
				AND   a.FK_CASHFLOWTYPE = 5.4
				AND   a.FK_DIRECTION = num_Direction
				AND   a.PREVSETTDATE <= dte_Reference
				AND   a.PREVSETTDATE > pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,dte_Reference,-1)   
				AND   a.REVUSER IS NULL
				AND   a.FK_REVREASON IS NULL;
				EXCEPTION WHEN NO_DATA_FOUND THEN
				  p_pkcash   := 0;
				  p_ValorCCY := 0;
				  p_ValorLoc := 0;
				  p_Moneda   := 0;				  
				 WHEN OTHERS THEN
				  p_pkcash   := 0;
				  p_ValorCCY := 0;
				  p_ValorLoc := 0;
				  p_Moneda   := 0;
				END;	
				
				IF p_pkcash <> 0 THEN
			       BEGIN
			       SELECT NVL(SUM(A.AMOUNT),0), NVL(MAX(A.FK_CURRENCY),0)
				   INTO p_ValorCCY, p_Moneda
				   FROM PGT_TRD.T_PGT_CASH_FLOWS_S    A
				   WHERE A.PK = p_pkcash;
				   END;				
				END IF;				

			    /*BEGIN
			    SELECT NVL(SUM(A.AMOUNT),0), NVL(MAX(A.FK_CURRENCY),0)
				INTO p_ValorCCY, p_Moneda
				FROM PGT_TRD.T_PGT_CASH_FLOWS_S    A
				WHERE A.FK_REGISTRY = num_Header
				AND   a.FK_CASHFLOWTYPE = 5.4
				AND   a.FK_DIRECTION = num_Direction
				AND   a.PREVSETTDATE <= dte_Reference
				AND   a.PREVSETTDATE > pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,dte_Reference,-1)
				--and   a.CASHFLOWDATE = a.PREVSETTDATE
				AND   a.REVUSER IS NULL
				AND   a.FK_REVREASON IS NULL;
				EXCEPTION WHEN NO_DATA_FOUND THEN
				  p_ValorCCY := 0;
				  p_ValorLoc := 0;
				  p_Moneda   := 0;
				 WHEN OTHERS THEN
				  p_ValorCCY := 0;
				  p_ValorLoc := 0;
				  p_Moneda   := 0;
				END;*/

				p_ValorCCY := p_ValorAntCCY + p_ValorCCY;

     		   IF p_Moneda =  num_LocalCurr OR p_Moneda = 0 THEN
                  p_ValorCCY :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount( num_LocalCurr    ,
                                                                        dte_Reference   ,
                                                                        p_ValorCCY  );
			      num_MISAccrCashCCY := p_ValorCCY;
				  num_MISAccrCashLoc := p_ValorCCY;
				  IF p_ValorCCY = 0 AND p_ValorLoc = 0 AND p_Moneda = 0 THEN
			      	 num_MISAccrCashCCY := num_MISAccrCashCCY;
				  	 num_MISAccrCashLoc := num_MISAccrCashLoc;
				  END IF;
			   ELSE
            	  p_ValorLoc := "PGT_PRG".Pkg_Acctgeneral.f_CtrValue(num_Branch,
        			      			       dte_Reference,
        			      			       num_Currency,
        		              			   p_ValorCCY,
        		              			   num_LocalCurr);

                  p_ValorCCY :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount( num_Currency    ,
                                                                        dte_Reference   ,
                                                                        p_ValorCCY  );

                  p_ValorLoc :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount( num_LocalCurr   ,
                                                                        dte_Reference   ,
                                                                        p_ValorLoc  );

			      num_MISAccrCashCCY := p_ValorCCY;
				  num_MISAccrCashLoc := p_ValorLoc;
			   END IF;
			 END IF;
     END IF;
        /*********************************/
        /* Atualiza o numero de Decimais */
        /*********************************/
        num_AccrMISCCY :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(        num_Currency    ,
                                                                        dte_Reference   ,
                                                                        num_AccrMISCCY  );
        num_AccrMISLoc :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(        num_LocalCurr   ,
                                                                        dte_Reference   ,
                                                                        num_AccrMISLoc  );
        num_MISAccrPayCCY :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(     num_Currency    ,
                                                                        dte_Reference   ,
                                                                        num_MISAccrPayCCY       );
        num_MISAccrPayLoc :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(     num_LocalCurr           ,
                                                                        dte_Reference           ,
                                                                        num_MISAccrPayLoc       );
        num_MISAccrCashCCY :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(    num_Currency            ,
                                                                        dte_Reference           ,
                                                                        num_MISAccrCashCCY      );
        num_MISAccrCashLoc :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(    num_LocalCurr           ,
                                                                        dte_Reference           ,
                                                                        num_MISAccrCashLoc      );
        num_MISAccrCashExDif :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(  num_LocalCurr           ,
                                                                                dte_Reference           ,
                                                                                num_MISAccrCashExDif    );
        /* **************************************************************************** */
        /* Si los dos saldos estan a cero significa que es el primer día y todavía no   */
        /* tengo periódica ...                                                                                                                  */
        /* **************************************************************************** */
/*        IF ((NVL(num_AccrMISCCY,0) = 0)         AND
            (NVL(num_AccrMISLoc,0) = 0)         AND
            (NVL(num_MISAccrPayCCY,0) = 0)      AND
            (NVL(num_MISAccrPayLoc,0) = 0)      AND
            (NVL(num_MISAccrCashCCY,0)=0)       AND
            (NVL(num_MISAccrCashLoc,0)=0)       AND
            (NVL(num_MISAccrCashExDif,0)=0)) THEN
                num_AccrPerCCY          := 0;
                num_AccrBenLoc          := 0;
                num_AccrPerLoc          := 0;
                num_AccrRevBen          := 0;
                num_AccrRevPer          := 0;
                num_AccrBenCCY          := 0;
                num_AccrPendCobroCCY    := 0;
                num_AccrPendCobroLoc    := 0;
                num_AccrPendPagoCCY     := 0;
                num_AccrPendPagoLoc     := 0;
                RETURN;
        END IF; */
        /* **************************************************************************** */
        /* Buscamos la llave de la cuenta de Accrual ....                                                               */
        /* **************************************************************************** */
        num_Key := PGT_PRG.Pkg_Acct_Trg.f_GetAcctKey (  num_CAccrual            ,
                                                        num_Branch              ,
                                                        num_Currency    ,
                                                        num_Instrument  ,
                                                        NULL                    ,
                                                        NULL                    ,
                                                        NULL                    ,
                                                        num_Folder              ,
                                                        NULL                    ,
                                                        num_Event               ,
                                                        num_Properties  ,
                                                        num_TAccrual    );
        /* **************************************************************************** */
        /* Buscamos la posición de la cuenta de Accrual ....                                                    */
        /* **************************************************************************** */
        PGT_PRG.Pkg_Acct_Trg.p_Get_GLTA_Bal (           dte_Reference           ,
                                                        num_Key                         ,
                                                        dte_Reference           ,
                                                        num_AccrCCYCr           ,
                                                        num_AccrCCYDb           ,
                                                        num_AccrLocCr           ,
                                                        num_AccrLocDb           ,
                                                        num_Quantity            ,
                                                        num_Reg                 );
        /* **************************************************************************** */
        /* Obtenemos los saldos de nuestra cuenta de periódica ...                                              */
        /* Calculamos   D       = num_AccrBalCCY                                                                                        */
        /*              DL      = num_AccrBalLoc                                                                                                        */
        /* **************************************************************************** */
        num_AccrBalCCY          := num_AccrCCYDb - num_AccrCCYCr;
        num_AccrBalLoc          := num_AccrLocDb - num_AccrLocCr;
--    dbms_output.put_line('Per num_AccrBalCCY ' || num_AccrBalCCY);
--    dbms_output.put_line('Per num_AccrBalLoc ' || num_AccrBalLoc);
        /* **************************************************************************** */
        /* Buscamos la llave de la cuenta de Cash ....                                                                  */
        /* **************************************************************************** */
        num_KeyCash := PGT_PRG.Pkg_Acct_Trg.f_GetAcctKey (      num_CTransitPayment     ,
                                                                num_Branch              ,
                                                                num_Currency    ,
                                                                num_Instrument  ,
                                                                NULL                    ,
                                                                NULL                    ,
                                                                NULL                    ,
                                                                num_Folder              ,
                                                                NULL                    ,
                                                                num_Event               ,
                                                                num_Properties  ,
                                                                num_TTransitPayment     );
        /* **************************************************************************** */
        /* Buscamos la posición de la cuenta de Cash ....                                                               */
        /* **************************************************************************** */
        PGT_PRG.Pkg_Acct_Trg.p_Get_GLTA_Bal (           dte_Reference           ,
                                                        num_KeyCash                     ,
                                                        dte_Reference           ,
                                                        num_AccrCashCCYCr       ,
                                                        num_AccrCashCCYDb       ,
                                                        num_AccrCashLocCr       ,
                                                        num_AccrCashLocDb       ,
                                                        num_Quantity            ,
                                                        num_Reg                 );
        /* **************************************************************************** */
        /* Obtenemos los saldos de nuestras cuentas de caja ...                                                 */
        /* **************************************************************************** */
        num_AccrCashBalCCY      := num_AccrCashCCYDb - num_AccrCashCCYCr;
        num_AccrCashBalLoc      := num_AccrCashLocDb - num_AccrCashLocCr;
        /* **************************************************************************** */
        /* Obtenemos la suma de todos los valores que nos envía MIS para ser capaces de */
        /* obtener el fixing.                                                                                                                   */
        /* F    = ( A + B + E)                                                                                                                  */
        /* FL   = ( AL + AL' + BL + EL)                                                                                                 */
        /* **************************************************************************** */
        num_AccrTempDifCCY      := ( num_MISAccrCashCCY + num_MISAccrPayCCY + num_AccrMISCCY);
        num_AccrTempDifLoc      := ( num_MISAccrCashLoc + num_MISAccrCashExDif + num_MISAccrPayLoc + num_AccrMISLoc);
        /* **************************************************************************** */
        /* Ahora calculamos los valores a insertar en las cuentas de pendientes                 */
        /* de cobro pago. Estos valores son los pagos de interes periódicos que se vayan*/
        /* haciendo ...                                                                                                                                 */
        /* H    = ( A + B ) - C                                                                                                                 */
        /* HL   = H * ( FL / F )                                                                                                                */
        /* **************************************************************************** */
        num_TempDifCCY  := 0;
        num_TempDifLoc  := 0;
        num_TempDifCCY  := ( num_MISAccrCashCCY + num_MISAccrPayCCY ) - num_AccrCashBalCCY;
        --num_TempDifLoc  := num_TempDifCCY * (num_AccrTempDifLoc / num_AccrTempDifCCY);
	IF num_Currency = num_LocalCurr THEN
		num_TempDifLoc := num_TempDifCCY;
	ELSE
		num_TempDifLoc := "PGT_PRG".Pkg_Acctgeneral.f_CtrValue(num_Branch,
        			      			       dte_Reference,
        			      			       num_Currency,
        		              			       num_TempDifCCY,
        		              			       num_LocalCurr);
	END IF;
        IF num_TempDifCCY < 0 THEN
                num_AccrPendCobroCCY    := 0;
                num_AccrPendPagoCCY     := ABS (num_TempDifCCY);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrPendPagoCCY :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_Currency            ,
                                                                dte_Reference           ,
                                                                num_AccrPendPagoCCY     );
        ELSE
                num_AccrPendCobroCCY    := ABS (num_TempDifCCY);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrPendCobroCCY :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_Currency            ,
                                                                dte_Reference           ,
                                                                num_AccrPendCobroCCY    );
                num_AccrPendPagoCCY     := 0;
        END IF;
        /* **************************************************************************** */
        /* MIS revalua la cuenta de pendiente de pago pero nosotros, una vez que                */
        /* enviamos a nuestra cuenta transitoria ya no revaluamos nada, por lo que              */
        /* si la diferencia en divisa es cero, la parte local tambien la hacemos cero.  */
        /* **************************************************************************** */
        IF (( num_AccrPendCobroCCY = 0 ) AND  ( num_AccrPendPagoCCY = 0 ))THEN
                num_AccrPendCobroLoc    := 0;
                num_AccrPendPagoLoc     := 0;
        ELSE
                IF num_TempDifLoc < 0 THEN
                        num_AccrPendCobroLoc    := 0;
                        num_AccrPendPagoLoc     := ABS (num_TempDifLoc);
                        /* ************************************************************ */
                        /* Actualizamos el valor teniendo en cuenta los decimales para  */
                        /* la divisa                                                                                                    */
                        /* ************************************************************ */
                        num_AccrPendPagoLoc :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr           ,
                                                                dte_Reference           ,
                                                                num_AccrPendPagoLoc     );
                ELSE
                        num_AccrPendCobroLoc    := ABS (num_TempDifLoc);
                        /* ************************************************************ */
                        /* Actualizamos el valor teniendo en cuenta los decimales para  */
                        /* la divisa                                                                                                    */
                        /* ************************************************************ */
                        num_AccrPendCobroLoc :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr           ,
                                                                dte_Reference           ,
                                                                num_AccrPendCobroLoc    );
                        num_AccrPendPagoLoc     := 0;
                END IF;
        END IF; -- viene de abajo rss
                /* ******************************************************************** */
                /* Lo normal, en un pago de interes, sería                                      */
                /* descontar el valor a pagar y despues hacer el resto de los                   */
                /* cálculos. Al hacerlo en un único evento, el valor a pagar lo                 */
                /* tendremos que descontar, por código, del valor que hemos obtenido de */
                /* nuestra cuenta de periódica. Este valor hay que corregirlo con las   */
                /* cajas anticipadas (pagos realizados pero que estamos periodificando. */
                /* Entonces :                                                                                                                   */
                /*              DL      = num_AccrBalLoc - num_TempDifLoc - num_MISAccrAdvLoc           */
                /*              D       = num_AccrBalCCY - num_TempDifCCY - num_MISAccrAdvCCY           */
                /* ******************************************************************** */
-- ojo RSS 26/9 Quito el saldo de caja del cálculo (ojo con las dif. de cambio)
--              num_AccrBalLoc  := num_AccrBalLoc - num_TempDifLoc - num_MISAccrAdvLoc;
--              num_AccrBalCCY  := num_AccrBalCCY - num_TempDifCCY - num_MISAccrAdvCCY;
--dbms_output.put_line('num_AccrBalCCY ' || num_AccrBalCCY);
                num_AccrBalCCY  := num_AccrBalCCY
                                        - ( num_MISAccrCashCCY + num_MISAccrPayCCY + num_MISAccrAdvCCY );
--dbms_output.put_line('num_MISAccrCashCCY ' || num_MISAccrCashCCY);
--dbms_output.put_line('num_MISAccrPayCCY ' || num_MISAccrPayCCY);
--dbms_output.put_line('num_MISAccrAdvCCY ' || num_MISAccrAdvCCY);
--  dbms_output.put_line('Despues num_AccrBalCCY ' || num_AccrBalCCY);
                num_AccrBalLoc  := num_AccrBalLoc
--                                      - (num_MISAccrCashCCY + num_MISAccrPayCCY) * (num_AccrTempDifLoc / num_AccrTempDifCCY)
                                        - (num_MISAccrCashLoc + num_MISAccrPayLoc + num_MISAccrCashExDif)
                                        - num_MISAccrAdvLoc;
                num_AccrBalCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_Currency    ,
                                                                dte_Reference   ,
                                                                num_AccrBalCCY  );
                num_AccrBalLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Reference   ,
                                                                num_AccrBalLoc  );
--      END IF; lo pongo mas arriba rss
        /* **************************************************************************** */
        /* Cálculamos la periódica del depósito ...                                                                             */
        /* G    = E - D                                                                                                                                 */
        /* **************************************************************************** */
--    dbms_output.put_line('num_AccrMISCCY ' || num_AccrMISCCY);
--    dbms_output.put_line('num_AccrBalCCY ' || num_AccrBalCCY);
        num_AccrDifCCY          := num_AccrMISCCY - num_AccrBalCCY;
	-- Agregado por CN para el evento currency change -----------------------------
	   /*BEGIN
	   SELECT count(1)
	   INTO p_verificar
	   FROM CHI_CON.V_CURRENCY_CHANGE A
	   WHERE A.PKHEADER = num_Header
	   AND   A.PPREVSETTDATE = dte_Reference;
	   EXCEPTION WHEN OTHERS THEN
	     p_verificar := 0;
	   END;

	   IF p_verificar > 0 THEN
	      IF num_Direction = 183.4 THEN
	      	 TAsset	  	:= 52.4;
	      	 CAsset	    := PGT_PRG.Pkg_AcctGeneral.f_GetAcctNo(	num_Branch,
								num_Properties,
								TAsset);

             num_KeyCash := PGT_PRG.Pkg_Acct_Trg.f_GetAcctKey (      CAsset     ,
                                                                num_Branch              ,
                                                                num_Currency    ,
                                                                num_Instrument  ,
                                                                NULL                    ,
                                                                NULL                    ,
                                                                NULL                    ,
                                                                num_Folder              ,
                                                                NULL                    ,
                                                                num_Event               ,
                                                                num_Properties  ,
                                                                TAsset     );

             PGT_PRG.Pkg_Acct_Trg.p_Get_GLTA_Bal (           dte_Reference           ,
                                                        num_KeyCash                     ,
                                                        dte_Reference           ,
                                                        num_AssetCCYCr       ,
                                                        num_AssetCCYDb       ,
                                                        num_AssetLocCr       ,
                                                        num_AssetLocDb       ,
                                                        num_Quantity            ,
                                                        num_Reg                 );

		     num_BalanceCCY := num_AssetCCYDb - num_AssetCCYCr;
			 num_BalanceLoc := num_AssetLocDb - num_AssetLocCr;

		 ELSE -- 184.4
	         TLiab			:= 55.4;
	      	 CLiab			:= PGT_PRG.Pkg_AcctGeneral.f_GetAcctNo(	num_Branch,
								num_Properties,
								TLiab);

             num_KeyCash := PGT_PRG.Pkg_Acct_Trg.f_GetAcctKey (      CLiab     ,
                                                                num_Branch              ,
                                                                num_Currency    ,
                                                                num_Instrument  ,
                                                                NULL                    ,
                                                                NULL                    ,
                                                                NULL                    ,
                                                                num_Folder              ,
                                                                NULL                    ,
                                                                num_Event               ,
                                                                num_Properties  ,
                                                                TLiab     );

             PGT_PRG.Pkg_Acct_Trg.p_Get_GLTA_Bal (           dte_Reference           ,
                                                        num_KeyCash                     ,
                                                        dte_Reference           ,
                                                        num_LiabCCYCr       ,
                                                        num_LiabCCYDb       ,
                                                        num_LiabLocCr       ,
                                                        num_LiabLocDb       ,
                                                        num_Quantity            ,
                                                        num_Reg                 );

		    num_BalanceCCY := num_LiabCCYDb - num_LiabCCYCr;
			num_BalanceLoc := num_LiabLocDb - num_LiabLocCr;
		END IF;

        num_BalanceCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_Currency   ,
                                                                dte_Reference   ,
                                                                num_BalanceCCY  );

        num_BalanceLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Reference   ,
                                                                num_BalanceLoc  );


        num_AccrDifCCY          := num_AccrMISCCY - num_BalanceCCY;
		num_AccrBalLoc			:= num_BalanceLoc;

	   END IF;*/
	----------------------------------FIN-------------------------------------

        IF (num_AccrDifCCY < 0 ) THEN
                num_AccrBenCCY          := 0;
                num_AccrPerCCY          := (num_AccrDifCCY);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrPerCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_Currency    ,
                                                                dte_Reference   ,
                                                                num_AccrPerCCY  );
                num_AccrBenLoc  := 0;
                /* ******************************************************************** */
                /* La periódica en divisa local ...                                                                             */
                /* GL   = ( E - D ) * ( F / FL)                                                                                 */
                /* ******************************************************************** */
                --num_AccrPerLoc  := num_AccrDifCCY * (num_AccrTempDifLoc / num_AccrTempDifCCY);
		IF num_Currency = num_LocalCurr THEN
			num_AccrPerLoc := num_AccrDifCCY;
		ELSE
--			  num_AccrPerLoc := "PGT_PRG".Pkg_Acctgeneral.f_CtrValue(num_Branch,
--        			      			       dte_Reference,
--        			      			       num_Currency,
--        		              			       num_AccrDifCCY,
--        		              			       num_LocalCurr);

           -- Nuevo para Dolar Mecado mod. por Carla Neira
		   IF num_Branch = 20010.4 and num_Instrument = 20.4 and num_Currency <> 172.4 and p_mercado <> -1 THEN
--		   IF num_Branch = 20010.4 AND num_Instrument IN (20092.4) AND num_Currency <> 172.4 AND p_mercado <> -1 THEN
		      num_AccrPerLoc := num_AccrDifCCY * p_mercado;
		   ELSE
			  num_AccrPerLoc := "PGT_PRG".Pkg_Acctgeneral.f_CtrValue(num_Branch,
        			      			       dte_Reference,
        			      			       num_Currency,
        		              			       num_AccrDifCCY,
        		              			       num_LocalCurr);		   	  
		   END IF;

		END IF;
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Reference   ,
                                                                num_AccrPerLoc  );
                /* ******************************************************************** */
                /* Calculamos la revaluación de la periodica ...                                                */
                /*      R =      EL - ( DL + GL )                                                                                               */
                /* ******************************************************************** */
------- PARA LA CUENTA (INTERNA) DE PERIODICA ACUMULADA
                num_AccrDifFX   :=  num_AccrMISLoc - ( num_AccrBalLoc + num_AccrPerLoc) ;
------- PARA LA CUENTA (INTERNA) DE CAJAS ACUMULADAS
                num_AccrDifFXCash := (num_MISAccrCashLoc + num_MISAccrPayLoc + num_MISAccrCashExDif)
                                - (num_AccrCashBalLoc + num_TempDifLoc);
------- PARA LA CUENTA DE PRIMAS COBRADAS/PAGADAS (EN ESTA ESTAN LA PERIODICA MAS LAS CAJAS)
                num_AccrDifFXInt        :=      num_AccrDifFX - num_AccrDifFXCash;
                /* Para los calculos que siguen necesito el valor absoluto */
                num_AccrPerCCY  := ABS (num_AccrPerCCY);
                num_AccrPerLOC  := ABS (num_AccrPerLOC);
        ELSE
                num_AccrBenCCY          := (num_AccrDifCCY);
                num_AccrPerCCY          := 0;
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrBenCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_Currency    ,
                                                                dte_Reference   ,
                                                                num_AccrBenCCY  );
                num_AccrPerLoc  := 0;
                /* ******************************************************************** */
                /* La periódica en divisa local ...                                                                             */
                /* GL   = ( E - D ) * ( F / FL)                                                                                 */
                /* ******************************************************************** */
                --num_AccrBenLoc  := num_AccrDifCCY * (num_AccrTempDifLoc / num_AccrTempDifCCY);
		IF num_Currency = num_LocalCurr THEN
			num_AccrBenLoc := num_AccrDifCCY;
		ELSE
--			num_AccrBenLoc := "PGT_PRG".Pkg_Acctgeneral.f_CtrValue(num_Branch,
--        			      			       dte_Reference,
--        			      			       num_Currency,
--        		              			       num_AccrDifCCY,
--        		              			       num_LocalCurr);

           -- Nuevo para Dolar Mecado mod. por Carla Neira
		   IF num_Branch = 20010.4 and num_Instrument = 20.4 and num_Currency <> 172.4 and p_mercado <> -1 THEN
--		   IF num_Branch = 20010.4 AND num_Instrument IN (20092.4) AND num_Currency <> 172.4 AND p_mercado <> -1 THEN
		      num_AccrBenLoc := num_AccrDifCCY * p_mercado;
		   ELSE
			  num_AccrBenLoc := "PGT_PRG".Pkg_Acctgeneral.f_CtrValue(num_Branch,
        			      			       dte_Reference,
        			      			       num_Currency,
        		              			       num_AccrDifCCY,
        		              			       num_LocalCurr);		   	  
		   END IF;

		END IF;
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Reference   ,
                                                                num_AccrBenLoc  );
                /* ******************************************************************** */
                /* Calculamos la revaluación de la periodica ...                                                */
                /*      R       = EL - ( DL + GL )                                                                                              */
                /* ******************************************************************** */
-- PARA LA CUENTA (INTERNA) DE PERIODICA ACUMULADA
                num_AccrDifFX   :=  num_AccrMISLoc - ( num_AccrBalLoc + num_AccrBenLoc) ;
-- PARA LA CUENTA (INTERNA) DE CAJAS ACUMULADAS
                num_AccrDifFXCash := (num_MISAccrCashLoc + num_MISAccrPayLoc + num_MISAccrCashExDif)
                                - (num_AccrCashBalLoc + num_TempDifLoc);
-- PARA LA CUENTA DE PRIMAS COBRADAS/PAGADAS (EN ESTA ESTAN LA PERIODICA MAS LAS CAJAS)
                num_AccrDifFXInt        :=      num_AccrDifFX - num_AccrDifFXCash;
        END IF;
        /******************************************************************************/
        /*              CUENTA (INTERNA) DE PERIODICA ACUMULADA                               */
        /* Dependiendo de si el importe de la revaluación de la periódica es positivo */
        /* o negativo devolvemos perdidas o beneficios.                               */
        /******************************************************************************/
        IF num_AccrDifFX > 0 THEN
                num_AccrRevBen          := ABS(num_AccrDifFX);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrRevBen  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Reference   ,
                                                                num_AccrRevBen  );
                num_AccrRevPer  := 0;
        ELSE
                num_AccrRevBen  := 0;
                num_AccrRevPer  := ABS (num_AccrDifFX);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrRevPer :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(        num_LocalCurr   ,
                                                                                dte_Reference   ,
                                                                                num_AccrRevPer  );
        END IF;
        /******************************************************************************/
        /*                      CUENTA (INTERNA) DE CAJAS ACUMULADAS                              */
        /* Dependiendo de si el importe de la revaluación de la periódica es positivo */
        /* o negativo devolvemos perdidas o beneficios.                               */
        /******************************************************************************/
        IF num_AccrDifFXInt > 0 THEN
                num_AccrRevBenInt               := ABS(num_AccrDifFXInt);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrRevBenInt       :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Reference   ,
                                                                num_AccrRevBenInt       );
                num_AccrRevPerInt       := 0;
        ELSE
                num_AccrRevBenInt       := 0;
                num_AccrRevPerInt       := ABS (num_AccrDifFXInt);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrRevPerInt :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(     num_LocalCurr   ,
                                                                                dte_Reference   ,
                                                                                num_AccrRevPerInt       );
        END IF;
        /******************************************************************************/
        /*                      CUENTA DE PRIMAS COBRADAS/PAGADAS (PERIODICA MAS CAJAS)           */
        /* Dependiendo de si el importe de la revaluación de la periódica es positivo */
        /* o negativo devolvemos perdidas o beneficios.                               */
        /******************************************************************************/
        IF num_AccrDifFXCash > 0 THEN
                num_AccrRevBenCash              := ABS(num_AccrDifFXCash);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrRevBenCash      :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Reference   ,
                                                                num_AccrRevBenCash      );
                num_AccrRevPerCash      := 0;
        ELSE
                num_AccrRevBenCash      := 0;
                num_AccrRevPerCash      := ABS (num_AccrDifFXCash);
                /* ******************************************************************** */
                /* Actualizamos el valor teniendo en cuenta los decimales para                  */
                /* la divisa                                                                                                                    */
                /* ******************************************************************** */
                num_AccrRevPerCash :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount(    num_LocalCurr   ,
                                                                                dte_Reference   ,
                                                                                num_AccrRevPerCash      );
        END IF;
/****************************
if num_event = 55258.4 AND num_direction = 184.4 THEN
        raise_application_error(-20001,'num_AccrDifFX: '||num_AccrDifFX||';'||
                        'num_AccrMISCCY: '||num_AccrMISCCY||';'||
                        'num_AccrMISLoc: '||num_AccrMISLoc||';'||
                        'num_AccrBalCCY: '||num_AccrBalCCY||';'||
                        'num_AccrBalLoc: '||num_AccrBalLoc||';'||
                        'num_AccrPerCCY: '||num_AccrPerCCY||';'||
                        'num_AccrPerLoc: '||num_AccrPerLoc||';'||
                        'num_AccrBenCCY: '||num_AccrBenCCY||';'||
                        'num_AccrBenLoc: '||num_AccrBenLoc||';'||
                        'num_AccrDifCCY: '||num_AccrDifCCY||';'||
                        'num_AccrDifLoc: '||num_AccrDifLoc||';'||
                        'num_AccrTempDifLoc: '||num_AccrTempDifLoc||';'||
                        'num_AccrTempDifCCY: '||num_AccrTempDifCCY||';'||
                        'num_MISAccrCashCCY: '||num_MISAccrCashCCY||';'||
                        'num_MISAccrCashLoc: '||num_MISAccrCashLoc||';'||
                        'num_MISAccrCashExDif: '||num_MISAccrCashExDif||';'||
                        'num_MISAccrPayCCY: '||num_MISAccrPayCCY||';'||
                        'num_MISAccrPayLoc: '||num_MISAccrPayLoc||';'||
                        'num_MISAccrAdvCCY: '||num_MISAccrAdvCCY||';'||
                        'num_MISAccrAdvLoc: '||num_MISAccrAdvLoc||';'||
                        'num_AccrDifFXInt: '||num_AccrDifFXInt||';'||
                        'num_AccrDifFXCash: '||num_AccrDifFXCash);
end if;
****/
END p_GetAcctInterestValues_Loc;

PROCEDURE p_GetValMarketValue_Loc ( num_Event       IN  NUMBER,
                                num_Header      IN  NUMBER,
                                num_Oper        IN  NUMBER,
                                dte_Proceso     IN  DATE,
                                num_Branch      IN  NUMBER,
                                num_Instrument  IN  NUMBER,
                                num_Currency    IN  NUMBER,
                                num_Folder      IN  NUMBER,
                                num_LocalCurr   IN  NUMBER,
                                num_Direction   IN  NUMBER,
                                num_Properties  IN  NUMBER,
                                num_TIntMTM     IN  NUMBER,
                                num_CIntMTM     IN  NUMBER,
                                num_TRevOpFin   IN  NUMBER,
                                num_CRevOpFin   IN  NUMBER,
                                num_TDevOpFin   IN  NUMBER,
                                num_CDevOpFin   IN  NUMBER,
                                num_AValBenLoc  OUT NUMBER,
                                num_AValBenCCY  OUT NUMBER,
                                num_AValPerLoc  OUT NUMBER,
                                num_AValPerCCY  OUT NUMBER,
                                num_ARevBenLoc  OUT NUMBER,
                                num_ARevPerLoc  OUT NUMBER,
                                num_ADevBenLoc  OUT NUMBER,
                                num_ADevPerLoc  OUT NUMBER)
IS
        /* ******************************************************************** */
        /* Variables                                                                                                                    */
        /* ******************************************************************** */
        num_Key                 NUMBER;
        num_Reg                 NUMBER;
        num_Quantity            NUMBER;
        num_TableMIS            NUMBER;
        dte_MISAcct             DATE;
		dte_ValueDate			DATE;
        num_PKCalendar          NUMBER;
        num_DateConv            NUMBER;
    	num_Error               NUMBER;
		num_Status           	NUMBER;
        /* ******************************************************************** */
        /* Variables para saldos recuperados de MIS                                                             */
        /* ******************************************************************** */
        num_MMarkValueCCY        NUMBER;
        num_MarketValueMan       NUMBER;
        num_MPrincCashCCY        NUMBER;
        num_MPrincCashLoc        NUMBER;
        num_MCancelRecPayCCY     NUMBER;
        num_MCancelRecPayLoc     NUMBER;
        num_MFeeAccrCCY          NUMBER;
        num_MFeeAccrLoc          NUMBER;
        num_MFeeAdvPayAccrCCY    NUMBER;
        num_MFeeAdvPayAccrLoc    NUMBER;
        num_MFeeRecPayCCY        NUMBER;
        num_MFeeRecPayLoc        NUMBER;
        num_MIntAdvPayAccrCCY    NUMBER;
        num_MIntAdvPayAccrLoc    NUMBER;
        num_MInterRecPayCCY      NUMBER;
        num_MInterRecPayLoc      NUMBER;
        num_MInterAccrCCY        NUMBER;
        num_MInterAccrLoc        NUMBER;
        num_MPrincReadjCCY       NUMBER;
        num_MInterReadjCCY       NUMBER;
		num_Principal            NUMBER;
		num_PrincipalCur         NUMBER;
		num_PrincipalFwd         NUMBER;
        /* ******************************************************************** */
        /* Variables para saldos contables                                                                              */
        /* ******************************************************************** */
        num_PosIntMTMCCYCr       NUMBER  ;
        num_PosIntMTMCCYDb       NUMBER  ;
        num_PosIntMTMCCY         NUMBER  ;
        num_PosIntMTMLocCr       NUMBER  ;
        num_PosIntMTMLocDb       NUMBER  ;
        num_PosIntMTMLoc         NUMBER  ;
        num_PosRevOpFinCCYCr     NUMBER  ;
        num_PosRevOpFinCCYDb     NUMBER  ;
        num_PosRevOpFinCCY       NUMBER  ;
        num_PosRevOpFinLocCr     NUMBER  ;
        num_PosRevOpFinLocDb     NUMBER  ;
        num_PosRevOpFinLoc       NUMBER  ;
        num_PosDevOpFinCCYCr     NUMBER  ;
        num_PosDevOpFinCCYDb     NUMBER  ;
        num_PosDevOpFinCCY       NUMBER  ;
        num_PosDevOpFinLocCr     NUMBER  ;
        num_PosDevOpFinLocDb     NUMBER  ;
        num_PosDevOpFinLoc       NUMBER  ;
        num_DMTMValueCCY         NUMBER  ;
        num_DMTMValueLoc         NUMBER  ;
        num_DRevMTML             NUMBER  ;
        num_DDevMTML             NUMBER  ;
        /* ******************************************************************** */
        /* Variables auxiliares                                                                                                 */
        /* ******************************************************************** */
        num_AuxDevLoc           NUMBER  ;
        num_AuxRevLoc           NUMBER  ;

		p_mercado			NUMBER := -1;
		p_Fecha 				VARCHAR2(10);
		--p_source Number  := 2.24; --Desarrollo
		p_source NUMBER  := 122.4; --Produccion y Test
BEGIN
        /* ******************************************************************** */
        /* Terminamos si la ejecución no esta permitida ...                                             */
        /* ******************************************************************** */
        IF PGT_PRG.Pkg_Acctgeneral.EXECUTE_PROCESS = 0 THEN
                RETURN;
        END IF;
        /* ******************************************************************** */
        /* Obtenemos la fecha en la que debemos ir a buscar los datos en el MIS */
        /* ******************************************************************** */
		IF PGT_PRG.Pkg_Acctgeneral.dte_SessionMISAcct IS NULL THEN
        	BEGIN
        		SELECT  FK_CALENDAR
                	INTO    num_PKCalendar
                	FROM    PGT_STC.T_PGT_BRANCH_S
               		WHERE   PK=num_Branch;
        	EXCEPTION
                	WHEN NO_DATA_FOUND THEN
                        	PGT_PRG.Pkg_Pgterror.p_RaiseError( 1.4, 'Calendar of Branch: '|| num_Branch);
        	END;
        	num_DateConv    := PGT_PRG.Pkg_Pgtconst.cteModifyFollowing;
        	PGT_PRG.Pkg_Acctgeneral.p_GetAccrualAcctDate (  dte_Proceso,
                                                	num_PKCalendar,
                                                	num_DateConv,
                                                	dte_MISAcct);
		ELSE
        	dte_MISAcct :=     PGT_PRG.Pkg_Acctgeneral.dte_SessionMISAcct;
		END IF;
--		IF num_Branch = 20010.4 and num_Instrument in (20092.4,20.4) and num_Currency <> 172.4 THEN
		IF num_Branch = 20010.4 AND num_Instrument IN (20092.4) AND num_Currency <> 172.4 THEN
           SELECT TO_CHAR(dte_MISAcct,'dd/mm/yyyy')
           INTO   p_Fecha
           FROM dual;

		   p_mercado := chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),p_source,p_Fecha,1);
		END IF;
        /* ******************************************************************** */
        /* Llamamos a la función que obtiene los valores de MIS ...                             */
        /* Esta función nos devuelve :                                                                                  */
        /* ******************************************************************** */
	    IF num_Instrument = 20092.4 THEN -- IRS / CMS
                 PGT_PRG.Pkg_Miscdmgeneral.p_GetSWAPMarkToMarket(       dte_MISAcct     ,
                                                        num_Header              ,
                                                        num_Direction           ,
                                                		num_MCancelRecPayCCY    ,
                                                		num_MCancelRecPayLoc    ,
                                                		num_MFeeAccrCCY         ,
                                                		num_MFeeAccrLoc         ,
                                                		num_MFeeAdvPayAccrCCY   ,
                                                		num_MFeeAdvPayAccrLoc   ,
                                                		num_MFeeRecPayCCY       ,
                                                		num_MFeeRecPayLoc       ,
                                                		num_MIntAdvPayAccrCCY   ,
                                                		num_MIntAdvPayAccrLoc   ,
                                                		num_MInterRecPayCCY     ,
                                                		num_MInterRecPayLoc     ,
                                                		num_MInterAccrCCY       ,
                                                		num_MInterAccrLoc       ,
                                                		num_MPrincReadjCCY      ,
                                                		num_MInterReadjCCY      ,
                                                        num_Error       );
		         PGT_PRG.Pkg_Miscdmgeneral.p_GetMISSwapNPV(dte_MISAcct,
                                                        num_Header              ,
                                                        num_Direction           ,
  			                                            num_MMarkValueCCY       ,
                            							num_MarketValueMan);
/*                  dbms_output.put_line('Soy Swap ');
                  dbms_output.put_line('num_MMarkValueCCY ' || num_MMarkValueCCY );
            dbms_output.put_line('num_MCancelRecPayCCY ' || num_MCancelRecPayCCY);
            dbms_output.put_line('num_MFeeAccrCCY ' || num_MFeeAccrCCY);
            dbms_output.put_line('num_MFeeAdvPayAccrCCY ' || num_MFeeAdvPayAccrCCY);
            dbms_output.put_line('num_MFeeRecPayCCY ' || num_MFeeRecPayCCY);
            dbms_output.put_line('num_MIntAdvPayAccrCCY ' || num_MIntAdvPayAccrCCY);
            dbms_output.put_line('num_MInterRecPayCCY ' || num_MInterRecPayCCY);
            dbms_output.put_line('num_MInterAccrCCY ' || num_MInterAccrCCY);
            dbms_output.put_line('num_MPrincReadjCCY ' || num_MPrincReadjCCY);
            dbms_output.put_line('num_MInterReadjCCY ' || num_MInterReadjCCY); */
    	ELSIF num_Instrument = 20.4 THEN -- CCS
                PGT_PRG.Pkg_Miscdmgeneral.p_GetCCSMarkToMarket( dte_MISAcct             ,
                                                        num_Header              ,
                                                        num_Direction           ,
                                                		num_MPrincCashCCY       ,
                                                		num_MPrincCashLoc       ,
                                                		num_MCancelRecPayCCY    ,
                                                		num_MCancelRecPayLoc    ,
                                                		num_MFeeAccrCCY         ,
                                                		num_MFeeAccrLoc         ,
                                                		num_MFeeAdvPayAccrCCY   ,
                                                		num_MFeeAdvPayAccrLoc   ,
                                                		num_MFeeRecPayCCY       ,
                                                		num_MFeeRecPayLoc       ,
                                                		num_MIntAdvPayAccrCCY   ,
                                                		num_MIntAdvPayAccrLoc   ,
                                                		num_MInterRecPayCCY     ,
                                                		num_MInterRecPayLoc     ,
                                                		num_MInterAccrCCY       ,
                                                		num_MInterAccrLoc       ,
                                                		num_MPrincReadjCCY      ,
                                                		num_MInterReadjCCY      ,
                                                        num_Error       );
        		PGT_PRG.Pkg_Miscdmgeneral.p_GetMISCCSNPV(dte_MISAcct,
                                                        num_Header              ,
                                                        num_Direction           ,
          			                                    num_MMarkValueCCY       ,
                            							num_MarketValueMan);
     	ELSE
         		PGT_PRG.Pkg_Miscdmgeneral.p_GetMMMarkToMarket (        dte_MISAcct      ,
		                                                num_Header              ,
  			                                            num_MPrincCashCCY       ,
     		                                            num_MPrincCashLoc       ,
       		                                         	num_MCancelRecPayCCY    ,
                                                		num_MCancelRecPayLoc    ,
                                                		num_MFeeAccrCCY         ,
                                                		num_MFeeAccrLoc         ,
                                                		num_MFeeAdvPayAccrCCY   ,
                                                		num_MFeeAdvPayAccrLoc   ,
                                                		num_MFeeRecPayCCY       ,
                                                		num_MFeeRecPayLoc       ,
                                                		num_MIntAdvPayAccrCCY   ,
                                                		num_MIntAdvPayAccrLoc   ,
                                                		num_MInterRecPayCCY     ,
                                                		num_MInterRecPayLoc     ,
                                                		num_MInterAccrCCY       ,
                                                		num_MInterAccrLoc       ,
                                                		num_MPrincReadjCCY      ,
                                                		num_MInterReadjCCY      ,
                                                        num_Error       );
        		PGT_PRG.Pkg_Miscdmgeneral.p_GetMISMMNPV(dte_MISAcct,
                                                        num_Header              ,
                                                		num_MMarkValueCCY       ,
                            							num_MarketValueMan);
     	END IF;
		/* Si el instrumento son CCS el Principal se obtiene de una vista del MIS 	*/
		IF num_Instrument = 20.4 THEN -- CCS
			BEGIN
				SELECT PRINCIPALCUR
				  INTO num_PrincipalCur
				  FROM "DEVENG".V_PGT_ENGSWDATAMIS_S
				 WHERE DEAL_ID	= num_Header
				   AND DDATE = dte_MISAcct
				   AND FK_DIRECTION = num_Direction;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					num_PrincipalCur   := 0;
				WHEN OTHERS THEN
		    		"PGT_PRG".Pkg_Pgterror.p_RaiseError(57.4, 'Principal MIS Information. Date: ' || dte_MISAcct || ', Deal Id: ' || num_Header|| ', Direction: ' || num_Direction);
			END;
			num_MPrincCashCCY := num_PrincipalCur;
		END IF;
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* OBTENEMOS TODAS NUESTRAS CUENTAS PARA REALIZAR LOS MOVIMIENTOS ...   */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* Buscamos la llave de la cuenta Revaluación de ope. financieras ...   */
        /* ******************************************************************** */
        num_Key := PGT_PRG.Pkg_Acct_Trg.f_GetAcctKey (  num_CRevOpFin           ,
                                                num_Branch              ,
                                                num_Currency            ,
                                                num_Instrument          ,
                                                NULL                    ,
                                                NULL                    ,
                                                NULL                    ,
                                                num_Folder              ,
                                                NULL                    ,
                                                num_Event               ,
                                                num_Properties          ,
                                                num_TRevOpFin           );
        /* ******************************************************************** */
        /* Buscamos la posición de la cuenta Revaluación de ope. financieras    */
        /* ******************************************************************** */
        PGT_PRG.Pkg_Acct_Trg.p_Get_GLTA_Bal (           dte_Proceso             ,
                                                num_Key                 ,
                                                dte_Proceso             ,
                                                num_PosRevOpFinCCYCr    ,
                                                num_PosRevOpFinCCYDb    ,
                                                num_PosRevOpFinLocCr    ,
                                                num_PosRevOpFinLocDb    ,
                                                num_Quantity            ,
                                                num_Reg                 );
        /* ******************************************************************** */
        /* Obtenemos los saldos de nuestra cuenta de la Opcion...               */
        /* ******************************************************************** */
        num_PosRevOpFinCCY      := num_PosRevOpFinCCYDb - num_PosRevOpFinCCYCr;
        num_PosRevOpFinLoc      := num_PosRevOpFinLocDb - num_PosRevOpFinLocCr;
--    dbms_output.put_line('Revaluacion ' || num_PosRevOpFinCCY);
        /* ******************************************************************** */
        /* Buscamos la llave de la cuenta Devaluación de ope. financieras ...   */
        /* ******************************************************************** */
        num_Key := PGT_PRG.Pkg_Acct_Trg.f_GetAcctKey (  num_CDevOpFin           ,
                                                num_Branch              ,
                                                num_Currency            ,
                                                num_Instrument          ,
                                                NULL                    ,
                                                NULL                    ,
                                                NULL                    ,
                                                num_Folder              ,
                                                NULL                    ,
                                                num_Event               ,
                                                num_Properties          ,
                                                num_TDevOpFin           );
        /* ******************************************************************** */
        /* Buscamos la posición de la cuenta Devaluación de ope. financieras    */
        /* ******************************************************************** */
        PGT_PRG.Pkg_Acct_Trg.p_Get_GLTA_Bal (           dte_Proceso             ,
                                                num_Key                 ,
                                                dte_Proceso             ,
                                                num_PosDevOpFinCCYCr    ,
                                                num_PosDevOpFinCCYDb    ,
                                                num_PosDevOpFinLocCr    ,
                                                num_PosDevOpFinLocDb    ,
                                                num_Quantity            ,
                                                num_Reg                 );
        /* ******************************************************************** */
        /* Obtenemos los saldos de nuestra cuenta de la Opcion...               */
        /* ******************************************************************** */
        num_PosDevOpFinCCY      := num_PosDevOpFinCCYDb - num_PosDevOpFinCCYCr;
        num_PosDevOpFinLoc      := num_PosDevOpFinLocDb - num_PosDevOpFinLocCr;
--    dbms_output.put_line('Devaluacion ' || num_PosDevOpFinCCY);
        /* ******************************************************************** */
        /*      MOVIMIENTO ...                                                  */
        /* ******************************************************************** */
        /* Mark to Market  ...                                                  */
        /* Las cuentas afectadas en el movimiento son:                          */
        /*      - Revaluación de operaciones financieras                        */
        /*      - Devaluación de operaciones financieras                        */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* El cálculo del valor de mercado se realizará de acuerdo a la         */
        /* siguiente fórmula.                                                   */
        /* ******************************************************************** */
        IF num_Instrument = 2.4 OR num_Instrument = 20.4 THEN -- CCS / MM
                num_DMTMValueCCY        := num_MMarkValueCCY + num_MPrincCashCCY - num_MCancelRecPayCCY
                                        - num_MFeeAccrCCY - num_MFeeAdvPayAccrCCY
                                        - num_MFeeRecPayCCY - num_MIntAdvPayAccrCCY
                                        - num_MInterRecPayCCY - num_MInterAccrCCY
                                        - num_MPrincReadjCCY - num_MInterReadjCCY
                                        - (num_PosRevOpFinCCY + num_PosDevOpFinCCY);
        ELSE
                num_DMTMValueCCY        := num_MMarkValueCCY - num_MCancelRecPayCCY
                                        - num_MFeeAccrCCY - num_MFeeAdvPayAccrCCY
                                        - num_MFeeRecPayCCY - num_MIntAdvPayAccrCCY
                                        - num_MInterRecPayCCY - num_MInterAccrCCY
                                        - num_MPrincReadjCCY - num_MInterReadjCCY
                                        - (num_PosRevOpFinCCY + num_PosDevOpFinCCY);
        END IF;
--    dbms_output.put_line('Resultado Calculo ' || num_DMTMValueCCY);
        /* ******************************************************************** */
        /* Obtenemos el importe local buscando su contravalor                   */
        /* ******************************************************************** */
        IF num_Currency <> num_LocalCurr THEN
--                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
--                                num_Branch              ,
--                                dte_MISAcct             ,
--                                num_Currency            ,
--                                num_DMTMValueCCY        ,
--                                num_LocalCurr           ,
--                                num_DMTMValueLoc        );


		   -- Mod. Valor Mercado
--		   IF num_Branch = 20010.4 and num_Instrument in (20092.4,20.4) and num_Currency <> 172.4 and p_mercado <> -1 THEN
		   IF num_Branch = 20010.4 AND num_Instrument IN (20092.4) AND num_Currency <> 172.4 AND p_mercado <> -1 THEN
		      num_DMTMValueLoc := num_DMTMValueCCY * p_mercado;
		   ELSE
                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_MISAcct             ,
                                num_Currency            ,
                                num_DMTMValueCCY        ,
                                num_LocalCurr           ,
                                num_DMTMValueLoc        );
		   END IF;
        ELSE
                num_DMTMValueLoc        := num_DMTMValueCCY;
        END IF;
        /* ******************************************************************** */
        /* Calculamos el ajuste de MTM en moneda Local                          */
        /* ******************************************************************** */
        IF num_DMTMValueLoc > 0 THEN
                num_AValBenLoc  := num_DMTMValueLoc;
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_AValBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Proceso     ,
                                                                num_AValBenLoc);
                num_AValPerLoc  := 0;
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en moneda local ...       */
                /*      - Revaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosRevOpFinLoc      := num_PosRevOpFinLoc + num_AValBenLoc;
        ELSE
                num_AValBenLoc  := 0;
                num_AValPerLoc  := ABS (num_DMTMValueLoc);
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_AValPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_LocalCurr   ,
                                                                dte_Proceso     ,
                                                                num_AValPerLoc);
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en moneda local ...       */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinLoc      := num_PosDevOpFinLoc - num_AValPerLoc;
        END IF;
        /* ******************************************************************** */
        /* Calculamos el ajuste de MTM                                                                                  */
        /* ******************************************************************** */
        IF num_DMTMValueCCY > 0 THEN
                num_AValBenCCY  := num_DMTMValueCCY;
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_AValBenCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_Currency    ,
                                                                dte_Proceso     ,
                                                                num_AValBenCCY);
                num_AValPerCCY  := 0;
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Revaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosRevOpFinCCY      := num_PosRevOpFinCCY + num_AValBenCCY;
        ELSE
                num_AValBenCCY  := 0;
                num_AValPerCCY  := ABS (num_DMTMValueCCY);
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_AValPerCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                                num_Currency    ,
                                                                dte_Proceso     ,
                                                                num_AValPerCCY);
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinCCY      := num_PosDevOpFinCCY - num_AValPerCCY;
        END IF;
        /* ******************************************************************** */
        /*      MOVIMIENTO ...                                                                                                          */
        /* ******************************************************************** */
        /* Revaluación por tipo de cambio de Mark to Market  ...                */
        /* Las cuentas afectadas en el movimiento son:                          */
        /*      - Revaluación de operaciones financieras                        */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* Obtenemos el importe local buscando su contravalor                   */
        /* ******************************************************************** */
        IF num_Currency <> num_LocalCurr THEN
--                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
--                                num_Branch              ,
--                                dte_MISAcct             ,
--                                num_Currency            ,
--                                num_PosRevOpFinCCY      ,
--                                num_LocalCurr           ,
--                                num_AuxRevLoc   );

		   -- Mod. Valor Mercado
		   --IF num_Branch = 20010.4 and num_Instrument in (20092.4,20.4) and num_Currency <> 172.4 and p_mercado <> -1 THEN
		   IF num_Branch = 20010.4 AND num_Instrument IN (20092.4) AND num_Currency <> 172.4 AND p_mercado <> -1 THEN
		      num_AuxRevLoc := num_PosRevOpFinCCY * p_mercado;
		   ELSE
                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_MISAcct             ,
                                num_Currency            ,
                                num_PosRevOpFinCCY      ,
                                num_LocalCurr           ,
                                num_AuxRevLoc   );
		   END IF;

        ELSE
                num_AuxRevLoc   := num_PosRevOpFinCCY;
        END IF;
        /* ******************************************************************** */
        /* num_PosRevOpFinLoc : Saldo local la revaluación MTM                  */
        /* ******************************************************************** */
        num_DRevMTML    := num_AuxRevLoc - num_PosRevOpFinLoc ;
        /* ******************************************************************** */
        /* Calculamos el sentido de la revaluación de MTM                       */
        /* ******************************************************************** */
        IF num_DRevMTML > 0 THEN
                num_ARevBenLoc  := num_DRevMTML;
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_ARevBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_ARevBenLoc  );
                num_ARevPerLoc  := 0;
        ELSE
                num_ARevBenLoc  := 0;
                num_ARevPerLoc  := ABS (num_DRevMTML);
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_ARevPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_ARevPerLoc  );
        END IF;
        /* ******************************************************************** */
        /*      MOVIMIENTO ...                                                  */
        /* ******************************************************************** */
        /* Revaluación por tipo de cambio de Mark to Market  ...                */
        /* Las cuentas afectadas en el movimiento son:                          */
        /*      - Devaluación de operaciones financieras                        */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* Obtenemos el importe local buscando su contravalor                   */
        /* ******************************************************************** */
        IF num_Currency <> num_LocalCurr THEN
--                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
--                                num_Branch              ,
--                                dte_MISAcct             ,
--                                num_Currency            ,
--                                num_PosDevOpFinCCY      ,
--                                num_LocalCurr           ,
--                                num_AuxDevLoc   );

		   -- Mod. Valor Mercado
--		   IF num_Branch = 20010.4 and num_Instrument in (20092.4,20.4) and num_Currency <> 172.4 and p_mercado <> -1 THEN
		   IF num_Branch = 20010.4 AND num_Instrument IN (20092.4) AND num_Currency <> 172.4 AND p_mercado <> -1 THEN
		      num_AuxDevLoc := num_PosDevOpFinCCY * p_mercado;
           ELSE
                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_MISAcct             ,
                                num_Currency            ,
                                num_PosDevOpFinCCY      ,
                                num_LocalCurr           ,
                                num_AuxDevLoc   );
		   END IF;
        ELSE
                num_AuxDevLoc   := num_PosDevOpFinCCY;
        END IF;
        /* ******************************************************************** */
        /* num_PosDevOpFinLoc : Saldo local la devaluación MTM                  */
        /* ******************************************************************** */
        num_DDevMTML    := num_AuxDevLoc - num_PosDevOpFinLoc ;
        /* ******************************************************************** */
        /* Calculamos el sentido de la devaluación de MTM                       */
        /* ******************************************************************** */
        IF num_DDevMTML > 0 THEN
                num_ADevPerLoc  := 0;
                num_ADevBenLoc  := ABS (num_DDevMTML);
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_ADevBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_ADevBenLoc  );
        ELSE
                num_ADevPerLoc  := ABS (num_DDevMTML);
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_ADevPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_ADevPerLoc  );
                num_ADevBenLoc  := 0;
        END IF;
END p_GetValMarketValue_Loc;

PROCEDURE p_Act_Financiero (  dte_Reference                   IN  DATE,
                                        num_Branch                      IN  NUMBER,
                                        num_Instrument                  IN  NUMBER)
AS
p_valor_mercado NUMBER;
Flag			INTEGER;

CURSOR C_Swap IS
   SELECT b.PK,
	   d.FK_DIRECTION,
	   d.FK_CURRENCY
   FROM pgt_trd.t_pgt_trade_events_s a,
	    pgt_trd.t_pgt_trade_header_s b,
	 	pgt_trd.t_pgt_ccs_s c,
	 	pgt_trd.t_pgt_intrate_s d
   WHERE a.PK = b.FK_PARENT
   AND   a.FK_INSTRUMENT = 20092.4
   AND   a.FK_BRANCH = 20010.4
   AND   a.FK_STATUS = 96.4
   AND   a.REVDATE IS NULL
   AND   a.FK_REVREASON IS NULL
   AND   b.PK = c.FK_PARENT
   AND   c.MATDATE > SYSDATE
   AND   c.PK = d.FK_PARENT
   AND   d.FK_OWNER_OBJ = 1752.4
   AND   d.FK_CURRENCY <> 172.4
   AND   d.FK_CURRENCY <> 20233.4;

CURSOR C_CCS IS
   SELECT b.pk,
	   d.FK_DIRECTION,
	   d.FK_CURRENCY
   FROM pgt_trd.t_pgt_trade_events_s a,
	    pgt_trd.t_pgt_trade_header_s b,
	 	pgt_trd.t_pgt_ccs_s c,
	 	pgt_trd.t_pgt_intrate_s d
   WHERE a.PK = b.FK_PARENT
   AND   a.FK_INSTRUMENT = 20.4
   AND   a.FK_BRANCH = 20010.4
   AND   a.FK_STATUS = 96.4
   AND   a.REVDATE IS NULL
   AND   a.FK_REVREASON IS NULL
   AND   b.PK = c.FK_PARENT
   AND   c.MATDATE > SYSDATE
   AND   c.PK = d.FK_PARENT
   AND   d.FK_OWNER_OBJ = 1752.4
   AND   d.FK_CURRENCY <> 172.4
   AND   d.FK_CURRENCY <> 20233.4;

p_Swap C_Swap%ROWTYPE;
p_CCS C_CCS%ROWTYPE;
p_Fecha  VARCHAR2(10);
--p_source Number  := 2.24; --Desarrollo
p_source NUMBER  := 122.4; --Produccion y Test

BEGIN
   p_valor_mercado := 0;
   Flag			   := 0;

   SELECT TO_CHAR(dte_Reference,'dd/mm/yyyy')
   INTO   p_Fecha
   FROM dual;

   IF num_Instrument = 20092.4 AND num_Branch = 20010.4 THEN
      OPEN C_Swap;
	  FETCH C_Swap INTO p_Swap;
	  WHILE C_Swap%FOUND LOOP
	      p_valor_mercado := chi_con.F_Conv_A_Src('172.4',TO_CHAR(p_Swap.FK_CURRENCY),p_source,p_Fecha,1);

	      BEGIN
      	  UPDATE DEVENG.T_PGT_IRFINANCST_S G SET
             G.INTERESTACCRUALL = ROUND(G.INTERESTACCRUAL * DECODE(p_valor_mercado,-1,1,p_valor_mercado))
          WHERE G.ddate        = dte_Reference
	      AND   G.FK_BRANCH    = 20010.4
	      AND   G.DEAL_ID      = p_Swap.PK
		  AND   G.FK_DIRECTION = p_Swap.FK_DIRECTION;
          EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO: ' || TO_CHAR(p_Swap.PK));
	      END;

	      FETCH C_Swap INTO p_Swap;
	  END LOOP;
   ELSIF num_Instrument = 20.4 AND num_Branch = 20010.4 THEN
      OPEN C_CCS;
	  FETCH C_CCS INTO p_CCS;
	  WHILE C_CCS%FOUND LOOP
	      p_valor_mercado := chi_con.F_Conv_A_Src('172.4',TO_CHAR(p_CCS.FK_CURRENCY),p_source,p_Fecha,1);

	      BEGIN
          UPDATE DEVENG.T_PGT_SWFINANCST_S G SET
              G.INTERESTACCRUALL = ROUND(G.INTERESTACCRUAL * DECODE(p_valor_mercado,-1,1,p_valor_mercado))
          WHERE G.ddate        = dte_Reference
  	      AND   G.FK_BRANCH    = 20010.4
	      AND   G.DEAL_ID      = p_CCS.PK
		  AND   G.FK_DIRECTION = p_CCS.FK_DIRECTION;
          EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO');
	      END;

	      FETCH C_CCS INTO p_CCS;
	  END LOOP;
   END IF;

END p_Act_Financiero;

PROCEDURE p_Act_FinCurrencyChange (  dte_Reference                   IN  DATE,
                                        num_Branch                      IN  NUMBER,
                                        num_Instrument                  IN  NUMBER)
IS

/*CURSOR C_CCS IS
   SELECT DISTINCT C.FK_REGISTRY    PkHeader,
   		  C.PREVSETTDATE   Fecha,
		  B.FK_FINCURRENCY MonedaPago,
		  C.FK_DIRECTION   Direccion
   FROM PGT_TRD.T_PGT_TRADE_EVENTS_S A,
     	PGT_TRD.T_PGT_CF_CURRENC_CHANGE_S B,
	 	PGT_TRD.T_PGT_CASH_FLOWS_S C
   WHERE A.PK = B.FK_PARENT
   AND   A.EVENTDATE <= dte_Reference
   AND   A.FK_STATUS = 96.4
   AND   A.FK_INSTRUMENT = 20.4
   AND   A.FK_BRANCH = 20010.4
   AND   B.FK_CASHFLOW = C.PK
 --  AND   C.FK_CASHFLOWTYPE = 5.4 -- Interes
--   AND   (C.PREVSETTDATE = dte_Reference or pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,C.PREVSETTDATE,1) = dte_Reference);
   AND   C.PREVSETTDATE = dte_Reference;*/

CURSOR C_CCSP IS
   SELECT DISTINCT C.FK_REGISTRY    PkHeader1,
   		  C.CASHFLOWDATE   Fecha1,
		  B.FK_FINCURRENCY MonedaPago1,
		  C.FK_DIRECTION   Direccion1
   FROM PGT_TRD.T_PGT_TRADE_EVENTS_S A,
     	PGT_TRD.T_PGT_CF_CURRENC_CHANGE_S B,
	 	PGT_TRD.T_PGT_CASH_FLOWS_S C
   WHERE A.PK = B.FK_PARENT
   AND   A.EVENTDATE <= dte_Reference
   AND   A.FK_STATUS = 96.4
   AND   A.FK_INSTRUMENT = 20.4
   AND   A.FK_BRANCH = 20010.4
   AND   B.FK_CASHFLOW = C.PK
   AND   C.FK_CASHFLOWTYPE = 5.4 -- Interes
--   AND   (C.PREVSETTDATE = dte_Reference or pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,C.PREVSETTDATE,1) = dte_Reference);
   AND   C.CASHFLOWDATE < C.PREVSETTDATE
   AND   C.CASHFLOWDATE = dte_Reference;

--p_CCS  C_CCS%ROWTYPE;
p_CCSP C_CCSP%ROWTYPE;
p_ValorPago1    NUMBER;
p_FinPago1      NUMBER;
p_ValorPagoLoc1 NUMBER;
p_ValorPago2    NUMBER;
p_FinPago2      NUMBER;
p_ValorPagoLoc2 NUMBER;
p_ValorPago3    NUMBER;
p_FinPago3      NUMBER;
p_ValorPagoLoc3 NUMBER;
p_ValorPago4    NUMBER;
p_FinPago4      NUMBER;
p_ValorPagoLoc4 NUMBER;
p_ValorPago5    NUMBER;
p_FinPago5      NUMBER;
p_ValorPagoLoc5 NUMBER;
p_ValorPago6    NUMBER;
p_FinPago6      NUMBER;
p_ValorPagoLoc6 NUMBER;
p_ValorPago7    NUMBER;
p_FinPago7      NUMBER;
p_ValorPagoLoc7 NUMBER;
p_ValorPago8    NUMBER;
p_FinPago8      NUMBER;
p_ValorPagoLoc8 NUMBER;
num_LocalCurr  NUMBER;
p_Direccion1   NUMBER;
p_Direccion2   NUMBER;
p_MonedaAss	   NUMBER;
p_MonedaLiab   NUMBER;
p_Moneda	   NUMBER;
p_MonedaAssp   NUMBER;
p_MonedaLiabp  NUMBER;
p_ValorPago11  NUMBER;
p_ValorPagoLoc11 NUMBER;

BEGIN
 IF num_Branch = 20010.4 AND num_Instrument = 20.4 THEN
      num_LocalCurr := 172.4;
/*---------------------------VALUTA-------------------------*/
      OPEN C_CCSP;

	  FETCH C_CCSP INTO p_CCSP;

	  WHILE C_CCSP%FOUND LOOP

		BEGIN
		SELECT b.FK_CURRENCY, c.FK_CURRENCY
		INTO  p_MonedaAssp, p_MonedaLiabp
		FROM PGT_TRD.T_PGT_CCS_S A,
			 PGT_TRD.T_PGT_INTRATE_S B,
			 PGT_TRD.T_PGT_INTRATE_S C
		WHERE A.FK_PARENT = p_CCSP.PkHeader1
		AND   A.PK = B.FK_PARENT
		AND   B.FK_OWNER_OBJ = 1752.4
		AND   B.FK_DIRECTION = 183.4
		AND   A.pk = C.FK_PARENT
		AND   C.FK_OWNER_OBJ = 1752.4
		AND   C.FK_DIRECTION = 184.4;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_MonedaAssp  := 0;
		   p_Monedaliabp := 0;
		  WHEN OTHERS THEN
		   p_MonedaAssp  := 0;
		   p_MonedaLiabp := 0;
		END;

	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_CCSP.MonedaPago1, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago11
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCSP.PkHeader1
		AND   A.CASHFLOWDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_CCSP.MonedaPago1
		AND   A.FK_CASHFLOWTYPE = 5.4 -- Interes
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago11 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago11 := 0;
		END;

		IF p_ValorPago11 <> 0 AND p_CCSP.MonedaPago1 <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_CCSP.MonedaPago1            ,
                                p_ValorPago11      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc11   );
		ELSE
		   p_ValorPagoLoc11 := p_ValorPago11;
		END IF;

		IF p_ValorPago11 <> 0 THEN
           p_ValorPago11  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_CCSP.MonedaPago1   ,
                                                        dte_Reference    ,
                                                        p_ValorPago11  );

           p_ValorPagoLoc11  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc11  );
		END IF;
		IF p_CCSP.MonedaPago1 = p_MonedaAssp THEN

	       BEGIN
		   UPDATE DEVENG.T_PGT_SWFINANCST_S H SET
		      H.INTERESTRECPAY     = p_ValorPago11,
		      H.INTERESTRECPAYL    = p_ValorPagoLoc11
		   WHERE H.DEAL_ID = p_CCSP.PkHeader1
		   AND	 H.DDATE = dte_Reference
		   AND   H.FK_DIRECTION = 183.4;
           EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO Currency ChangeP1');
		   END;

	       BEGIN
		   UPDATE DEVENG.T_PGT_SWFINANCST_S H SET
		      H.INTERESTRECPAY     = 0,
		      H.INTERESTRECPAYL    = 0
		   WHERE H.DEAL_ID = p_CCSP.PkHeader1
		   AND	 H.DDATE = dte_Reference
		   AND   H.FK_DIRECTION = 184.4;
           EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO Currency ChangeP1');
		   END;

		ELSE
	       BEGIN
		   UPDATE DEVENG.T_PGT_SWFINANCST_S H SET
		      H.INTERESTRECPAY     = p_ValorPago11,
		      H.INTERESTRECPAYL    = p_ValorPagoLoc11
		   WHERE H.DEAL_ID = p_CCSP.PkHeader1
		   AND	 H.DDATE = dte_Reference
		   AND   H.FK_DIRECTION = 184.4;
           EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO Currency ChangeP2');
		   END;

	       BEGIN
		   UPDATE DEVENG.T_PGT_SWFINANCST_S H SET
		      H.INTERESTRECPAY     = 0,
		      H.INTERESTRECPAYL    = 0
		   WHERE H.DEAL_ID = p_CCSP.PkHeader1
		   AND	 H.DDATE = dte_Reference
		   AND   H.FK_DIRECTION = 183.4;
           EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO Currency ChangeP2');
		   END;
		END IF;


	    FETCH C_CCSP INTO p_CCSP;
	  END LOOP;

	  CLOSE C_CCSP;

/*---------------------------VALUTA-------------------------*/

    /*  OPEN C_CCS;

	  FETCH C_CCS INTO p_CCS;

	  WHILE C_CCS%FOUND LOOP

	    p_ValorPago1    := 0;
		p_ValorPagoLoc1 := 0;
	    p_ValorPago2    := 0;
		p_ValorPagoLoc2 := 0;
		p_FinPago1      := 0;
		p_FinPago2      := 0;
		p_Direccion1   := 0;
		p_Direccion2   := 0;
		p_MonedaAss	   := 0;
		p_MonedaLiab   := 0;
		p_Moneda	   := 0;

		BEGIN
		SELECT b.FK_CURRENCY, c.FK_CURRENCY
		INTO  p_MonedaAss, p_MonedaLiab
		FROM PGT_TRD.T_PGT_CCS_S A,
			 PGT_TRD.T_PGT_INTRATE_S B,
			 PGT_TRD.T_PGT_INTRATE_S C
		WHERE A.FK_PARENT = p_CCS.PkHeader
		AND   A.PK = B.FK_PARENT
		AND   B.FK_OWNER_OBJ = 1752.4
		AND   B.FK_DIRECTION = 183.4
		AND   A.pk = C.FK_PARENT
		AND   C.FK_OWNER_OBJ = 1752.4
		AND   C.FK_DIRECTION = 184.4;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_MonedaAss  := 0;
		   p_Monedaliab := 0;
		  WHEN OTHERS THEN
		   p_MonedaAss  := 0;
		   p_MonedaLiab := 0;
		END;

		IF p_MonedaAss <> 0 AND p_MonedaLiab <> 0 THEN
		   IF p_MonedaAss = p_CCS.MonedaPago THEN
		      p_Moneda := p_MonedaLiab;
		   ELSE
		   	  p_Moneda := p_MonedaAss;
		   END IF;
		ELSE
          RAISE_APPLICATION_ERROR(-20001,  'NO SE ENCONTRO MONEDAS');
		END IF;

	  --- Interes 5.4  ------
	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_CCS.MonedaPago, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago1
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCS.PkHeader
		AND   A.PREVSETTDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_CCS.MonedaPago
		AND   A.FK_CASHFLOWTYPE = 5.4 -- Interes
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago1 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago1 := 0;
		END;

	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_Moneda, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago2
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCS.PkHeader
		AND   A.PREVSETTDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_Moneda
		AND   A.FK_CASHFLOWTYPE = 5.4 -- Interes
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago2 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago2 := 0;
		END;

	  --- Nominal 3.4  ------
	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_CCS.MonedaPago, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago3
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCS.PkHeader
		AND   A.PREVSETTDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_CCS.MonedaPago
		AND   A.FK_CASHFLOWTYPE = 3.4 -- Nominal
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago3 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago3 := 0;
		END;

	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_Moneda, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago4
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCS.PkHeader
		AND   A.PREVSETTDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_Moneda
		AND   A.FK_CASHFLOWTYPE = 3.4 -- Nominal
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago4 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago4 := 0;
		END;

	  --- Interes Adjust 2.4  ------
	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_CCS.MonedaPago, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago5
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCS.PkHeader
		AND   A.PREVSETTDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_CCS.MonedaPago
		AND   A.FK_CASHFLOWTYPE = 2.4 -- Interes Adjust
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago5 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago5 := 0;
		END;

	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_Moneda, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago6
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCS.PkHeader
		AND   A.PREVSETTDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_Moneda
		AND   A.FK_CASHFLOWTYPE = 2.4 -- Interes Adjust
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago6 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago6 := 0;
		END;

	  ---  Nominal Adjust 1.4  ------
	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_CCS.MonedaPago, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago7
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCS.PkHeader
		AND   A.PREVSETTDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_CCS.MonedaPago
		AND   A.FK_CASHFLOWTYPE = 1.4 -- Nominal Adjust
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago7 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago7 := 0;
		END;

	    BEGIN
		SELECT NVL(SUM(PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (p_Moneda, dte_Reference, A.AMOUNT )),0)--SUM(A.AMOUNT)
		INTO p_ValorPago8
		FROM PGT_TRD.T_PGT_CASH_FLOWS_S A
		WHERE A.FK_OWNER_OBJ    = 1476.4
		AND   A.FK_REGISTRY     = p_CCS.PkHeader
		AND   A.PREVSETTDATE    = dte_Reference
		AND   A.FK_CURRENCY     = p_Moneda
		AND   A.FK_CASHFLOWTYPE = 1.4 -- Nominal Adjust
		AND   A.FK_REVREASON IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		   p_ValorPago8 := 0;
		  WHEN OTHERS THEN
		   p_ValorPago8 := 0;
		END;


		IF p_CCS.Direccion = 184.4 THEN
		   p_Direccion1 := 183.4;
		   p_Direccion2 := 184.4;

		   BEGIN
		   SELECT G.INTERESTCASH, G.PRINCIPALCASH, G.INTERESTREADJCASH, G.PRINCIPALREADJCASH
		   INTO p_FinPago1, p_FinPago3, p_FinPago5, p_FinPago7
		   FROM DEVENG.T_PGT_SWFINANCST_S G
		   WHERE G.DEAL_ID = p_CCS.PkHeader
		   AND	 G.DDATE = pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,dte_Reference,-1)
		   AND   G.FK_DIRECTION = 183.4;
		   EXCEPTION WHEN NO_DATA_FOUND THEN
		       p_FinPago1 := 0;
		       p_FinPago3 := 0;
		       p_FinPago5 := 0;
		       p_FinPago7 := 0;
			 WHEN OTHERS THEN
		       p_FinPago1 := 0;
		       p_FinPago3 := 0;
		       p_FinPago5 := 0;
		       p_FinPago7 := 0;
		   END;

		   BEGIN
		   SELECT G.INTERESTCASH, G.PRINCIPALCASH, G.INTERESTREADJCASH, G.PRINCIPALREADJCASH
		   INTO p_FinPago2, p_FinPago4, p_FinPago6, p_FinPago8
		   FROM DEVENG.T_PGT_SWFINANCST_S G
		   WHERE G.DEAL_ID = p_CCS.PkHeader
		   AND	 G.DDATE = pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,dte_Reference,-1)
		   AND   G.FK_DIRECTION = 184.4;
		   EXCEPTION WHEN NO_DATA_FOUND THEN
		       p_FinPago2 := 0;
		       p_FinPago4 := 0;
		       p_FinPago6 := 0;
		       p_FinPago8 := 0;
			 WHEN OTHERS THEN
		       p_FinPago2 := 0;
		       p_FinPago4 := 0;
		       p_FinPago6 := 0;
		       p_FinPago8 := 0;
		   END;
		ELSE
		   p_Direccion1 := 184.4;
		   p_Direccion2 := 183.4;
		   BEGIN
		   SELECT G.INTERESTCASH, G.PRINCIPALCASH, G.INTERESTREADJCASH, G.PRINCIPALREADJCASH
		   INTO p_FinPago1, p_FinPago3, p_FinPago5, p_FinPago7
		   FROM DEVENG.T_PGT_SWFINANCST_S G
		   WHERE G.DEAL_ID = p_CCS.PkHeader
		   AND	 G.DDATE = pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,dte_Reference,-1)
		   AND   G.FK_DIRECTION = 184.4;
		   EXCEPTION WHEN NO_DATA_FOUND THEN
		       p_FinPago1 := 0;
		       p_FinPago3 := 0;
		       p_FinPago5 := 0;
		       p_FinPago7 := 0;
			 WHEN OTHERS THEN
		       p_FinPago1 := 0;
		       p_FinPago3 := 0;
		       p_FinPago5 := 0;
		       p_FinPago7 := 0;
		   END;

		   BEGIN
		   SELECT G.INTERESTCASH, G.PRINCIPALCASH, G.INTERESTREADJCASH, G.PRINCIPALREADJCASH
		   INTO p_FinPago2, p_FinPago4, p_FinPago6, p_FinPago8
		   FROM DEVENG.T_PGT_SWFINANCST_S G
		   WHERE G.DEAL_ID = p_CCS.PkHeader
		   AND	 G.DDATE = pgt_prg.pkg_calendar.F_GOWORKDAYS(94.4,dte_Reference,-1)
		   AND   G.FK_DIRECTION = 183.4;
		   EXCEPTION WHEN NO_DATA_FOUND THEN
		       p_FinPago2 := 0;
		       p_FinPago4 := 0;
		       p_FinPago6 := 0;
		       p_FinPago8 := 0;
			 WHEN OTHERS THEN
		       p_FinPago2 := 0;
		       p_FinPago4 := 0;
		       p_FinPago6 := 0;
		       p_FinPago8 := 0;
		   END;
		END IF;

		p_ValorPago1 := p_ValorPago1 + p_FinPago1;
		p_ValorPago2 := p_ValorPago2 + p_FinPago2;
		p_ValorPago3 := p_ValorPago3 + p_FinPago3;
		p_ValorPago4 := p_ValorPago4 + p_FinPago4;
		p_ValorPago5 := p_ValorPago5 + p_FinPago5;
		p_ValorPago6 := p_ValorPago6 + p_FinPago6;
		p_ValorPago7 := p_ValorPago7 + p_FinPago7;
		p_ValorPago8 := p_ValorPago8 + p_FinPago8;

		IF p_ValorPago1 <> 0 AND p_CCS.MonedaPago <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_CCS.MonedaPago            ,
                                p_ValorPago1      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc1   );
		ELSE
		   p_ValorPagoLoc1 := p_ValorPago1;
		END IF;

		IF p_ValorPago1 <> 0 THEN
           p_ValorPago1  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_CCS.MonedaPago   ,
                                                        dte_Reference    ,
                                                        p_ValorPago1  );

           p_ValorPagoLoc1  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc1  );
		END IF;

		IF p_ValorPago2 <> 0 AND p_Moneda <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_Moneda            ,
                                p_ValorPago2      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc2   );
		ELSE
		   p_ValorPagoLoc2 := p_ValorPago2;
		END IF;

		IF p_ValorPago2 <> 0 THEN
           p_ValorPago2  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_Moneda   ,
                                                        dte_Reference    ,
                                                        p_ValorPago2  );

           p_ValorPagoLoc2  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc2  );
		END IF;

---------------
		IF p_ValorPago3 <> 0 AND p_CCS.MonedaPago <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_CCS.MonedaPago            ,
                                p_ValorPago3      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc3   );
		ELSE
		   p_ValorPagoLoc3 := p_ValorPago3;
		END IF;

		IF p_ValorPago3 <> 0 THEN
           p_ValorPago3  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_CCS.MonedaPago   ,
                                                        dte_Reference    ,
                                                        p_ValorPago3  );

           p_ValorPagoLoc3  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc3  );
		END IF;

		IF p_ValorPago4 <> 0 AND p_Moneda <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_Moneda            ,
                                p_ValorPago4      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc4   );
		ELSE
		   p_ValorPagoLoc4 := p_ValorPago4;
		END IF;

		IF p_ValorPago4 <> 0 THEN
           p_ValorPago4  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_Moneda   ,
                                                        dte_Reference    ,
                                                        p_ValorPago4  );

           p_ValorPagoLoc4  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc4  );
		END IF;

----------------------
		IF p_ValorPago5 <> 0 AND p_CCS.MonedaPago <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_CCS.MonedaPago            ,
                                p_ValorPago5      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc5   );
		ELSE
		   p_ValorPagoLoc5 := p_ValorPago5;
		END IF;

		IF p_ValorPago5 <> 0 THEN
           p_ValorPago5  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_CCS.MonedaPago   ,
                                                        dte_Reference    ,
                                                        p_ValorPago5  );

           p_ValorPagoLoc5  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc5  );
		END IF;

		IF p_ValorPago6 <> 0 AND p_Moneda <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_Moneda            ,
                                p_ValorPago6      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc6   );
		ELSE
		   p_ValorPagoLoc6 := p_ValorPago6;
		END IF;

		IF p_ValorPago6 <> 0 THEN
           p_ValorPago6  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_Moneda   ,
                                                        dte_Reference    ,
                                                        p_ValorPago6  );

           p_ValorPagoLoc6  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc6  );
		END IF;

---------------------
		IF p_ValorPago7 <> 0 AND p_CCS.MonedaPago <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_CCS.MonedaPago            ,
                                p_ValorPago7      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc7   );
		ELSE
		   p_ValorPagoLoc7 := p_ValorPago7;
		END IF;

		IF p_ValorPago7 <> 0 THEN
           p_ValorPago7  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_CCS.MonedaPago   ,
                                                        dte_Reference    ,
                                                        p_ValorPago7  );

           p_ValorPagoLoc7  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc7  );
		END IF;

		IF p_ValorPago8 <> 0 AND p_Moneda <> 172.4 THEN
           PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Reference             ,
                                p_Moneda            ,
                                p_ValorPago8      ,
                                num_LocalCurr           ,
                                p_ValorPagoLoc8   );
		ELSE
		   p_ValorPagoLoc8 := p_ValorPago8;
		END IF;

		IF p_ValorPago8 <> 0 THEN
           p_ValorPago8  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        p_Moneda   ,
                                                        dte_Reference    ,
                                                        p_ValorPago8  );

           p_ValorPagoLoc8  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Reference    ,
                                                        p_ValorPagoLoc8  );
		END IF;


--		IF p_ValorPago1 <> p_FinPago1  THEN
		   BEGIN
		   UPDATE DEVENG.T_PGT_SWFINANCST_S H SET
		      H.INTERESTCASH       = p_ValorPago1,
			  H.INTERESTCASHL      = p_ValorPagoLoc1,
			  H.PRINCIPALCASH  	   = p_ValorPago3,
  			  H.PRINCIPALCASHL     = p_ValorPagoLoc3,
			  H.INTERESTREADJCASH  = p_ValorPago5,
			  H.PRINCIPALREADJCASH = p_ValorPago7
		   WHERE H.DEAL_ID = p_CCS.PkHeader
		   AND	 H.DDATE = dte_Reference
		   AND   H.FK_DIRECTION = p_Direccion1;
           EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO Currency Change1');
		   END;
	--	END IF;


--		IF p_ValorPago2 <> p_FinPago2  THEN
		   BEGIN
		   UPDATE DEVENG.T_PGT_SWFINANCST_S H SET
		      H.INTERESTCASH  = p_ValorPago2,
			  H.INTERESTCASHL = p_ValorPagoLoc2,
			  H.PRINCIPALCASH  	   = p_ValorPago4,
  			  H.PRINCIPALCASHL     = p_ValorPagoLoc4,
			  H.INTERESTREADJCASH  = p_ValorPago6,
			  H.PRINCIPALREADJCASH = p_ValorPago8
		   WHERE H.DEAL_ID = p_CCS.PkHeader
		   AND	 H.DDATE = dte_Reference
		   AND   H.FK_DIRECTION = p_Direccion2;
           EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO Currency Change2');
		   END;
--		END IF;

	    FETCH C_CCS INTO p_CCS;
	  END LOOP;

	  CLOSE C_CCS;*/
	END IF;

END	p_Act_FinCurrencyChange;

PROCEDURE p_ValorMercado ( num_Branch        IN     NUMBER,
		  				   num_FechaProceso  IN 	DATE,
						   num_MonedaCCY	 IN 	NUMBER,
						   num_MonedaLocal	 IN     NUMBER,
						   num_FxRate		 IN OUT NUMBER)
IS
  p_mercado NUMBER;
BEGIN
  IF num_MonedaCCY <> num_MonedaLocal THEN
     p_mercado := chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_MonedaCCY),122.4,TO_CHAR(num_FechaProceso,'DD/MM/YYYY'),1);
  ELSE
     p_mercado := -1;
  END IF;

  IF p_mercado <> -1 THEN
     num_FxRate := p_mercado;
  END IF;

END	p_ValorMercado;

PROCEDURE P_GETMTMVALUE_LOC ( num_Event       			IN  NUMBER,
                                dte_Proceso     	IN  DATE,
                                num_Branch      	IN  NUMBER,
                                num_Instrument  	IN  NUMBER,
                                num_Currency    	IN  NUMBER,
                                num_Folder      	IN  NUMBER,
                                num_LocalCurr   	IN  NUMBER,
                                num_Properties  	IN  NUMBER,
                                num_MTMValue   		IN  NUMBER,
								num_MTMValueLoc		IN	NUMBER,
                                num_TRevOpFin   	IN  NUMBER,
                                num_CRevOpFin   	IN  NUMBER,
                                num_TDevOpFin   	IN  NUMBER,
                                num_CDevOpFin   	IN  NUMBER,
                                num_AValRevBenLoc  	OUT NUMBER,
                                num_AValRevBenCCY  	OUT NUMBER,
                                num_AValRevPerLoc  	OUT NUMBER,
                                num_AValRevPerCCY  	OUT NUMBER,
                                num_AValDevBenLoc  	OUT NUMBER,
                                num_AValDevBenCCY  	OUT NUMBER,
                                num_AValDevPerLoc  	OUT NUMBER,
                                num_AValDevPerCCY  	OUT NUMBER,
                                num_ARevBenLoc  	OUT NUMBER,
                                num_ARevPerLoc  	OUT NUMBER,
                                num_ADevBenLoc  	OUT NUMBER,
                                num_ADevPerLoc  	OUT NUMBER)
IS
        /* ******************************************************************** */
        /* Variables                                                                                                                    */
        /* ******************************************************************** */
        num_Key                 NUMBER;
        num_Reg                 NUMBER;
        num_Quantity            NUMBER;
        num_TableMIS            NUMBER;
		dte_ValueDate			DATE;
        num_PKCalendar          NUMBER;
        num_DateConv            NUMBER;
    	num_Error               NUMBER;
		num_Status           	NUMBER;
        /* ******************************************************************** */
        /* Variables para saldos contables                                                                              */
        /* ******************************************************************** */
        num_PosRevOpFinCCYCr     NUMBER  ;
        num_PosRevOpFinCCYDb     NUMBER  ;
        num_PosRevOpFinCCY       NUMBER  ;
        num_PosRevOpFinLocCr     NUMBER  ;
        num_PosRevOpFinLocDb     NUMBER  ;
        num_PosRevOpFinLoc       NUMBER  ;
        num_PosDevOpFinCCYCr     NUMBER  ;
        num_PosDevOpFinCCYDb     NUMBER  ;
        num_PosDevOpFinCCY       NUMBER  ;
        num_PosDevOpFinLocCr     NUMBER  ;
        num_PosDevOpFinLocDb     NUMBER  ;
        num_PosDevOpFinLoc       NUMBER  ;
        num_DMTMValueCCY         NUMBER  ;
        num_DMTMValueLoc         NUMBER  ;
        num_DRevMTML             NUMBER  ;
        num_DDevMTML             NUMBER  ;
        /* ******************************************************************** */
        /* Variables auxiliares                                                                                                 */
        /* ******************************************************************** */
        num_AuxDevLoc           NUMBER  ;
        num_AuxRevLoc           NUMBER  ;
BEGIN
        /* ******************************************************************** */
        /* Terminamos si la ejecución no esta permitida ...                     */
        /* ******************************************************************** */
        IF PGT_PRG.Pkg_Acctgeneral.EXECUTE_PROCESS = 0 THEN
                RETURN;
        END IF;

        /* ******************************************************************** */
        /* ******************************************************************** */
        /* OBTENEMOS TODAS NUESTRAS CUENTAS PARA REALIZAR LOS MOVIMIENTOS ...   */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* Buscamos la llave de la cuenta Revaluación de ope. financieras ...   */
        /* ******************************************************************** */
        num_Key := PGT_PRG.Pkg_Acct_Trg.f_GetAcctKey (  num_CRevOpFin           ,
                                                num_Branch              ,
                                                num_Currency            ,
                                                num_Instrument          ,
                                                NULL                    ,
                                                NULL                    ,
                                                NULL                    ,
                                                num_Folder              ,
                                                NULL                    ,
                                                num_Event               ,
                                                num_Properties          ,
                                                num_TRevOpFin           );
        /* ******************************************************************** */
        /* Buscamos la posición de la cuenta Revaluación de ope. financieras    */
        /* ******************************************************************** */
        PGT_PRG.Pkg_Acct_Trg.p_Get_GLTA_Bal (           dte_Proceso             ,
                                                num_Key                 ,
                                                dte_Proceso             ,
                                                num_PosRevOpFinCCYCr    ,
                                                num_PosRevOpFinCCYDb    ,
                                                num_PosRevOpFinLocCr    ,
                                                num_PosRevOpFinLocDb    ,
                                                num_Quantity            ,
                                                num_Reg                 );
        /* ******************************************************************** */
        /* Obtenemos los saldos de nuestra cuenta de la Opcion...               */
        /* ******************************************************************** */
        num_PosRevOpFinCCY      := num_PosRevOpFinCCYDb - num_PosRevOpFinCCYCr;
        num_PosRevOpFinLoc      := num_PosRevOpFinLocDb - num_PosRevOpFinLocCr;
        /* ******************************************************************** */
        /* Buscamos la llave de la cuenta Devaluación de ope. financieras ...   */
        /* ******************************************************************** */
        num_Key := PGT_PRG.Pkg_Acct_Trg.f_GetAcctKey (  num_CDevOpFin           ,
                                                num_Branch              ,
                                                num_Currency            ,
                                                num_Instrument          ,
                                                NULL                    ,
                                                NULL                    ,
                                                NULL                    ,
                                                num_Folder              ,
                                                NULL                    ,
                                                num_Event               ,
                                                num_Properties          ,
                                                num_TDevOpFin           );
        /* ******************************************************************** */
        /* Buscamos la posición de la cuenta Devaluación de ope. financieras    */
        /* ******************************************************************** */
        PGT_PRG.Pkg_Acct_Trg.p_Get_GLTA_Bal (           dte_Proceso             ,
                                                num_Key                 ,
                                                dte_Proceso             ,
                                                num_PosDevOpFinCCYCr    ,
                                                num_PosDevOpFinCCYDb    ,
                                                num_PosDevOpFinLocCr    ,
                                                num_PosDevOpFinLocDb    ,
                                                num_Quantity            ,
                                                num_Reg                 );
        /* ******************************************************************** */
        /* Obtenemos los saldos de nuestra cuenta de la Opcion...               */
        /* ******************************************************************** */
        num_PosDevOpFinCCY      := num_PosDevOpFinCCYDb - num_PosDevOpFinCCYCr;
        num_PosDevOpFinLoc      := num_PosDevOpFinLocDb - num_PosDevOpFinLocCr;
        /* ******************************************************************** */
        /*      MOVIMIENTO ...                                                  */
        /* ******************************************************************** */
        /* Mark to Market  ...                                                  */
        /* Las cuentas afectadas en el movimiento son:                          */
        /*      - Revaluación de operaciones financieras                        */
        /*      - Devaluación de operaciones financieras                        */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* Calculamos el ajuste de MTM en Divisa.                               */
		/* Si el Valor de Mercado es Positivo comprobamos si hay saldo en la	*/
		/* cuenta de Devaluación para anularlo en caso afirmativo. El apunte	*/
		/* que haremos a Revaluación será por el Valor Mdo - Sdo Revaluación	*/
        /* ******************************************************************** */
        IF num_MTMValue > 0 THEN
			-- DEVALUACION
			IF num_PosDevOpFinCCY < 0 THEN
				num_AValDevBenCCY	:= ABS(num_PosDevOpFinCCY);

	            num_AValDevBenCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValDevBenCCY  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinCCY      := num_PosDevOpFinCCY + num_AValDevBenCCY;
			ELSIF num_PosDevOpFinCCY > 0 THEN
				num_AValDevPerCCY	:= num_PosDevOpFinCCY;
	            num_AValDevPerCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValDevPerCCY  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinCCY      := num_PosDevOpFinCCY - num_AValDevPerCCY;
			ELSE
				num_AValDevBenCCY	:= 0;
				num_AValDevPerCCY	:= 0;
			END IF;

			-- REVALUACION
			num_DMTMValueCCY	:= num_MTMValue - num_PosRevOpFinCCY;
			IF num_DMTMValueCCY > 0 THEN
				num_AValRevBenCCY  := num_DMTMValueCCY;
	            num_AValRevBenCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValRevBenCCY  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Revaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosRevOpFinCCY      := num_PosRevOpFinCCY + num_AValRevBenCCY;

			ELSE
				num_AValRevPerCCY  := ABS(num_DMTMValueCCY);
	            num_AValRevPerCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValRevPerCCY  );
                num_PosRevOpFinCCY      := num_PosRevOpFinCCY - num_AValRevPerCCY;
   			END IF;
		END IF;

        /* ******************************************************************** */
        /* Calculamos el ajuste de MTM en moneda Local (Valor de Mercado > 0)   */
        /* ******************************************************************** */
        IF num_MTMValueLoc > 0 THEN
			-- DEVALUACION
			IF num_PosDevOpFinLoc < 0 THEN
				num_AValDevBenLoc	:= ABS(num_PosDevOpFinLoc);

	            num_AValDevBenLoc  	:=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValDevBenLoc  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinLoc      := num_PosDevOpFinLoc + num_AValDevBenLoc;

			ELSIF num_PosDevOpFinLoc > 0 THEN
				num_AValDevPerLoc	:= num_PosDevOpFinCCY;
	            num_AValDevPerLoc  	:=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValDevPerLoc  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinLoc      := num_PosDevOpFinLoc - num_AValDevPerLoc;

			ELSE
				num_AValDevBenLoc	:= 0;
				num_AValDevPerLoc	:= 0;
			END IF;

			-- REVALUACION
	        IF num_Currency <> num_LocalCurr THEN
--                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
--                                num_Branch              ,
--                                dte_Proceso             ,
--                                num_Currency            ,
--                                num_DMTMValueCCY        ,
--                                num_LocalCurr           ,
--                                num_DMTMValueLoc        );

                IF chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),122.4,TO_CHAR(dte_Proceso,'DD/MM/YYYY'),1) <> -1 AND num_Branch = 20010.4 THEN
				   num_DMTMValueLoc := chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),122.4,TO_CHAR(dte_Proceso,'DD/MM/YYYY'),num_DMTMValueCCY);
				ELSE
                   PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Proceso             ,
                                num_Currency            ,
                                num_DMTMValueCCY        ,
                                num_LocalCurr           ,
                                num_DMTMValueLoc        );
				END IF;

			ELSE
                num_DMTMValueLoc        := num_DMTMValueCCY;
	        END IF;

			IF num_DMTMValueLoc > 0 THEN
				num_AValRevBenLoc  := num_DMTMValueLoc;
	            num_AValRevBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValRevBenLoc  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Revaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosRevOpFinLoc      := num_PosRevOpFinLoc + num_AValRevBenLoc;

			ELSE
				num_AValRevPerLoc  := ABS(num_DMTMValueLoc);
	            num_AValRevPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValRevPerLoc  );
                num_PosRevOpFinLoc      := num_PosRevOpFinLoc - num_AValRevPerLoc;
   			END IF;
		END IF;

        /* ******************************************************************** */
        /* Calculamos el ajuste de MTM en Divisa.                               */
		/* Si el Valor de Mercado es Negativo comprobamos si hay saldo en la	*/
		/* cuenta de Revaluación para anularlo en caso afirmativo. El apunte	*/
		/* que haremos a Devaluación será por el Valor Mdo - Sdo Devaluación	*/
        /* ******************************************************************** */
        IF num_MTMValue < 0 THEN
			-- REVALUACION
			IF num_PosRevOpFinCCY < 0 THEN
				num_AValRevBenCCY	:= ABS(num_PosRevOpFinCCY);

	            num_AValRevBenCCY  	:=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValRevBenCCY  );
				num_PosRevOpFinCCY	:= num_PosRevOpFinCCY + num_AValRevBenCCY;
			ELSIF num_PosRevOpFinCCY > 0 THEN
				num_AValRevPerCCY	:= num_PosRevOpFinCCY;
	            num_AValRevPerCCY  	:=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValRevPerCCY  );
				num_PosRevOpFinCCY	:= num_PosRevOpFinCCY - num_AValRevPerCCY;
			ELSE
				num_AValRevBenCCY	:= 0;
				num_AValRevPerCCY	:= 0;
			END IF;

			-- DEVALUACION
			num_DMTMValueCCY	:= num_MTMValue - num_PosDevOpFinCCY;
			IF num_DMTMValueCCY > 0 THEN
				num_AValDevBenCCY  := num_DMTMValueCCY;
	            num_AValDevBenCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValDevBenCCY  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Revaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinCCY      := num_PosDevOpFinCCY + num_AValDevBenCCY;

			ELSE
				num_AValDevPerCCY  := ABS(num_DMTMValueCCY);
	            num_AValDevPerCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValDevPerCCY  );
                num_PosDevOpFinCCY      := num_PosDevOpFinCCY - num_AValDevPerCCY;
   			END IF;
		END IF;

        /* ******************************************************************** */
        /* Calculamos el ajuste de MTM en moneda Local (Valor de Mercado < 0)   */
        /* ******************************************************************** */
        IF num_MTMValueLoc < 0 THEN
			-- REVALUACION
			IF num_PosRevOpFinLoc < 0 THEN
				num_AValRevBenLoc	:= ABS(num_PosRevOpFinLoc);

	            num_AValRevBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValRevBenLoc  );
				num_PosRevOpFinLoc	:= num_PosRevOpFinLoc + num_AValRevBenLoc;
			ELSIF num_PosRevOpFinLoc > 0 THEN
				num_AValRevPerLoc	:= num_PosRevOpFinLoc;
	            num_AValRevPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValRevPerLoc  );
				num_PosRevOpFinLoc	:= num_PosRevOpFinLoc - num_AValRevPerLoc;
			ELSE
				num_AValRevBenLoc	:= 0;
				num_AValRevPerLoc	:= 0;
			END IF;

			-- DEVALUACION
	        IF num_Currency <> num_LocalCurr THEN
--                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
--                                num_Branch              ,
--                                dte_Proceso             ,
--                                num_Currency            ,
--                                num_DMTMValueCCY        ,
--                                num_LocalCurr           ,
--                                num_DMTMValueLoc        );

                IF chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),122.4,TO_CHAR(dte_Proceso,'DD/MM/YYYY'),1) <> -1 AND num_Branch = 20010.4 THEN
				   num_DMTMValueLoc := chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),122.4,TO_CHAR(dte_Proceso,'DD/MM/YYYY'),num_DMTMValueCCY);
                ELSE
                   PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Proceso             ,
                                num_Currency            ,
                                num_DMTMValueCCY        ,
                                num_LocalCurr           ,
                                num_DMTMValueLoc        );
				END IF;
			ELSE
                num_DMTMValueLoc        := num_DMTMValueCCY;
	        END IF;

			IF num_DMTMValueLoc > 0 THEN
				num_AValDevBenLoc  := num_DMTMValueLoc;
	            num_AValDevBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValDevBenLoc  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Revaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinLoc      := num_PosDevOpFinLoc + num_AValDevBenLoc;

			ELSE
				num_AValDevPerLoc  := ABS(num_DMTMValueLoc);
	            num_AValDevPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValDevPerLoc  );
                num_PosDevOpFinLoc      := num_PosDevOpFinLoc - num_AValDevPerLoc;
   			END IF;
		END IF;

        IF num_MTMValue = 0 THEN
			-- ANULO DEVALUACION
			IF num_PosDevOpFinCCY < 0 THEN
				num_AValDevBenCCY	:= ABS(num_PosDevOpFinCCY);

	            num_AValDevBenCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValDevBenCCY  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinCCY      := num_PosDevOpFinCCY + num_AValDevBenCCY;
			ELSIF num_PosDevOpFinCCY > 0 THEN
				num_AValDevPerCCY	:= num_PosDevOpFinCCY;
	            num_AValDevPerCCY  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValDevPerCCY  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinCCY      := num_PosDevOpFinCCY - num_AValDevPerCCY;
			ELSE
				num_AValDevBenCCY	:= 0;
				num_AValDevPerCCY	:= 0;
			END IF;

			-- ANULO REVALUACION
			IF num_PosRevOpFinCCY < 0 THEN
				num_AValRevBenCCY	:= ABS(num_PosRevOpFinCCY);

	            num_AValRevBenCCY  	:=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValRevBenCCY  );
				num_PosRevOpFinCCY	:= num_PosRevOpFinCCY + num_AValRevBenCCY;
			ELSIF num_PosRevOpFinCCY > 0 THEN
				num_AValRevPerCCY	:= num_PosRevOpFinCCY;
	            num_AValRevPerCCY  	:=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_Currency   ,
                                                        dte_Proceso     ,
                                                        num_AValRevPerCCY  );
				num_PosRevOpFinCCY	:= num_PosRevOpFinCCY - num_AValRevPerCCY;
			ELSE
				num_AValRevBenCCY	:= 0;
				num_AValRevPerCCY	:= 0;
			END IF;

		END IF;

        IF num_MTMValueLoc = 0 THEN
			-- ANULO DEVALUACION
			IF num_PosDevOpFinLoc < 0 THEN
				num_AValDevBenLoc	:= ABS(num_PosDevOpFinLoc);

	            num_AValDevBenLoc  	:=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValDevBenLoc  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinLoc      := num_PosDevOpFinLoc + num_AValDevBenLoc;

			ELSIF num_PosDevOpFinLoc > 0 THEN
				num_AValDevPerLoc	:= num_PosDevOpFinCCY;
	            num_AValDevPerLoc  	:=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValDevPerLoc  );
                /* ************************************************************ */
                /* Actualizamos las cuentas afectadas en divisa ...             */
                /*      - Devaluación de operaciones financieras                */
                /* ************************************************************ */
                num_PosDevOpFinLoc      := num_PosDevOpFinLoc - num_AValDevPerLoc;

			ELSE
				num_AValDevBenLoc	:= 0;
				num_AValDevPerLoc	:= 0;
			END IF;

			-- ANULO REVALUACION
			IF num_PosRevOpFinLoc < 0 THEN
				num_AValRevBenLoc	:= ABS(num_PosRevOpFinLoc);

	            num_AValRevBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValRevBenLoc  );
				num_PosRevOpFinLoc	:= num_PosRevOpFinLoc + num_AValRevBenLoc;
			ELSIF num_PosRevOpFinLoc > 0 THEN
				num_AValRevPerLoc	:= num_PosRevOpFinLoc;
	            num_AValRevPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_AValRevPerLoc  );
				num_PosRevOpFinLoc	:= num_PosRevOpFinLoc - num_AValRevPerLoc;
			ELSE
				num_AValRevBenLoc	:= 0;
				num_AValRevPerLoc	:= 0;
			END IF;

		END IF;

        /* ******************************************************************** */
        /*      MOVIMIENTO ...                                                                                                          */
        /* ******************************************************************** */
        /* Revaluación por tipo de cambio de Mark to Market  ...                */
        /* Las cuentas afectadas en el movimiento son:                          */
        /*      - Revaluación de operaciones financieras                        */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* Obtenemos el importe local buscando su contravalor                   */
        /* ******************************************************************** */

        IF num_Currency <> num_LocalCurr THEN
--                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
--                                num_Branch              ,
--                                dte_Proceso             ,
--                                num_Currency            ,
--                                num_PosRevOpFinCCY      ,
--                                num_LocalCurr           ,
--                                num_AuxRevLoc   );

                IF chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),122.4,TO_CHAR(dte_Proceso,'DD/MM/YYYY'),1) <> -1 AND num_Branch = 20010.4 THEN
				   num_AuxRevLoc := chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),122.4,TO_CHAR(dte_Proceso,'DD/MM/YYYY'),num_PosRevOpFinCCY);
                ELSE
                   PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Proceso             ,
                                num_Currency            ,
                                num_PosRevOpFinCCY      ,
                                num_LocalCurr           ,
                                num_AuxRevLoc   );								   
				END IF;
        ELSE
                num_AuxRevLoc   := num_PosRevOpFinCCY;
        END IF;

        /* ******************************************************************** */
        /* num_PosRevOpFinLoc : Saldo local la revaluación MTM                  */
        /* ******************************************************************** */
        num_DRevMTML    := num_AuxRevLoc - num_PosRevOpFinLoc ;
        /* ******************************************************************** */
        /* Calculamos el sentido de la revaluación de MTM                       */
        /* ******************************************************************** */
        IF num_DRevMTML > 0 THEN
                num_ARevBenLoc  := num_DRevMTML;
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_ARevBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_ARevBenLoc  );
                num_ARevPerLoc  := 0;
        ELSE
                num_ARevBenLoc  := 0;
                num_ARevPerLoc  := ABS (num_DRevMTML);
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_ARevPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_ARevPerLoc  );
        END IF;
        /* ******************************************************************** */
        /*      MOVIMIENTO ...                                                  */
        /* ******************************************************************** */
        /* Revaluación por tipo de cambio de Mark to Market  ...                */
        /* Las cuentas afectadas en el movimiento son:                          */
        /*      - Devaluación de operaciones financieras                        */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* ******************************************************************** */
        /* Obtenemos el importe local buscando su contravalor                   */
        /* ******************************************************************** */
        IF num_Currency <> num_LocalCurr THEN
--                PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
--                                num_Branch              ,
--                                dte_Proceso             ,
--                                num_Currency            ,
--                                num_PosDevOpFinCCY      ,
--                                num_LocalCurr           ,
--                                num_AuxDevLoc   );

                IF chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),122.4,TO_CHAR(dte_Proceso,'DD/MM/YYYY'),1) <> -1 AND num_Branch = 20010.4 THEN
				   num_AuxDevLoc := chi_con.F_Conv_A_Src('172.4',TO_CHAR(num_Currency),122.4,TO_CHAR(dte_Proceso,'DD/MM/YYYY'),num_PosDevOpFinCCY);
                ELSE
                   PGT_PRG.Pkg_Acctgeneral.p_GetCtrValue (
                                num_Branch              ,
                                dte_Proceso             ,
                                num_Currency            ,
                                num_PosDevOpFinCCY      ,
                                num_LocalCurr           ,
                                num_AuxDevLoc   );
				END IF;
        ELSE
                num_AuxDevLoc   := num_PosDevOpFinCCY;
        END IF;
        /* ******************************************************************** */
        /* num_PosDevOpFinLoc : Saldo local la devaluación MTM                  */
        /* ******************************************************************** */
        num_DDevMTML    := num_AuxDevLoc - num_PosDevOpFinLoc ;
        /* ******************************************************************** */
        /* Calculamos el sentido de la devaluación de MTM                       */
        /* ******************************************************************** */
        IF num_DDevMTML > 0 THEN
                num_ADevPerLoc  := 0;
                num_ADevBenLoc  := ABS (num_DDevMTML);
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_ADevBenLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_ADevBenLoc  );
        ELSE
                num_ADevPerLoc  := ABS (num_DDevMTML);
                /* ************************************************************ */
                /* Actualizamos el valor teniendo en cuenta los decimales para  */
                /* la divisa                                                    */
                /* ************************************************************ */
                num_ADevPerLoc  :=  PGT_PRG.Pkg_Pgtgeneral.f_TreatCurrencyAmount (
                                                        num_LocalCurr   ,
                                                        dte_Proceso     ,
                                                        num_ADevPerLoc  );
                num_ADevBenLoc  := 0;
        END IF;
END p_GetMTMValue_LOC;

PROCEDURE p_ActFinAnt ( dte_Reference   IN  DATE,
                        num_Branch      IN  NUMBER,
                        num_Instrument  IN  NUMBER)
IS 

CURSOR C_DealId IS
SELECT H.DEAL_ID, H.FK_DIRECTION, H.INTERESTCASH, H.CANCELLATIONCASH, H.PRINCIPALCASH,
       H.FK_BRANCH
FROM DEVENG.T_PGT_SWFINANCST_S H
WHERE H.FK_CURRENCY = 20233.4 -- CLF 
AND   (H.INTERESTCASH <> 0 OR H.CANCELLATIONCASH <> 0 OR H.PRINCIPALCASH <> 0)
AND   H.DDATE       = dte_Reference
--AND   H.FK_BRANCH   = num_Branch
AND   CHI_CON.F_CCSUFCLP(H.DEAL_ID) = 1 -- CLP/CLF 
AND   EXISTS (SELECT 1
	          FROM PGT_TRD.T_PGT_TRADE_EVENTS_S  A,
	   		       PGT_TRD.T_PGT_TRADE_HEADER_S B,
				   PGT_TRD.T_PGT_TRADE_EVENTS_S C,
				   PGT_TRD.T_PGT_CCS_CANCEL_S   D
	   		  WHERE A.PK = B.FK_PARENT
			  --AND   A.ACCTPROC    IS NOT NULL
	   		  --AND   A.FK_PORTPROP IS NOT NULL
	   		  --AND   A.FK_BRANCH     = num_Branch
	   		  AND   A.FK_STATUS     <> 186.4
	   		  AND   A.FK_REVUSER  IS NULL
	   		  AND   A.FK_INSTRUMENT = 20.4
			  AND   A.PK = B.FK_PARENT
	   		  AND   B.PK = H.DEAL_ID
	   		  AND   A.PK = C.FK_SOURCEEVENT
	   		  --AND   C.ACCTPROC    IS NOT NULL
	   		  AND   C.FK_INSTRUMENT = 20.4
	   		  AND   C.FK_STATUS     in (96.4,22832.4)
	   		  AND   C.PK            = D.FK_PARENT
	   		  --AND   D.ACCTSPOTGEN IS NOT NULL
	   		  AND   D.VALUEDATE     >= dte_Reference
	   		  AND   D.INTSETTDATE   <= dte_Reference
	   		  AND   CHI_CON.F_CCSUFCLP(B.PK) = 1);


p_DealId C_DealId%ROWTYPE;
p_Dir2    NUMBER;
UFAMOUNT  NUMBER;
p_Interes NUMBER;
p_Nominal NUMBER;
p_Cancel  NUMBER;

BEGIN
   IF num_Instrument = 20.4  THEN
	  
      OPEN C_DealId;
	  FETCH C_DealId INTO p_DealId;
	  WHILE C_DealId%FOUND LOOP
	      p_Dir2    := 0;
	      p_Interes := 0;
		  p_Nominal := 0;
		  p_Cancel	:= 0;
		  
		  IF p_DealId.FK_DIRECTION = 183.4 THEN
		     p_Dir2 := 184.4;
		  ELSE
		     p_Dir2 := 183.4;
		  END IF;	 
	  
	      SELECT CHI_CON.F_VALOR_UF(dte_Reference)
		  INTO UFAMOUNT
		  FROM DUAL;
		  
		  p_Interes := ROUND(p_DealId.INTERESTCASH     * UFAMOUNT);
		  p_Nominal := ROUND(p_DealId.PRINCIPALCASH    * UFAMOUNT);
		  p_Cancel  := ROUND(p_DealId.CANCELLATIONCASH * UFAMOUNT);
		  
	      BEGIN
      	  UPDATE DEVENG.T_PGT_SWFINANCST_S G SET
             G.INTERESTCASH      = G.INTERESTCASH      + p_Interes,
			 G.INTERESTCASHL     = G.INTERESTCASHL     + p_Interes,
			 G.CANCELLATIONCASH  = G.CANCELLATIONCASH  + p_Cancel,
			 G.CANCELLATIONCASHL = G.CANCELLATIONCASHL + p_Cancel,
			 G.PRINCIPALCASH	 = G.PRINCIPALCASH     + p_Nominal,
			 G.PRINCIPALCASHL	 = G.PRINCIPALCASHL    + p_Nominal
          WHERE G.ddate        = dte_Reference
	      AND   G.FK_BRANCH    = p_DealId.FK_BRANCH
	      AND   G.DEAL_ID      = p_DealId.DEAL_ID
		  AND   G.FK_DIRECTION = p_Dir2
		  AND   G.FK_CURRENCY  = 172.4;
          EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO: ' || TO_CHAR(p_DealId.Deal_ID));
	      END;
		  
	      BEGIN
      	  UPDATE DEVENG.T_PGT_SWFINANCBA_S E SET
             E.INTERESTCASH      = E.INTERESTCASH      + p_Interes,
			 E.CANCELLATIONCASH  = E.CANCELLATIONCASH  + p_Cancel,
			 E.PRINCIPALCASH	 = E.PRINCIPALCASH     + p_Nominal
          WHERE E.ddate        = dte_Reference
	      AND   E.FK_BRANCH    = p_DealId.FK_BRANCH
	      AND   E.DEAL_ID      = p_DealId.DEAL_ID
		  AND   E.FK_DIRECTION = p_Dir2
		  AND   E.FK_CURRENCY  = 172.4;
          EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO: ' || TO_CHAR(p_DealId.Deal_ID));
	      END;		   
		  

	      BEGIN
      	  UPDATE DEVENG.T_PGT_SWFINANCST_S G SET
             G.INTERESTCASH      = 0,
			 G.INTERESTCASHL     = 0,
			 G.CANCELLATIONCASH  = 0,
			 G.CANCELLATIONCASHL = 0,
			 G.PRINCIPALCASH	 = 0,
			 G.PRINCIPALCASHL	 = 0
          WHERE G.ddate        = dte_Reference
	      AND   G.FK_BRANCH    = p_DealId.FK_BRANCH
	      AND   G.DEAL_ID      = p_DealId.DEAL_ID
		  AND   G.FK_DIRECTION = p_DealId.FK_DIRECTION
		  AND   G.FK_CURRENCY  = 20233.4;
          EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO: ' || TO_CHAR(p_DealId.Deal_ID));
	      END;
		  
	      BEGIN
      	  UPDATE DEVENG.T_PGT_SWFINANCBA_S E SET
             E.INTERESTCASH      = 0,
			 E.CANCELLATIONCASH  = 0,
			 E.PRINCIPALCASH	 = 0
          WHERE E.ddate        = dte_Reference
	      AND   E.FK_BRANCH    = p_DealId.FK_BRANCH
	      AND   E.DEAL_ID      = p_DealId.DEAL_ID
		  AND   E.FK_DIRECTION = p_DealId.FK_DIRECTION
		  AND   E.FK_CURRENCY  = 20233.4;
          EXCEPTION WHEN OTHERS THEN
	          RAISE_APPLICATION_ERROR(-20001,  'NO SE PUDO ACTUALIZAR EL FINANCIERO: ' || TO_CHAR(p_DealId.Deal_ID));
	      END;		  

	      FETCH C_DealId INTO p_DealId;
	  END LOOP;
   END IF;
END p_ActFinAnt;						


END Pkg_Acct_Dolar_Mercado;
/


GRANT DEBUG ON CHI_CON.PKG_ACCT_DOLAR_MERCADO TO CHICON_ALL;

GRANT DEBUG ON CHI_CON.PKG_ACCT_DOLAR_MERCADO TO CHICON_SELECT;

GRANT EXECUTE, DEBUG ON CHI_CON.PKG_ACCT_DOLAR_MERCADO TO PUBLIC;

