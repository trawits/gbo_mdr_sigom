DROP PACKAGE CHI_CON.PGK_CHI_CON_TREAT_FRONT_STRUC;

CREATE OR REPLACE PACKAGE CHI_CON.PGK_CHI_CON_TREAT_FRONT_STRUC 
IS


/*Global vars*/
 Rec_Glb_Event                   PGT_TRD.T_PGT_TRADE_EVENTS_S%ROWTYPE; -- Registro global
 Rec_Glb_TradeHeader             PGT_TRD.T_PGT_TRADE_HEADER_S%ROWTYPE; -- Registro Global.
 
/************************************************************************************************
-- Funcion/Procedimiento: sp_chi_con_treat_struct_prcs
-- Objetivo:              Procedimiento que inicia el tratamiento de cheuqueo y asociación de operaciones
                          pertenecientes a una estructura. 
-- Sistema:               SIGOM
-- Base de Datos:         MDR
-- Tablas Usadas:         PGT_TRD.T_PGT_TRADE_EVENTS_AUX_S 
--                        PGT_TRD.T_PGT_TRADE_EVENTS_S
--                        PGT_TRD.T_PGT_TRADE_HEADER_S
--                        PGT_TRD.T_PGT_STRUCTURE_S
--                        
-- Fecha:                 05-01-2018
-- Autor:                 CONNECTIS
-- Input:                 N/A                                                                                                      
-- Output:                N/A
-- Input/Output:          N/A.
-- Retorno:               N/A.
-- Observaciones:
--***********************************************************************************************/
PROCEDURE sp_chi_con_treat_struct_prcs;




END PGK_CHI_CON_TREAT_FRONT_STRUC;
/


GRANT EXECUTE ON CHI_CON.PGK_CHI_CON_TREAT_FRONT_STRUC TO CHI_CON_SEL;

GRANT EXECUTE ON CHI_CON.PGK_CHI_CON_TREAT_FRONT_STRUC TO PUBLIC;

