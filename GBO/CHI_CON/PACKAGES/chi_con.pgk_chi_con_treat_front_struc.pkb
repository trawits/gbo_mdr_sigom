DROP PACKAGE BODY CHI_CON.PGK_CHI_CON_TREAT_FRONT_STRUC;

CREATE OR REPLACE PACKAGE BODY CHI_CON.PGK_CHI_CON_TREAT_FRONT_STRUC 
AS

/************************************************************************************************
-- Funcion/Procedimiento: sp_chi_con_treat_struct_prcs
-- Objetivo:              Procedimiento que inicia el tratamiento de cheuqueo y asociación de operaciones
                          pertenecientes a una estructura. 
-- Sistema:               SIGOM
-- Base de Datos:         MDR
-- Tablas Usadas:         PGT_TRD.T_PGT_TRADE_EVENTS_AUX_S 
--                        PGT_TRD.T_PGT_TRADE_EVENTS_S
--                        PGT_TRD.T_PGT_TRADE_HEADER_S
--                        PGT_TRD.T_PGT_STRUCTURE_S
-- Fecha:                 05-01-2018
-- Autor:                 CONNECTIS
-- Input:                 N/A                                                                                                      
-- Output:                N/A
-- Input/Output:          N/A.
-- Retorno:               N/A.
-- Observaciones:
--***********************************************************************************************/
PROCEDURE sp_chi_con_treat_struct_prcs
AS


Cst_Module          CONSTANT VARCHAR2(70) := 'sp_chi_con_treat_struct_prcs';
Rec_Event           PGT_TRD.T_PGT_TRADE_EVENTS_S%ROWTYPE;
Rec_TradeHeader     PGT_TRD.T_PGT_TRADE_HEADER_S%ROWTYPE;
Rec_Structure       PGT_TRD.T_PGT_STRUCTURE_S%ROWTYPE;
boo_Header          BOOLEAN;
str_strucSubType    VARCHAR2(250);
str_numStrucSource  VARCHAR2(50); 
num_strucSubTypePK  NUMBER; 
num_StrucPK         NUMBER;
num_slqcode         NUMBER; 
str_MsgError        VARCHAR2(250);
str_comentario      VARCHAR2(250);
 
 
 /*Exceptions*/
NOT_FOUND_HDR            EXCEPTION;
NOT_FOUND_STCSUBTYPE     EXCEPTION;
 

/*Constanst*/
CST_FK_EQUIVALENCETYPE          CONSTANT        NUMBER := 20310.4;
CST_SC_MUREX_LATAM3             CONSTANT        NUMBER := 513.4;    
CST_OWNER_OBJ_TRADEHEADER       CONSTANT        NUMBER := 1546.4;
CST_OWNER_OBJ_STRUCTURE         CONSTANT        NUMBER := 12120.4;
CST_EXT_OBJ_STRUCTURE           CONSTANT        NUMBER := 36877.4;
CST_OWNER_OBJ_STRUCSUBTYPE      CONSTANT        NUMBER := 13300.4;
CST_EXTENSION_EVENT_AUX         CONSTANT        NUMBER := 40648.4;
 
 
  
BEGIN
--     
    PGT_SYS.Pkg_ApplicationInfo.p_StartModule(Cst_Module,
                                             NULL,
                                             ' ======> START <======'
                                             );

      CHI_CON.PKG_LOG_S.p_inserta_log_inicio ('MX3',
                                              'CHI_CON',
                                              Cst_Module,
                                              Cst_Module);
    
    
    str_comentario := 'Debe ingresar Branch';
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                          
    
                                          
    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                         NULL,
                                         ' << PRUEBA  >>'
                                         );
                                         
    
    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                         NULL,
                                         ' << Loading Event from Dispatcher >>'
                                         );
                                                                                                                                                                   
    /*Loading Event from Dispatcher*/
    Rec_Event:= PGT_PRG.Pkg_EventDispatcher.Rec_Event;
       

     str_comentario := 'Loading Event from Dispatcher';
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                          
    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                         NULL,
                                         ' << Rec_Event.PK >> ' || Rec_Event.PK
                                         );
       
    /*Loading Header from Deal*/
    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                          NULL,
                                          ' << Loading Trade Header >>'
                                         );
                                             
    boo_Header:= PGT_PRG.Pkg_Tradeheadergeneral.f_LoadTradeHeaderByEventPK(Rec_Event.pk);

    str_comentario := 'Loading Header from Deal';
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                            
    IF boo_Header
      THEN 
         Rec_TradeHeader:= PGT_PRG.Pkg_TradeHeaderGeneral.rHeader;
         
         str_comentario := 'PGT_PRG.Pkg_TradeHeaderGeneral.rHeader';
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                           
    ELSE    
         str_comentario := 'NOT_FOUND_HDR';
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                             
     RAISE NOT_FOUND_HDR;    
    END IF;
       
           
    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                          NULL,
                                          ' << Rec_TradeHeader.PK >> ' || Rec_TradeHeader.PK);
                                              
                                              
        
    /*****************************************************Check*********************************************/  
                                             
       
    /* Capture External_ID (Reference number for structure SubType) */
    /* This values came from Murex 3 interface */
    str_numStrucSource:= PGT_PRG.Pkg_Pgtutility.f_GetAlterAliasSource(CST_FK_EQUIVALENCETYPE,       -- 20310.4   -- Deal Group
                                                                       CST_SC_MUREX_LATAM3,         -- 513.4     -- Murex 3
                                                                       Rec_Event.PK,
                                                                       CST_OWNER_OBJ_TRADEHEADER,   --1546.4
                                                                       1
                                                                       );
                                                                           
     str_comentario := 'str_numStrucSource: ' ||str_numStrucSource;
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
    
    /* If exist External_ID (Reference number for structure SubType) continue process */                                                                                                            
    IF (str_numStrucSource <> '<NOT FOUND>') 
    THEN             
        /*Capture Structure SubType from Others Info - Field57*/   
        BEGIN 
                
            SELECT  FIELD57
            INTO    str_strucSubType
            FROM    PGT_TRD.T_PGT_TRADE_EVENTS_AUX_S     
            WHERE   FK_PARENT       = Rec_Event.PK
            AND     FK_OWNER_OBJ    = CST_OWNER_OBJ_TRADEHEADER
            AND     FK_EXTENSION    = CST_EXTENSION_EVENT_AUX; -- 40648.4
                  
            str_comentario := 'str_strucSubType: ' ||str_strucSubType;
             CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                              NULL,
                                              'MX3',
                                              'CHI_CON',
                                              Cst_Module,
                                              Cst_Module,
                                              str_comentario);
        EXCEPTION 
        WHEN NO_DATA_FOUND 
          THEN
             str_MsgError:= 'NO SE CONSIGUE EL STRUCTURE SUBTYPE ASOCIADO A LA OPERACION- OPERACION CON EXTERNALID';
             RAISE;
                
        WHEN OTHERS
          THEN 
             RAISE;
        END;
                
                                       
        IF (str_strucSubType = '')
          THEN               
             RAISE NOT_FOUND_STCSUBTYPE;    
        END IF;    
                
                                           
                
        /*****************************************************Structure*****************************************/                                                 
                                                            
        /* We'll check if estructure exist */ 
        /* We get the Structure Pk */
         num_StrucPK:= PGT_PRG.Pkg_STPUtility.f_GetPkParentSource (str_numStrucSource,
                                                                   CST_SC_MUREX_LATAM3,
                                                                   CST_OWNER_OBJ_STRUCTURE 
                                                                   ); 
                                                                   
              
            str_comentario := 'num_StrucPK: ' ||num_StrucPK;
             CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                              NULL,
                                              'MX3',
                                              'CHI_CON',
                                              Cst_Module,
                                              Cst_Module,
                                              str_comentario);
                                                                                                                                                                                                                                      
        /* If not exist, will be create a new structure */
        IF num_StrucPK IS NULL 
        THEN     
                    
            /*We'll create a new Structure*/
            PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                                    NULL,
                                                    ' << Create New Structure>>'
                                                    );
                                                            
            num_strucSubTypePK:= PGT_PRG.Pkg_STPUtility.f_GetPkParentSource (str_strucSubType,
                                                                              CST_SC_MUREX_LATAM3,
                                                                              CST_OWNER_OBJ_STRUCSUBTYPE
                                                                              );                                                       
                                                                                                                                                                                                                   
             str_comentario := 'num_strucSubTypePK: ' ||num_strucSubTypePK;
             CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                              NULL,
                                              'MX3',
                                              'CHI_CON',
                                              Cst_Module,
                                              Cst_Module,
                                              str_comentario);
                                                              
            
            /* Procedure to create Strucure */
            PGT_PRG.PKG_STRUCTUREGENERAL.p_CreateStructure ( num_StrucPK,
                                                             num_strucSubTypePK,--str_strucSubType,                                                               
                                                            'AUTOMATIC ASSOCIATION' 
                                                             );
                                                             
            /* After created the structure we will update the status */    
            UPDATE  PGT_TRD.T_PGT_STRUCTURE_S
            SET     FK_STATUS       = 95.4 -- VFO
            WHERE   PK              = num_StrucPK
            AND     FK_OWNER_OBJ    = 12120.4;
                                                                     
             str_comentario := 'PGT_PRG.PKG_STRUCTUREGENERAL.p_CreateStructure.';
             CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                              NULL,
                                              'MX3',
                                              'CHI_CON',
                                              Cst_Module,
                                              Cst_Module,
                                              str_comentario);
                                                              
                                              
             
             PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                                    NULL,
                                                    ' << Finished- New PK Structure>> ' || num_StrucPK 
                                                    );                                      
        
        
            PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                                    NULL,
                                                    ' << Loading Structure RowType>>'
                                                    );
                
            PGT_PRG.PKG_STRUCTUREGENERAL.p_GetStructure (num_StrucPK, 
                                                         Rec_Structure
                                                         );     
                                                         
            str_comentario := 'PGT_PRG.PKG_STRUCTUREGENERAL.p_GetStructure.';
             CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                              NULL,
                                              'MX3',
                                              'CHI_CON',
                                              Cst_Module,
                                              Cst_Module,
                                              str_comentario);
                                                                                                  
                 
            /*****************************************************Asociate*****************************************/                                                       
                 
             /*We'll asociate the External_ID to the Estructure */
             PGT_PRG.Pkg_Pgtgeneral.p_PutEquivalency (Rec_Structure.PK,
                                                      CST_OWNER_OBJ_STRUCTURE,
                                                      CST_EXT_OBJ_STRUCTURE,
                                                      CST_SC_MUREX_LATAM3,
                                                      str_numStrucSource
                                                      );
                                                      
            str_comentario := 'PGT_PRG.Pkg_Pgtgeneral.p_PutEquivalency.';
             CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                              NULL,
                                              'MX3',
                                              'CHI_CON',
                                              Cst_Module,
                                              Cst_Module,
                                              str_comentario);
                                                                   
        END IF;
                
        PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                                    NULL,
                                                    ' <<Rec_Structure.PK >> ' || Rec_Structure.PK 
                                                    );
                                                            
         /*We'll asociate the Estructure to the Deal*/             
         UPDATE PGT_TRD.T_PGT_TRADE_HEADER_S TH
          SET   TH.FK_STRUCTURE =NVL(Rec_Structure.PK,num_StrucPK),
                TH.STRUCTIND = 1               
          WHERE TH.PK           = Rec_TradeHeader.PK
          AND   TH.FK_OWNER_OBJ = Rec_TradeHeader.FK_OWNER_OBJ;  
          
          str_comentario := 'Well asociate the Estructure to the Deal';
             CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                              NULL,
                                              'MX3',
                                              'CHI_CON',
                                              Cst_Module,
                                              Cst_Module,
                                              str_comentario);
                                                                                                                                                                                                                   
                                                                                                                                             
    END IF;
     

    --Traza
      CHI_CON.PKG_LOG_S.p_inserta_log_fin ('MX3',
                                           'CHI_CON',
                                           Cst_Module,
                                           Cst_Module);
                                                                                          
     PGT_SYS.Pkg_ApplicationInfo.p_EndModule(' ======> END <======'
                                             );
EXCEPTION
WHEN NOT_FOUND_HDR 
THEN
    str_comentario := 'NO SE CONSIGUE EL HEADER ASOCIADO A LA OPERACION';
     
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                          
    --Traza
      CHI_CON.PKG_LOG_S.p_inserta_log_fin ('MX3',
                                           'CHI_CON',
                                           Cst_Module,
                                           Cst_Module);
                                           
    str_MsgError := 'NO SE CONSIGUE EL HEADER ASOCIADO A LA OPERACION';
    num_slqcode := SQLCODE;

    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                         NULL,
                                         ' << EXCEPTION WHEN OTHERS >> ' || str_MsgError);
                                         
                                                                                         
     PGT_SYS.Pkg_ApplicationInfo.p_EndModule(' ======> END <======'
                                             );
                                                         
    CHI_CON.Pkg_Chi_Contracts.p_put_contract_exception(1,
                                                      Cst_Module,
                                                      str_MsgError,
                                                      num_slqcode);
                                                                      
    PGT_PRG.Pkg_PgtError.p_PutError(141.24,
                                   Cst_Module,
                                   1,
                                   'W', -- Error Type
                                   str_MsgError
                                   );
                                   RAISE;
                                                                                      
WHEN NOT_FOUND_STCSUBTYPE 
THEN

     str_comentario := 'NO SE CONSIGUE EL STRUCTURE SUBTYPE ASOCIADO A LA OPERACION- OPERACION CON EXTERNALID';
            CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                          
    --Traza
      CHI_CON.PKG_LOG_S.p_inserta_log_fin ('MX3',
                                           'CHI_CON',
                                           Cst_Module,
                                           Cst_Module);
                                           
    str_MsgError := 'NO SE CONSIGUE EL STRUCTURE SUBTYPE ASOCIADO A LA OPERACION- OPERACION CON EXTERNALID';
    num_slqcode := SQLCODE;

    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                         NULL,
                                         ' << EXCEPTION WHEN OTHERS >> ' || str_MsgError);
                                         
                                                                                         
     PGT_SYS.Pkg_ApplicationInfo.p_EndModule(' ======> END <======'
                                             );
                                                         
                                                                     
    PGT_PRG.Pkg_PgtError.p_PutError(141.24,
                                   Cst_Module,
                                   1,
                                   'W', -- Error Type
                                   str_MsgError
                                   );   
                                   RAISE;   
                                                                                                                                                                          
WHEN NO_DATA_FOUND 
THEN

     str_comentario := SUBSTR(SQLERRM, 1, 250);
     
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                          
    --Traza
      CHI_CON.PKG_LOG_S.p_inserta_log_fin ('MX3',
                                           'CHI_CON',
                                           Cst_Module,
                                           Cst_Module);
                                           
    str_MsgError := SUBSTR(SQLERRM, 1, 250);
    num_slqcode := SQLCODE;

    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                        NULL,
                                        ' << EXCEPTION WHEN NO DATA FOUND >> ' || str_MsgError
                                        );
                                                                                                            
                                                                                                   
     PGT_SYS.Pkg_ApplicationInfo.p_EndModule(' ======> END <======'
                                             );                      
    PGT_PRG.Pkg_PgtError.p_PutError(141.24,
                                   Cst_Module,
                                   1,
                                   'W', -- Error Type
                                   str_MsgError
                                   );
                                   
                                   RAISE;
          
WHEN OTHERS 
THEN

    str_comentario := SUBSTR(SQLERRM, 1, 250);
    
         CHI_CON.PKG_LOG_S.p_inserta_log (NULL,
                                          NULL,
                                          'MX3',
                                          'CHI_CON',
                                          Cst_Module,
                                          Cst_Module,
                                          str_comentario);
                                          
    --Traza
      CHI_CON.PKG_LOG_S.p_inserta_log_fin ('MX3',
                                           'CHI_CON',
                                           Cst_Module,
                                           Cst_Module);
                                           
    str_MsgError := SUBSTR(SQLERRM, 1, 250);
    num_slqcode := SQLCODE;

    PGT_SYS.Pkg_ApplicationInfo.p_Process(Cst_Module,
                                         NULL,
                                         ' << EXCEPTION WHEN OTHERS >> ' || str_MsgError);
                                                                                                     
     PGT_SYS.Pkg_ApplicationInfo.p_EndModule(' ======> END <======'
                                             );    
    PGT_PRG.Pkg_PgtError.p_PutError(141.24,
                                   Cst_Module,
                                   1,
                                   'E', -- Error Type
                                   str_MsgError
                                   );    

    RAISE;                                                                                           
END sp_chi_con_treat_struct_prcs;
  


END PGK_CHI_CON_TREAT_FRONT_STRUC;
/


GRANT EXECUTE ON CHI_CON.PGK_CHI_CON_TREAT_FRONT_STRUC TO CHI_CON_SEL;

GRANT EXECUTE ON CHI_CON.PGK_CHI_CON_TREAT_FRONT_STRUC TO PUBLIC;

