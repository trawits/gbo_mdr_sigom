DROP PACKAGE CHI_CON.PGK_CHI_CON_DATE_DEP;

CREATE OR REPLACE PACKAGE CHI_CON.PGK_CHI_CON_DATE_DEP 
IS


/*Global vars*/
 Rec_Glb_Event                   PGT_TRD.T_PGT_TRADE_EVENTS_S%ROWTYPE; -- Registro global
 Rec_Glb_TradeHeader             PGT_TRD.T_PGT_TRADE_HEADER_S%ROWTYPE; -- Registro Global.
 
/************************************************************************************************
-- Funcion/Procedimiento: PGK_CHI_CON_DATE_DEP
-- Objetivo:              Procedimiento para crear el registro Date Dependency
-- Sistema:               SIGOM
-- Base de Datos:         MDR
-- Tablas Usadas:         PGT_TRD.T_PGT_TRADE_EVENTS_S
--                        PGT_TRD.T_PGT_TRADE_HEADER_S
--                        
-- Fecha:                 13-05-2019
-- Autor:                 BANCO
-- Input:                 N/A                                                                                                      
-- Output:                N/A
-- Input/Output:          N/A.
-- Retorno:               N/A.
-- Observaciones:
--***********************************************************************************************/
PROCEDURE SP_CHI_CON_DEPENDENCY( P_FECHA IN DATE);

FUNCTION FN_CHI_CON_HEADER ( P_PKEVENT IN NUMBER) RETURN NUMBER;

END PGK_CHI_CON_DATE_DEP;
/


GRANT EXECUTE ON CHI_CON.PGK_CHI_CON_DATE_DEP TO PUBLIC;

